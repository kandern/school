/* Author: Nicholas Anderson
 * Last Edit: 02/11/2019
 * G00895684
 * Sorry for the overload of comments, Anytime I hear I am being graded on comments, I go a bit overboard
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
//this prevents us from having to hardcode values everywhere
#define STRING_LENGTH (25)
#define NUM_OF_STRINGS (10)
//just makes it easier for me to read
#define true (1)
#define false (0)
#define RESET "\x1b[0m"//resets the color of the text
#define BOLD "\033[1m"//bold text, I couldn't figure out how to do underline

//retrieves the string from the user, and puts it into the given pointer
void getInput(int,char*);
//will check if the string is not too long, and that it contains only allowed chars
//if the string is too large, it will clear out the fgets buffer
char* validateString(char *);
//insert the given string into the array, sorted in ascending order
int insert(char arr[NUM_OF_STRINGS][STRING_LENGTH+1],char string[STRING_LENGTH]);
//will return a char representing the sort direction retrieved from the user
char getSort();


int main(){
  char strings[NUM_OF_STRINGS][STRING_LENGTH+1] = {{0}};//2D array holding all input strings

  printf("\nEnter %d character strings:\n\n",NUM_OF_STRINGS);

  //ask for all 10 strings
  for(int i=0;i<NUM_OF_STRINGS;i++){
    char str[STRING_LENGTH];
    getInput(i,str);
    //check the array for a duplicate if one is found, ask for a new string
    //if not, insert it into the sorted array
    if(insert(strings,str)){
      printf(BOLD"Error: Duplicate string - please re-enter\n"RESET);
      //redo the input for this string
      i--;
    }
  }

  //determine what order to print the strings in.
  char order = getSort();

  //user chose 'A'
  //print out the strings in Ascending order strings[0]->strings[NUM_OF_STRINGS]
  if(order=='A'){
    printf(BOLD"\nAscending order:\n\n"RESET);
    for(int i=0;i<NUM_OF_STRINGS;i++){
      printf("%s\n",strings[i]);
    }

  //user chose 'D'
  //print out the strings in Descending order strings[NUM_OF_STRINGS]->strings[0]
  }else{
    printf(BOLD"\nDescending order:\n\n"RESET);
    for(int i=NUM_OF_STRINGS-1;i>=0;i--){
      printf("%s\n",strings[i]);
    }
  }

  printf("\nString with lowest ascii value: %s\n",strings[0]);
  printf("String with highest ascii value: %s\n",strings[NUM_OF_STRINGS-1]);

  return 0;
}

//Gets the input string from the user. will check the string for correctness
//when it recieves a valid string, it will write it to the output string
void getInput(int strNum,char* output){
  //Allocate a c string with two extra bytes, 1 for \n and 1 for 0
  char buffer[STRING_LENGTH+2];
  int valid = false;
  //loop till they enter an acceptable string
  do{
    //request the input string
    printf("Enter string %d: ",strNum+1);
    fgets(buffer,sizeof(buffer),stdin);
    //validate the string
    char* error = validateString(buffer);

    //Check to see if we reach a new line i.e end of input,
    //if not, the user entered too long of a string.
    if(strlen(error)!=0){
      //tell the user that they entered a string that is too long
      printf("%s",error);
      valid = false;
    }else
      valid = true;

  }while(!valid);
  //trim off the new line
  buffer[strlen(buffer)-1] = 0;
  //copy the input string to the pointer we were given
  strcpy(output,buffer);
}

//will validate the string. will return an error message if the string is invalid
char* validateString(char * string){
  //string too short 1 character long means it is just \n
  if(strlen(string)==1)
    return BOLD"Error: String too short, please enter a string between 1 and 25 characters(inclusive)\n"RESET;
  //string too long
  if(strchr(string,'\n')==NULL){
    //clear out the fgets buffer, we don't care what they entered
    do{
      fgets(string,sizeof(string),stdin);
    }while(strchr(string,'\n')==NULL);
    return  BOLD"Error: String too long, Please enter a string between 1 and 25 characters (inclusive)\n"RESET;
  }
  //string contains invalid character
  if(strchr(string,'!'))
    return BOLD"Error: ! is an illegal character - please re-enter\n"RESET;
  if(strchr(string,'@'))
    return BOLD"Error: @ is an illegal character - please re-enter\n"RESET;
  if(strchr(string,'#'))
    return BOLD"Error: # is an illegal character - please re-enter\n"RESET;
  if(strchr(string,'$'))
    return BOLD"Error: $ is an illegal character - please re-enter\n"RESET;
  if(strchr(string,'%'))
    return BOLD"Error: \% is an illegal character - please re-enter\n"RESET;
  if(strchr(string,'^'))
    return BOLD"Error: ^ is an illegal character - please re-enter\n"RESET;
  if(strchr(string,'('))
    return BOLD"Error: ( is an illegal character - please re-enter\n"RESET;
  if(strchr(string,')'))
    return BOLD"Error: ) is an illegal character - please re-enter\n"RESET;
  //string is valid
  return "";
}

//will insert the string into the array of strings.
//If the string is a duplicate, it will leave the array as is and return 1
int insert(char arr[NUM_OF_STRINGS][STRING_LENGTH+1],char* string){
  for(int i=0;i<NUM_OF_STRINGS;i++){

    //if arr[i] is empty, put the string there
    if(strcmp("",arr[i])==0){
      strcpy(arr[i],string);
      break;
    }

    //if our string is less than the arr[i], put this string in arr[i] and shift everything to the right
    if(strcmp(string,arr[i])<0){
      //shift all the rest of the strings 1 over
      for(int j=NUM_OF_STRINGS-1;j>=i;j--){
        strcpy(arr[j+1],arr[j]);
      }
      strcpy(arr[i],string);
      break;
    }

    //if we find a duplicate, return 1 and leave the array as is
    if(strcmp(string,arr[i])==0)
      return 1;
  }
  return 0;
}

//will return the sort direction retrieved from the user
char getSort(){
 char order;

  char buffer[STRING_LENGTH];
  //loop till they enter an acceptable string (char)
  do{
    //request the input string
    printf("\nPrint charachter strings in "BOLD"A"RESET"scending or "BOLD"D"RESET"escending order:");
    fgets(buffer,sizeof(buffer),stdin);
    //validate the string

    //user entered more than one character
    if(strchr(buffer,'\n')==NULL||strlen(buffer)>2){
      //clear out the fgets buffer, we don't care what they entered
      while(strchr(buffer,'\n')==NULL){
        fgets(buffer,sizeof(buffer),stdin);
      }
      printf(BOLD"Error: String too long, please enter just one character, A/D. - please re-enter\n"RESET);
      continue;
    }

    //user just entered \n
    if(strlen(buffer)==1){
      printf(BOLD"Error: String too short, please enter one character, A/D. - please re-enter\n"RESET);
      continue;
    }

    //user entered one character, set it to order and check if it is A, or D
    order = buffer[0];
    if(order!='A'&&order!='D'){
      printf(BOLD"Error: %c is an invalid option - please re-enter.\n"RESET,order);
    }
  }while(order!='A'&&order!='D');

  return order;
}
