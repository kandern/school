Artifact Name:
    Since this is a Procedurally generated model, I just call it tree, since there is only one way to ever get the same tree which is by using the same seed.
Notes:
   I have made a procedural tree generation class called tree.cpp
   you can modify multiple different values of the tree including the vertical spacing of tree limbs/twigs
   you can also modify the ratio of the branches, twigs and leaves. The heirarchy of sizes are as follows
    Leaf Size
        |
        based on
         |
         v
    Twig Size
        |
        based on
        |
        v
    Branch Size
        |
        based on
        |
        v
    Trunk Size
  I did make it solely out of triangles, though I did make a method to make a trapezoid out of triangles.
  
  The only thing I can say for ideas for extra credit is the thing I did with mesh loading.
  I made it so that the color was based on the normal of the face. I think doing something along these lines is cool, since normals are quite important when it comes to 3d models.
  So I think some sort of extra credit focusing on normals would be nice.