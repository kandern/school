#pragma once
#include <string>
#include <vector>
#include "vec.h"
#include "model.h"

#define winter 0
#define spring 1
#define summer 2
#define fall   3
class tree : public model {
public:
	tree();
	tree(int);
	void draw();
	float length = 1;
	float trunkWidth = .4;
	float topWidth = 5;
	float treeHeight = 5.0;//the height of whole tree
	float branchRatio = .3;
	float twigRatio = .2;
	float leafRatio = .7;
	int	  numLevels = 30;
	int	  branchSegments = 10;
	int	  branchSpacing = 3;
	int   windSpeed = 5;
	float spread = 1;
	int  seed;
	bool  animating;
private:
	int  frameDrops = 0;
	int  defaultFrameDrops = 10;
	int  step;
	void drawTrunk();
	void drawBranch(float, float, bool);
	void drawLeaf(int season);
};