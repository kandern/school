#include "modelerview.h"
#include "modelerapp.h"
#include "modelerdraw.h"
#include <FL/gl.h>

#include "modelerglobals.h"
#include "tree.h"

// To make a SampleModel, we inherit off of ModelerView
class SampleModel : public ModelerView 
{

public:
	SampleModel(int x, int y, int w, int h, char *label) 
        : ModelerView(x,y,w,h,label)
    {
        m_tree = new tree();
        models.push_back(m_tree);
	}

    void draw();

private:
    tree* m_tree;
};

// We need to make a creator function, mostly because of
// nasty API stuff that we'd rather stay away from.
ModelerView* createSampleModel(int x, int y, int w, int h, char *label)
{ 
    return new SampleModel(x,y,w,h,label); 
}

// We are going to override (is that the right word?) the draw()
// method of ModelerView to draw out SampleModel
void SampleModel::draw()
{
    m_tree->animating = ModelerApplication::Instance()->m_animating;
    m_tree->seed   = VAL(SEED);
    m_tree->spread = VAL(SPREAD);
    m_tree->windSpeed = VAL(WIND_SPEED);
    m_tree->branchSpacing = VAL(BRANCH_SPACING);
    m_tree->branchSegments = VAL(BRANCH_SEGMENTS);
    m_tree->numLevels = VAL(TRUNK_SEGMENTS);
    m_tree->branchRatio = VAL(BRANCH_RATIO);
    m_tree->twigRatio = VAL(TWIG_RATIO);
    m_tree->leafRatio = VAL(LEAF_RATIO);
    // This call takes care of a lot of the nasty projection 
    // matrix stuff.  Unless you want to fudge directly with the 
	// projection matrix, don't bother with this ...
    ModelerView::draw();
    glPushMatrix();
        glScaled(VAL(SCALE), VAL(SCALE), VAL(SCALE));
        models.at(modelIndex)->draw();
    glPopMatrix();
}

int main()
{
	// Initialize the controls
	// Constructor is ModelerControl(name, minimumvalue, maximumvalue, 
	// stepsize, defaultvalue)
    ModelerControl controls[NUMCONTROLS];
    controls[SCALE] = ModelerControl("Overall Scale", 0, 2, .1f, 1);
    controls[SEED] = ModelerControl("seed", 0, 10, 1, 1);
    controls[SPREAD] = ModelerControl("spread", 0, 1, 0.1f, 1);
    controls[WIND_SPEED] = ModelerControl("wind speed", 0, 100, 1, 10);
    controls[BRANCH_SPACING] = ModelerControl("branch spacing(vertical)", 0, 4, 1, 1);
    controls[BRANCH_SEGMENTS] = ModelerControl("Branch Segments", 0, 20, 1, 10);
    controls[TRUNK_SEGMENTS] = ModelerControl("Trunk Segments", 0, 100, 1, 30);
    controls[BRANCH_RATIO] = ModelerControl("Branch scale", 0, 5, 0.1f, .3f);
    controls[TWIG_RATIO] = ModelerControl("Twig scale", 0, 5, 0.1f, .2);
    controls[LEAF_RATIO] = ModelerControl("Leaf scale", 0, 5, 0.1f, .7);

    ModelerApplication::Instance()->Init(&createSampleModel, controls, NUMCONTROLS);
    return ModelerApplication::Instance()->Run();
}
