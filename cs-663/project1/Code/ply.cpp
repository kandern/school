#include "ply.h"
#include <sstream>
#include <iomanip>
#include "modelerdraw.h"
#include "modelerview.h"
#include "modelerapp.h"
#include <FL/gl.h>
#include "modelerglobals.h"
using namespace std;

ply::ply() : vertices(), faces()
{}

ply::ply(vector<Vec3<double>> &v, vector<vector<int>> &f): vertices(), faces()
{
}


void ply::pushVertex(Vec3<double> & v) {
	vertices.push_back(Vec3<double>(v[0], v[1], v[2]));
}

void ply::pushFace(vector<int> & f) {
	faces.push_back(vector<int>());
	for (int i : f) {
		faces.at(faces.size() - 1).push_back(i);
	}
}

std::string ply::toString() {
	stringstream outString;
	outString << &faces<<endl;
	outString << faces[0][0] << endl;
	outString << std::setprecision(10);
	for (vector<int> face : faces) {
		outString << "[";
		for (int i = 0; i<face.size(); i++) {
			outString << "(";
			Vec3<double> vertex =  vertices[face[i]];
			outString << vertex[0] << ", ";
			outString << vertex[1] << ", ";
			outString << vertex[2] << ")";
			if (i != face.size() - 1) {
				outString << "-> ";
			}
		}
		outString << "]" << endl;
	}
	return outString.str();
}

Vec3<double> cross(Vec3<double> v1, Vec3<double> v2) {
	double x = v1[1] * v2[2] - v1[2] * v2[1];
	double y = v1[2] * v2[0] - v1[0] * v2[2];
	double z = v1[0] * v2[1] - v1[1] * v2[0];
	return Vec3<double>(x, y, z);
}


void ply::draw() {
	// draw the model
	glPushMatrix();
	for (vector<int> face : faces) {
		Vec3<double> v1 = vertices[face[0]];
		Vec3<double> v2 = vertices[face[1]];
		Vec3<double> v3 = vertices[face[2]];
		Vec3<double> c = cross(v2-v1, v3-v1);
		c.normalize();
		setDiffuseColor(c[0],c[1],c[2]);
		drawTriangle(v1[0], v1[1], v1[2],
					 v2[0], v2[1], v2[2], 
					 v3[0], v3[1], v3[2]);
		
	}
	glPopMatrix();
}
