#pragma once
#include <string>
#include <vector>
#include "vec.h"
#include "model.h"
class ply : public model{
public:
	ply();
	ply(std::vector<Vec3<double>> &, std::vector<std::vector<int>> &);
	std::string toString();
	void draw();
	void pushVertex(Vec3<double> &);
	void pushFace(vector<int> &);
private:
	std::vector<Vec3<double>> vertices;
	std::vector<std::vector<int>> faces;
};