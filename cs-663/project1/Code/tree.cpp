#include "tree.h"
#include "modelerdraw.h"
#include "modelerapp.h"
#include "modelerglobals.h"

tree::tree():
	seed(3),
	step(1)
{
	srand(seed);
}
tree::tree(int startingSeed) :
	seed(startingSeed),
	step(1)
{
}

void tree::draw()
{
	srand(seed);
	if (animating){
		step++;
	}
	else {
		step = 0;
	}
	drawTrunk();

}

void tree::drawTrunk() {// draw the trunk
	float height = treeHeight / numLevels;
	float width = trunkWidth;
	int levelToChance = 100 / numLevels;
 	setAmbientColor(.1f, .1f, .1f);
	glPushMatrix();
	int lastBranch = branchSpacing;
	//draw main trunk
	for (int level = 0; level < numLevels; level++) {
		lastBranch++;
		float bottomWidth = (width / numLevels) * (numLevels - level);
		float topWidth = (width / numLevels) * (numLevels - (level + 1));

		setDiffuseColor(.5f, .14f, .14f);
		glPushMatrix();
		glTranslated(0, height * level, bottomWidth / 2);
		drawTrapazoidalPrism(bottomWidth, topWidth, height);
		//choose if we want to make any branches, and if so their direction
		//we can make up to 4, one for each side;
		bool grewBranch = false;
		for (int i = 0; i < 4 && level > numLevels /2 && lastBranch>=branchSpacing; i++) {
			//the chance grows as we go up the tree
			bool growBranch = (rand() % 100) > ((numLevels - level) * levelToChance);
			glPushMatrix();

			//choose a direction for the branch
			glTranslated(0, 0, -bottomWidth / 2);
			//turn around the tree
			glRotated((rand() % 90 )* (i+1), 0, 1, 0);
			//rotate flat
			glRotated(90 * spread, 0, 0, 1);
			if (growBranch) {
				grewBranch = true;
				//weird to use the height as a width, but it stops some clipping
				drawBranch(topWidth, treeHeight*branchRatio,false);
			}
			glPopMatrix();
		}
		if (grewBranch) {
			lastBranch = 0;
		}
		glPopMatrix();
	}
	glPopMatrix();
}
void tree::drawBranch(float width, float length, bool twig) {
	int levelToChance = 100 / branchSegments;
	setAmbientColor(.1f, .1f, .1f);
	setDiffuseColor(.5f, .14f, .14f);
	glPushMatrix();
		//draw twig
		for (int level = 0; level < branchSegments - 1; level++) {
			float bottomWidth = (width / branchSegments) * (branchSegments - level);
			float topWidth = (width / branchSegments) * (branchSegments - (level + 1));
			setDiffuseColor(.5f, .14f, .14f);

			glPushMatrix();
			glTranslated(0, (length / branchSegments) * level, bottomWidth / 2);
			drawTrapazoidalPrism(bottomWidth, topWidth, length/branchSegments);
			if (twig) {
				glPopMatrix();
				continue;
			}
			//choose if we want to make any twigs, and if so their direction
			//we can make up to 4, one for each side;
			for (int i = 0; i < 4; i++) {
				//the chance grows as we go up the tree
				bool growBranch = (rand() % 100) > ((branchSegments - level) * levelToChance);
				glPushMatrix();

				//choose a direction for the branch
				glTranslated(0, 0, -bottomWidth / 2);
				glRotated((rand() % 359), 0, 1, 0);
				glRotated(90*spread, 0, 0, 1);
				if (growBranch) {
					//weird to use the height as a width, but it stops some clipping
					drawBranch(topWidth, length*twigRatio, true);
				}
				glPopMatrix();
			}
			glPopMatrix();
		}

		glPushMatrix();
		glTranslated(0, length - (length/branchSegments) , 0);
			float leafScale = length * leafRatio;
			if (!twig) {
				glScaled(leafScale* twigRatio, leafScale* twigRatio, leafScale* twigRatio);
			}
			else {
				glScaled(leafScale, leafScale, leafScale);
			}
			glRotated(sin(step)*windSpeed, 1, 1, 1);
			drawLeaf(1);
		glPopMatrix();
	glPopMatrix();
}

void tree::drawLeaf(int season) {
	glPushMatrix();
		switch (season)
		{
		case winter:// no leaves in fall silly
			glPopMatrix();
			return;
		case spring://tiny light green leaves
			setDiffuseColor(.0f, .14f, .0f);
			break;
		case summer://dark green leaves
			setDiffuseColor(.0f, .04f, .0f);
			break;
		case fall: //orange leaves, or maybe random reddish orange if I have time
			setDiffuseColor(.4f, .14f, .0f);
			break;
		default:
			break;
		}
	  //Stem and veins
	  drawTrapazoidalPrism(.1f, .001f, 1);
	  glPushMatrix();
		  glTranslated(0, .3f, 0);
		  glPushMatrix();
			  glTranslated(0, 0, -.015);
			  glRotated(60, 0, 0, 1);
			  drawTrapazoidalPrism(.07f, .001f, .5f);
		  glPopMatrix();
		  glPushMatrix();
			  glTranslated(0, 0, -.015);
			  glRotated(60, 0, 0, -1);
			  drawTrapazoidalPrism(.07f, .001f, .5f);
		  glPopMatrix();
	  glPopMatrix();
	glPopMatrix();
	glPushMatrix();
		//Draw Leaf Body
		switch (season)
		{
		case winter:// no leaves in fall silly
			return;
		case spring://tiny light green leaves
			setDiffuseColor(.0f, .34f, .0f);
			break;
		case summer://dark green leaves
			setDiffuseColor(.0f, .14f, .0f);
			break;
		case fall: //orange leaves, or maybe random reddish orange if I have time
			setDiffuseColor(.3f, .14f, .0f);
			break;
		default:
			break;
		}
		glTranslated(0, 0, -.05);
		for (int i = 0; i < 2; i++) {
			glPushMatrix();
			glRotated(180*i, 0, 1, 0);//makes the normal backwards, but hopefully that doesn't matter.
									//The color changes, so I wil use that to my advantage
			drawTriangle(
				0, 1, 0,//top Left
				-.15f, .8, 0,//bottom right
				0, .8, 0);//top right
			drawTriangle(
				.15f, .8, 0,//top Left
				.15f, .4, 0,//bottom right
				0, .8, 0);//top right
			drawTriangle(
				0, .8, 0,//top Left
				-.15f, .4, 0,//bottom right
				0, .4, 0 );//top right
			drawTriangle(
				.15f, .4, 0,//top Left
				0, .3, 0,//bottom right
				0, .4, 0);//top right
			glPopMatrix();
		}
		for (int i = 0; i < 2; i++) {
			glPushMatrix();
			glRotated(180 * i, 0, 1, 0);
			drawTriangle(
				-.15f, .4, 0,//top Left
				-.15, .5, 0,//bottom right
				-.4, .6, 0);//top right
			drawTriangle(
				-.4, .6, 0,//top Left
				-.5, .58, 0,//bottom right
				-.15f, .4, 0);//top right
			drawTriangle(
				-.5, .58, 0,//top Left
				-.5, .5, 0,//bottom right
				-.15f, .4, 0);//top right
			drawTriangle(
				-.5, .5, 0,//top Left
				-.3, .3, 0,//bottom right
				0, .2, 0);//top right
			drawTriangle(
				-.5, .5, 0,//top Left
				0, .2, 0,//bottom right
				0, .4, 0);//top right
			glPopMatrix();
		}
	glPopMatrix();
}