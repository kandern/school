#pragma once
#include <string>
#include <vector>
#include "vec.h"
class model {
public:
	virtual void draw() = 0;
private:
	int seed = 0;
};