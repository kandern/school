#include "tree.h"
#include "modelerdraw.h"
#include "modelerapp.h"
#include "modelerglobals.h"
#include <sstream>

using namespace std;

tree::tree() :
	axiom("AB"),
	length(.5)
{
}

tree::tree(std::string startingSeed) :
	axiom(startingSeed),
	seed()
{
}

void tree::setLength(float newLength) {
	length = newLength;
}
void tree::setTrunkColor(Vec3<float> newColor) {
	trunkColor[0] = newColor[0];
	trunkColor[1] = newColor[1];
	trunkColor[2] = newColor[2];
}
void tree::setSeason(float newSeason) {
	if (newSeason < 1) {
		season = 0;
	}
	else if (newSeason < 2) {
		season = 1;
	}
	else if (newSeason < 3) {
		season = 2;
	}
	else if (newSeason < 4) {
		season = 3;
	}
}

void tree::buildSeed(string newAxiom, int numRecursions) {
	seed = newAxiom;
	stringstream seedStream;
	for (int i = 0; i < numRecursions; i++) {
		for (const char& c : seed) {
			seedStream << rules[c];
		}
		seed = seedStream.str();
		seedStream.str(string());
		seedStream.clear();
	}
}

void tree::draw()
{
	//don't reiterate through the string if it's stayed the same
	if (prevIter != numIter) {
		buildSeed(axiom, numIter);
		prevIter = numIter;
	}
	drawTree(seed, trunkWidth, false);
}

void tree::drawTree(string treeSeed, float newWidth,bool isBranch) {// draw the tree based on a procedural seed
	int numLevels = 0;
	for (const char c : treeSeed) {
		if (c=='A' || c=='[' || c == 'F') {
			numLevels++;
		}
		if (isBranch && c == 'D') {
			numLevels++;
		}
	}
	numLevels = max(numLevels, 1);
	float shrinkRate = newWidth / numLevels;
	float curWidth = newWidth;
	glPushMatrix();
	for (unsigned int i = 0; i < treeSeed.size(); i++) {
		char c = treeSeed.at(i);
		stringstream ss;
		ss << "";
		switch (c)
		{
			//draw a trunk
			case 'A':
			case 'D':
				setDiffuseColor(trunkColor[0], trunkColor[1], trunkColor[2]);
				drawTrapazoidalPrism(curWidth, curWidth-shrinkRate, length);
				glTranslated(0, length, -(shrinkRate/2));
				curWidth -= shrinkRate;
				break;
			case 'B':
				setDiffuseColor(trunkColor[0], trunkColor[1], trunkColor[2]);
				glPushMatrix();
					drawLeaf();
				glPopMatrix();
				break;

			//draw a leaf
			case 'C':
				drawLeaf();
				break;
			//draw a leaf
			case 'E':
				glPushMatrix();
				glRotated(-45, 0, 0, 1);
					drawLeaf();
					glPushMatrix();
						glRotated(90, 0, 1, 0);
						drawLeaf();
					glPopMatrix();
				glPopMatrix();
				break;

			//We are making a branch, push on the matrix and rotate
			case '[': {
				size_t leftBracket = treeSeed.find(']', i) - i - 1;
				//print(string("push mat\n"));
				glPushMatrix();
					glTranslated(0, 0, -curWidth/2);
					for (int j = 0; j < numBranches; j++) {
						glPushMatrix();
						glRotated((360 / numBranches * j), 0, 1, 0);
						glRotated(70 * spread, 1, 0, 0);
						glRotated(90, 0, 1, 0);
						drawTree(treeSeed.substr(i + 1, leftBracket),curWidth + (shrinkRate/2),true);
						glPopMatrix();
					}
				glPopMatrix();
				i += leftBracket+1;
				break;
				//We should realistically never 
			}case ']':
				print(string("] SHOULD NEVER BE REACHED"));
				break;
			default:
				break;
		}
	}
	glPopMatrix();
}

void tree::drawLeaf() {
	glPushMatrix();
		switch (season)
		{
		case winter:// no leaves in fall silly
			glPopMatrix();
			return;
		case spring://tiny light green leaves
			glScaled(.5f,.5f,.5f);
			setDiffuseColor(.0f, .3f, .0f);
			break;
		case summer://dark green leaves
			setDiffuseColor(.01f, .2f, .0f);
			break;
		case fall: //orange leaves, or maybe random reddish orange if I have time
			setDiffuseColor(.4f, .14f, .0f);
			break;
		default:
			break;
		}

	  glPushMatrix();
		  //Stem and veins
		  drawTrapazoidalPrism(.1f, .001f, 1);
		  glPushMatrix();
			  glTranslated(0, .3f, 0);
			  glPushMatrix();
				  glTranslated(0, 0, -.015);
				  glRotated(60, 0, 0, 1);
				  drawTrapazoidalPrism(.07f, .001f, .5f);
			  glPopMatrix();
			  glPushMatrix();
				  glTranslated(0, 0, -.015);
				  glRotated(60, 0, 0, -1);
				  drawTrapazoidalPrism(.07f, .001f, .5f);
			  glPopMatrix();
		  glPopMatrix();
		glPopMatrix();
		glPushMatrix();
			//Draw Leaf Body
			switch (season)
			{
			case winter:// no leaves in fall silly
				return;
			case spring://tiny light green leaves
				setDiffuseColor(.0f, .34f, .0f);
				break;
			case summer://dark green leaves
				setDiffuseColor(.0f, .14f, .0f);
				break;
			case fall: //orange leaves, or maybe random reddish orange if I have time
				setDiffuseColor(.3f, .14f, .0f);
				break;
			default:
				break;
			}
			glTranslated(0, 0, -.05);
			for (int i = 0; i < 2; i++) {
				glPushMatrix();
				glRotated(180*i, 0, 1, 0);//makes the normal backwards, but hopefully that doesn't matter.
										//The color changes, so I wil use that to my advantage
				drawTriangle(
					0, 1, 0,//top Left
					-.15f, .8, 0,//bottom right
					0, .8, 0);//top right
				drawTriangle(
					.15f, .8, 0,//top Left
					.15f, .4, 0,//bottom right
					0, .8, 0);//top right
				drawTriangle(
					0, .8, 0,//top Left
					-.15f, .4, 0,//bottom right
					0, .4, 0 );//top right
				drawTriangle(
					.15f, .4, 0,//top Left
					0, .3, 0,//bottom right
					0, .4, 0);//top right
				glPopMatrix();
			}
			for (int i = 0; i < 2; i++) {
				glPushMatrix();
				glRotated(180 * i, 0, 1, 0);
				drawTriangle(
					-.15f, .4, 0,//top Left
					-.15, .5, 0,//bottom right
					-.4, .6, 0);//top right
				drawTriangle(
					-.4, .6, 0,//top Left
					-.5, .58, 0,//bottom right
					-.15f, .4, 0);//top right
				drawTriangle(
					-.5, .58, 0,//top Left
					-.5, .5, 0,//bottom right
					-.15f, .4, 0);//top right
				drawTriangle(
					-.5, .5, 0,//top Left
					-.3, .3, 0,//bottom right
					0, .2, 0);//top right
				drawTriangle(
					-.5, .5, 0,//top Left
					0, .2, 0,//bottom right
					0, .4, 0);//top right
				glPopMatrix();
			}
		glPopMatrix();
	glPopMatrix();
}