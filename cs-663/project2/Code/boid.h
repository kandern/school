#pragma once
#include "model.h"
#include "vec.h"
class boidsModel : public model
{

struct boid {
	int type;
	int id;
	int goalIndex;
	double r;
	void (*draw)(double);
	Vec3<float> * position;
	Vec3<float> * velocity;
};
public:
	boidsModel();
	void draw();
	void moveBoids();
	void setDrawDirection(bool);
	void setBreeds(bool);
	void setBoundingLength(float);
	void setPerceptionRadius(float);
	void setAvoidenceRadius(float);
	void setMaxVelocity(float);
private:
	int numBoids;
	bool breeds;
	float radius;
	vector<boid *> boids;
	void drawBoid(boid *);
	Vec3<float> center(boid*);
	Vec3<float> avoidOthers(boid*);
	Vec3<float> avoidOtherBreeds(boid*);
	Vec3<float> matchVelocity(boid*);
	Vec3<float> stayBounded(boid*);
	Vec3<float> goTowardsGoal(boid*);
	vector<Vec3<float>> goals;
	bool drawDirection;
	float boundingLength;
	float perceptionRadius;
	float avoidenceRadius;
	float maxVelocity;

	//Consts
	float matchRatio = 8;
	int step;
	int goalIndex;
	int switchStep = 1000;

};

float getDistance(Vec3<float>, Vec3<float>);
float getMagnitude(Vec3<float>);
