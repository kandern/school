#include "boid.h"
#include "modelerdraw.h"
#include "modelerapp.h"
#include "modelerglobals.h"
#include <math.h>

using namespace std;
boidsModel::boidsModel() :
	numBoids(10),
	radius(.1),
	drawDirection(true),
	boundingLength(4),
	avoidenceRadius(1),
	perceptionRadius(2),
	maxVelocity(.1),
	breeds(false),
	step(0),
	goalIndex(0)
{
	for (int i = 0; i < numBoids;i++) {
		boid * b = new boid();
		b->id = i;
		b->r = radius;
		b->position = new Vec3<float>(rand() % 10, rand() % 10, rand() % 10);
		b->velocity = new Vec3<float>(rand() % 2, rand() % 2, rand() % 2);
		b->type = rand() % 2 + 1;
		b->goalIndex = 0;
		boids.push_back(b);
	}
	goals.push_back(Vec3<float>(3.0f,5.0f,3.0f));
	goals.push_back(Vec3<float>(-3.0f, 5.0f, 3.0f));
	goals.push_back(Vec3<float>(-3.0f, 5.0f, -3.0f));
	goals.push_back(Vec3<float>(3.0f, 5.0f, -3.0f));

}

void boidsModel::draw() {
	for (boid * b: boids) {
		glPushMatrix();
			if (b->type == 1) {
				setDiffuseColor(1, 0, 0);
			}
			if (b->type == 2) {
				setDiffuseColor(0, 0, 1);
			}
			Vec3<float> v = *b->velocity;
			glTranslated((*b->position)[0], (*b->position)[1], (*b->position)[2]);
			drawBoid(b);
		glPopMatrix();
	}
}

void boidsModel::drawBoid(boid * b) {
	drawSphere(b->r);
	if (drawDirection) {
		glBegin(GL_LINES);
			glVertex3f(0, 0, 0);
			Vec3<float> v = *b->velocity;
			v.normalize();
			glVertex3f(v[0]* b->r *2, v[1] * b->r * 2, v[2] * b->r * 2);
		glEnd();
	}
}

void boidsModel::moveBoids() {
	step++;
	if (step == 50) {
		goalIndex = (goalIndex + 1) % 4;
		step = 0;
	}

	for (boid* b : boids) {
		Vec3<float> newVelocity = *b->velocity;
		newVelocity += center(b);
		newVelocity += avoidOthers(b);
		newVelocity += goTowardsGoal(b);
		newVelocity += matchVelocity(b);
		newVelocity += stayBounded(b);
		if (getMagnitude(newVelocity) > maxVelocity) {
			newVelocity = newVelocity / getMagnitude(newVelocity) * maxVelocity;
		}
		(*b->position)[0] = (*b->position)[0] + newVelocity[0];
		(*b->position)[1] = (*b->position)[1] + newVelocity[1];
		(*b->position)[2] = (*b->position)[2] + newVelocity[2];
		(*b->velocity)[0] = newVelocity[0];
		(*b->velocity)[1] = newVelocity[1];
		(*b->velocity)[2] = newVelocity[2];
	}
}


Vec3<float> boidsModel::center(boid* b) {
	Vec3<float> center(0,0,0);
	int numBirds = 0;
	for (boid* b2 : boids) {
		if (breeds && b2->type != b->type) {
			continue;
		}
		if (b2->id == b->id || getMagnitude(*b2->position - *b->position) > perceptionRadius) {
			continue;
		}
		center += (*b2->position);
		numBirds++;
	}
	if (numBirds > 0) {
		center = center / (numBirds);
		center = center - (*b->position);
	}
	return  center / 100;
}

Vec3<float> boidsModel::avoidOthers(boid* b) {
	Vec3<float> newVelocity(0, 0, 0);
	for (boid* b2 : boids) {
		if (b2->id == b->id) {
			continue;
		}
		if (getMagnitude(* b2->position - *b->position) < avoidenceRadius) {
			Vec3<float> distance = (*b2->position - *b->position);
			if (breeds && b2->type != b->type) {
				distance += (*b2->position - *b->position);
			}
			newVelocity = newVelocity - distance;
		}
	}
	return newVelocity/10;
}

Vec3<float> boidsModel::matchVelocity(boid* b) {
	Vec3<float> newVelocity(0, 0, 0);
	int numBirds = 0;
	for (boid* b2 : boids) {
		if (breeds && b2->type != b->type) {
			continue;
		}
		if (b2->id == b->id || getMagnitude(*b2->position - *b->position) > perceptionRadius) {
			continue;
		}
		newVelocity += *b2->velocity;
		numBirds++;
	}
	if (numBirds >0) {
		newVelocity = newVelocity / numBirds;
	}
	return newVelocity / matchRatio;
}

Vec3<float> boidsModel::stayBounded(boid* b) {
	Vec3<float> center(0, 0, 0);
	Vec3<float> newPos(0, 0, 0);
	Vec3<float> pos = *b->position;
	float distance = getMagnitude(pos);
	if( distance > boundingLength){
		newPos = center-pos;
	}

	return newPos/100;
}


Vec3<float> boidsModel::goTowardsGoal(boid* b) {
	Vec3<float> newVelocity = (goals.at(goalIndex) - *b->position) / 1000;
	return newVelocity;
}

void boidsModel::setDrawDirection(bool newDrawDirection) {
	drawDirection = newDrawDirection;
}

void boidsModel::setBreeds(bool newBreeds) {
	breeds = newBreeds;
}

void boidsModel::setBoundingLength(float newBoundingLength) {
	boundingLength = newBoundingLength;
}

void boidsModel::setPerceptionRadius(float newRadius) {
	perceptionRadius = newRadius;
}

void boidsModel::setAvoidenceRadius(float newRadius) {
	avoidenceRadius = newRadius;
}

void boidsModel::setMaxVelocity(float newVelocity) {
	maxVelocity = newVelocity;
}

float getDistance(Vec3<float> v1,Vec3<float> v2) {
	float distance = sqrt(pow(v1[0] + v2[0], 2) + pow(v1[1] + v2[1], 2) + pow(v1[2] + v2[2], 2));
	return distance;
}

float getMagnitude(Vec3<float> v1) {
	return getDistance(v1,Vec3<float>(0,0,0));
}