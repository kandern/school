#pragma once
#include <string>
#include <vector>
#include "vec.h"
#include "model.h"
#include <map>

#define winter 0
#define spring 1
#define summer 2
#define fall   3



struct levelDetails{
	bool trunk;
	int  branches;
	bool leaf;
};
class tree : public model {
public:
	std::map<const char, std::string> rules = {
		{'A',"AD"},//growing trunk
		{'B',"D[ADEC]B"},//static trunk with branch
		{'C',"DEC"},//Leaf -> branch + leaf
		//Constants
		{'D',"D"},//static Branch
		{'E',"E"},//static leaf
		{'[',"["},//start an off shoot
		{']',"]"},//end an off shoot
	};

	tree();
	tree(std::string);
	void draw();
	std::string axiom = "";
	std::string seed;
	int numBranches = 1;
	float trunkWidth = .4f;
	float branchRatio = .3f;
	float twigRatio = .2f;
	float leafRatio = .7f;
	int	  branchSpacing = 3;
	int   windSpeed = 5;
	float spread = 1.0f;
	int prevIter = 0;
	int numIter = 7;
	void setLength(float);
	void setSeason(float);
	void setTrunkColor(Vec3<float>);

private:
	int  frameDrops = 0;
	int  defaultFrameDrops = 10;
	float length;
	int season;
	Vec3<float> trunkColor;

	void buildSeed(std::string, int);
	void drawTree(std::string,float,bool);
	void drawBranch(float, float, bool);
	void drawLeaf();
};