#include "modelerview.h"
#include "modelerapp.h"
#include "modelerdraw.h"
#include <FL/gl.h>

#include "modelerglobals.h"
#include "tree.h"
#include "boid.h"
#include "vec.h"

// To make a SampleModel, we inherit off of ModelerView
class SampleModel : public ModelerView 
{

public:
	SampleModel(int x, int y, int w, int h, char *label) 
        : ModelerView(x,y,w,h,label)
    {
        m_tree = new tree();
        models.push_back(m_tree);
        b = new boidsModel();
        models.push_back(b);
	}

    void draw();

private:
    tree* m_tree;
    boidsModel* b;
};

// We need to make a creator function, mostly because of
// nasty API stuff that we'd rather stay away from.
ModelerView* createSampleModel(int x, int y, int w, int h, char *label)
{ 
    return new SampleModel(x,y,w,h,label); 
}

// We are going to override (is that the right word?) the draw()
// method of ModelerView to draw out SampleModel
void SampleModel::draw()
{
    //m_tree->animating = ModelerApplication::Instance()->m_animating;
    m_tree->spread = VAL(SPREAD);
    m_tree->numIter = VAL(NUM_ITERATIONS);
    m_tree->setLength(VAL(LENGTH));
    m_tree->numBranches = VAL(NUM_BRANCHES);
    m_tree->setTrunkColor(Vec3<float>(VAL(TRUNK_R), VAL(TRUNK_G), VAL(TRUNK_B)));
    m_tree->setSeason(VAL(SEASON));

    b->setDrawDirection(VAL(DRAW_DIRECTION) > 0);
    b->setBreeds(VAL(BREEDS) > 0);
    b->setMaxVelocity(VAL(MAX_VELOCITY));
    b->setPerceptionRadius(VAL(PERCEPTION_RADIUS));
    b->setBoundingLength(VAL(BOUNDING_LENGTH));
    b->setAvoidenceRadius(VAL(AVOIDENCE_RADIUS));
    // This call takes care of a lot of the nasty projection 
    // matrix stuff.  Unless you want to fudge directly with the 
	// projection matrix, don't bother with this ...
    ModelerView::draw();
    glPushMatrix();
        glScaled(VAL(SCALE), VAL(SCALE), VAL(SCALE));
        if (ModelerApplication::Instance()->m_animating) {
            b->moveBoids();
        }
        for (model * m : models) {
            m->draw();
        }
    glPopMatrix();
}

int main()
{
	// Initialize the controls
	// Constructor is ModelerControl(name, minimumvalue, maximumvalue, 
	// stepsize, defaultvalue)
    ModelerControl controls[NUMCONTROLS];
    controls[SCALE] = ModelerControl("Overall Scale", 0, 2, .1f, 1);
    controls[NUM_ITERATIONS] = ModelerControl("Number of Iterations", 1, 10, 1, 1);
    controls[SPREAD] = ModelerControl("spread", 0, 2, 0.1f, 1);
    controls[NUM_BRANCHES] = ModelerControl("Number of Branches", 0, 10, 1, 1);
    controls[LENGTH] = ModelerControl("Tallness", 0, 5, 0.1f, .5);
    controls[TRUNK_R] = ModelerControl("TrunkR", 0, 1, 0.1f, .5);
    controls[TRUNK_G] = ModelerControl("TrunkG", 0, 1, 0.1f, .14);
    controls[TRUNK_B] = ModelerControl("TrunkB", 0, 1, 0.1f, .14);
    controls[SEASON] = ModelerControl("Season", 0, 3, 1, 1);
    controls[BREEDS] = ModelerControl("Different Breeds avoid each other", 0, .1, .1, 0);
    controls[DRAW_DIRECTION] = ModelerControl("Draw Direction", 0, 1, 1, 1);
    controls[BOUNDING_LENGTH] = ModelerControl("Max Distance from center", 1, 10, .1, 5);
    controls[PERCEPTION_RADIUS] = ModelerControl("Perception Radius", 0, 3, .1, .4);
    controls[AVOIDENCE_RADIUS] = ModelerControl("Avoidence Radius", .1, 1, .1, .2);
    controls[MAX_VELOCITY] = ModelerControl("Max Velocity", .1, 10, .1, .1);

    ModelerApplication::Instance()->Init(&createSampleModel, controls, NUMCONTROLS);
    return ModelerApplication::Instance()->Run();
}
