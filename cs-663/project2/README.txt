L system:
    variables : A B C D E F
    constants : [ ] D E F
    axiom  : AB
    rules  : (A → AD), (B → D[ADEC]B), (C → DEC)

which produces:
(0): AB
(1): ADD[ADEC]B
(2): ADDD[ADDEDEC]D[ADEC]B
(3): ADDDD[ADDDEDEDEC]D[ADDEDEC]D[ADEC]B
(4): ADDDDD[ADDDDEDEDEDEC]D[ADDDEDEDEC]D[ADDEDEC]D[ADEC]B
(5): ADDDDDD[ADDDDDEDEDEDEDEC]D[ADDDDEDEDEDEC]D[ADDDEDEDEC]D[ADDEDEC]D[ADEC]B
(6): ADDDDDDD[ADDDDDDEDEDEDEDEDEC]D[ADDDDDEDEDEDEDEC]D[ADDDDEDEDEDEC]D[ADDDEDEDEC]D[ADDEDEC]D[ADEC]B
(7): ADDDDDDDD[ADDDDDDDEDEDEDEDEDEDEC]D[ADDDDDDEDEDEDEDEDEC]D[ADDDDDEDEDEDEDEC]D[ADDDDEDEDEDEC]D[ADDDEDEDEC]D[ADDEDEC]D[ADEC]B


What they do:
A,F,D all create one branch/trunk section.
B and C draws a regular leaf on the edge of the branch/tip of the tree
E Draws a rotated leaf, to the top of the branch with another rotated 90 degrees, to make it look 3d
[  Designates the begining of a branch which is perpendicular to the trunk
]  Ends the current branch this is actually skipped in the draw step
(  Draws a branch/twig, the difference between this and "[", is that this is unaffected by the number of branches variable
) Ends the current branch/twig this is actually skipped in the draw step.

Notes:
I based the tree off of an acacia tree.

Leaves are conditional, if the season is winter, "0", then, they won't draw since acacia trees can be deciduous. 

The two parameters I added, were  one for season, and one for changing the number of branches.

The Season parameter changes the leave's shape, color, and whether or not they draw in the first place. 
Winter - No leaves
Spring - small green leaves
Summer - large dark green leaves
Fall - Orange leaves

The number of branches, changes the number of branches per branch set of brackets.
In order to do multiple branches, my code relies on matching [] and () and for those to not be nested inside themselves, i.e. (()). 

I also added a feature to boids
I made it so that the red and blue boids will fly together but ignore each other.
I made a function to make them actively avoid each other, but once I added the goals, it didn't look very nice.
Regardless, if you use the slider, it and give them enough time, you will see the two groups diverge
I find a good way to make things happen is increase their velocity, or the avoiding radius to get them separated,
since the processing is frame rate based, these will both create some distance between them that they won't fix imediately.