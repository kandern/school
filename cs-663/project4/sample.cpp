// The sample model.  You should build a file
// very similar to this for when you make your model.
#include "modelerview.h"
#include "modelerapp.h"
#include "modelerdraw.h"
#include <FL/gl.h>
#include <GL/glut.h>
#include <sstream>
#include "vec.h"
#include "MakeItHome.h"
#include "modelerglobals.h"
#include <algorithm>
#include <vector>

using namespace std;
// To make a SampleModel, we inherit off of ModelerView
class MyModel : public ModelerView 
{
public:

	goal goals;
	state currentState;
	state bestState;
	vector<costs> costs;
	STATE state;
	MyModel(int x, int y, int w, int h, char* label)
		: ModelerView(x, y, w, h, label),
		currentState(),
		goals(),
		costs()
	{
		//Couch
		currentState.itteration = 0;
		currentState.couch.height = 1.0f;
		currentState.couch.width = 2.0f;
		currentState.couch.position = new Vec3<float>(0, 0, 0);
		currentState.couch.rotation = 0.0f;
		//TV
		currentState.couch.height = 1.0f;
		currentState.couch.width = 2.0f;
		currentState.tv.position = new Vec3<float>(10, 2, 10);
		currentState.tv.rotation = 0.0f;
		state = DONE;
		//SideTable
		currentState.sideTable.height = 0.5f;
		currentState.sideTable.width = 0.5f;
		currentState.sideTable.position = new Vec3<float>(2, 0, 2);
		currentState.sideTable.rotation = 0.0f;
		currentState.cost = FLT_MAX;
		ModelerApplication::Instance()->allCosts = &costs;
	}

    virtual void draw();
};

// We need to make a creator function, mostly because of
// nasty API stuff that we'd rather stay away from.
ModelerView* createSampleModel(int x, int y, int w, int h, char *label)
{
    return new MyModel(x,y,w,h,label);
}

// We are going to override (is that the right word?) the draw()
// method of ModelerView to draw out SampleModel
void MyModel::draw()
{
	//currentState.couch.position = new Vec3<float>(GETVAL(COUCH_XPOS), 2, GETVAL(COUCH_ZPOS));
	//currentState.couch.rotation = GETVAL(COUCH_ROTATION);
	if (state == DONE) {
		goals.maxItterations = GETVAL(MAXLOOPCOUNTER);
		goals.roomSize = GETVAL(FLOOR_SIZE);
		(*currentState.couch.position)[0] = max(min((*currentState.couch.position)[0], (goals.roomSize - currentState.couch.width / 2)), currentState.couch.width / 2);
		(*currentState.couch.position)[2] = max(min((*currentState.couch.position)[2], (goals.roomSize - currentState.couch.width / 2)), currentState.couch.width / 2);

		(*currentState.tv.position)[0] = max(min((*currentState.tv.position)[0], (goals.roomSize - currentState.tv.width / 2)), currentState.tv.width / 2);
		(*currentState.tv.position)[2] = max(min((*currentState.tv.position)[2], (goals.roomSize - currentState.tv.width / 2)), currentState.tv.width / 2);

		(*currentState.sideTable.position)[0] = max(min((*currentState.sideTable.position)[0], (goals.roomSize - currentState.sideTable.width / 2)), currentState.sideTable.width / 2),
		(*currentState.sideTable.position)[2] = max(min((* currentState.sideTable.position)[2], (goals.roomSize - currentState.sideTable.width / 2)), currentState.sideTable.width / 2);

		//set weights
		goals.couchTVAngle = GETVAL(COUCH_TV_ANGLE);
		goals.couchTVAngleWeight = GETVAL(COUCH_TV_ANGLE_WEIGHT);
		goals.tvCouchAngle = GETVAL(TV_COUCH_ANGLE);
		goals.tvCouchAngleWeight = GETVAL(TV_COUCH_ANGLE_WEIGHT);
		goals.couchTVDistance = GETVAL(COUCH_TV_DISTANCE);
		goals.couchTVDistanceWeight = GETVAL(COUCH_TV_DISTANCE_WEIGHT);
		goals.couchSideTableDistance = GETVAL(COUCH_SIDETABLE_DISTANCE);
		goals.couchSideTableDistanceWeight = GETVAL(COUCH_SIDETABLE_ANGLE_WEIGHT);
		goals.couchSideTableAngle = GETVAL(COUCH_SIDETABLE_ANGLE);
		goals.couchSideTableAngleWeight = GETVAL(COUCH_SIDETABLE_DISTANCE_WEIGHT);

		goals.tStep = 2;
		goals.rStep = 15;
		goals.temperature = 1000;
		goals.maxTemperature = 1000;
	}

	if (ModelerApplication::Instance()->init) {
		currentState.cost = FLT_MAX;
		initialize(&goals, &currentState);
		bestState = copyState(&currentState);
		ModelerApplication::Instance()->init = false;
	}

	if (ModelerApplication::Instance()->start == true) {
		state = READY;
		ModelerApplication::Instance()->start = false;
	}
	
	if (state == RUNNING || state == READY) {
		state = makeItHome(&goals, &currentState,&costs);
		if (bestState.cost > currentState.cost) {
			bestState = copyState(&currentState);
		}
		if (state == DONE) {
			bestState.itteration = 1000;
			currentState = copyState(&bestState);
		}
	}
	

    // This call takes care of a lot of the nasty projection 
    // matrix stuff. Unless you want to fudge directly with the 
	// projection matrix, don't bother with this ...
    ModelerView::draw();
	if (ModelerApplication::Instance()->visualize || state != RUNNING) {
		// draw the floor
		setAmbientColor(.1f, .1f, .1f);
		setDiffuseColor(0.0f, 0.0f, 0.5f);
		glPushMatrix();
		//move the floor and make its side align to x-axis and z-axis
		glTranslated(GETVAL(FLOOR_SIZE) / 2, 0, GETVAL(FLOOR_SIZE) / 2);
		drawBox(GETVAL(FLOOR_SIZE), 0.1f, GETVAL(FLOOR_SIZE));
		glTranslated(-GETVAL(FLOOR_SIZE) / 2, 0, -GETVAL(FLOOR_SIZE) / 2);
		glPopMatrix();

		char c1[6] = "Couch";
		// draw the Chair
		glPushMatrix();
		setAmbientColor(COLOR_ORANGE);
		setDiffuseColor(COLOR_ORANGE);
		//draw a box in such way to make rotation axis in obj center
		glTranslated((*currentState.couch.position)[0], 0.5f, (*currentState.couch.position)[2]);
		glRotated(currentState.couch.rotation, 0.0, 1.0, 0.0);
		drawBox(2.0f, 1, 1.0f);

		//drawVector
		setDiffuseColor(COLOR_RED);
		glPushMatrix();
		drawVector(0.0f, 0, 0.0f, 3); //from x1,y1   to y2,x2 object origin
		glPopMatrix();

		//drawtext
		glPushMatrix();
		drawString(c1);
		glPopMatrix();
		glPopMatrix();

		//Draw TV*****************************************************************************************************************************
		char c2[3] = "TV";
		glPushMatrix();
		setAmbientColor(COLOR_LIGHTGREY);
		setDiffuseColor(COLOR_LIGHTGREY);
		//draw a box in such way to make rotation axis in obj center
		glTranslated((*currentState.tv.position)[0], (*currentState.tv.position)[1], (*currentState.tv.position)[2]);
		glRotated(currentState.tv.rotation, 0, 1, 0);
		drawBox(2.0f, 1, 0.1f);
		setAmbientColor(COLOR_BLUE);
		setDiffuseColor(COLOR_BLUE);
		glTranslated(0.0f, 0.0f, 0.1f);
		drawBox(1.8f, 0.8f, 0.001f);

		//drawVector
		setDiffuseColor(COLOR_LIGHTGREEN);
		glPushMatrix();
		drawVector(0.0f, 0, 0.0f, 3); //from x1,y1   to y2,x2 object origin
		glPopMatrix();

		//drawtext
		glPushMatrix();
		drawString(c2);
		glPopMatrix();
		glPopMatrix();

		//Draw TV*****************************************************************************************************************************
		char c3[10] = "SideTable";
		glPushMatrix();
		setAmbientColor(COLOR_RED);
		setDiffuseColor(COLOR_LIGHTGREY);
		//draw a box in such way to make rotation axis in obj center
		glTranslated((*currentState.sideTable.position)[0], (*currentState.sideTable.position)[1], (*currentState.sideTable.position)[2]);
		glRotated(currentState.sideTable.rotation, 0, 1, 0);
		drawBox(0.5f, .5f, 0.5f);

		//drawVector
		setDiffuseColor(COLOR_LIGHTGREEN);
		glPushMatrix();
		drawVector(0.0f, 0, 0.0f, 3); //from x1,y1   to y2,x2 object origin
		glPopMatrix();

		//drawtext
		glPushMatrix();
		drawString(c3);
		glPopMatrix();
	}else{
		drawBigStringOnScreen("PROCESSING...");
	}
	glPopMatrix();

	struct costs currentCost = getCost(&goals, &currentState);
	stringstream ss;
	ss << "Itteration: " << currentState.itteration << " |   "
		<< "Total Cost: " << currentCost.total << " |   "
		<< "tv-Couch distance Cost: " << currentCost.couchTVDistance << " |   "
		<< "tv-Couch angle Cost: " << currentCost.tvCouchAngle << " |   "
		<< "Couch-tv angle Cost: " << currentCost.couchTVAngle << " |   "
		//<< "couch-sideTable distance Cost: " << currentCost.couchSideTableDistance << " |   "
		//<< "couch-sideTable angle Cost: "    << currentCost.couchSideTableAngle
		;
	char* outString = new char[ss.str().size() + 1];
	strcpy(outString, ss.str().c_str());

	//debug text
	drawStringOnScreen(outString);
	
}

int main()
{
	// Initialize the controls
	// Constructor is ModelerControl(name, minimumvalue, maximumvalue, stepsize, defaultvalue)
	
    ModelerControl controls[NUMCONTROLS];
	controls[MAXLOOPCOUNTER] = ModelerControl("Max Loop Counter", 100, 5000, 1.00f, 1000);

	controls[COUCH_TV_ANGLE] = ModelerControl("Couch->TV Angle", 0.0, 360, 1.0, 0);
	controls[COUCH_TV_ANGLE_WEIGHT] = ModelerControl("Couch->TV Angle Weight", 0, 1, .01f, 1);

	controls[TV_COUCH_ANGLE] = ModelerControl("TV->Couch Angle", 0.0, 360, 1.0, 0);
	controls[TV_COUCH_ANGLE_WEIGHT] = ModelerControl("TV->Couch Angle Weight", 0, 1, .01f, 1);

	controls[COUCH_TV_DISTANCE] = ModelerControl("TV->Couch Distance", 0.0, 10, 1.0, 5);
	controls[COUCH_TV_DISTANCE_WEIGHT] = ModelerControl("TV->Couch Distance Weight", 0, 1, .01f, 1);

	controls[COUCH_SIDETABLE_ANGLE] = ModelerControl("Couch->SideTable Angle", 0.0, 360, 1.0, 0);
	controls[COUCH_SIDETABLE_ANGLE_WEIGHT] = ModelerControl("Couch->SideTable Angle Weight", 0, 0, .01f, 0);

	controls[COUCH_SIDETABLE_DISTANCE] = ModelerControl("Couch->SideTable Distance", 0.0, 10, 1.0, .5);
	controls[COUCH_SIDETABLE_DISTANCE_WEIGHT] = ModelerControl("Couch->SideTable Distance Weight", 0, 0, .01f, 0);

    //controls[COUCH_XPOS] = ModelerControl("Couch X Position", 0, 30, 0.1f, 2); 
    //controls[COUCH_ZPOS] = ModelerControl("Couch Z Position", 0, 30, 0.1f, 2);
	//controls[COUCH_ROTATION] = ModelerControl("Couch  Rotation", 0, 360, 1, 0);

	controls[FLOOR_SIZE] = ModelerControl("Size Of Floor", 10, 30, 0.1f, 20);
	
	


    ModelerApplication::Instance()->Init(&createSampleModel, controls, NUMCONTROLS);
    return ModelerApplication::Instance()->Run();
}


