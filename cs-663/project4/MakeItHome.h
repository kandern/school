#pragma once

#include "vec.h"
#include <windows.h>
#include <math.h>
#include "modelerglobals.h"
#include <vector>
#define print(x) (OutputDebugStringA(x.c_str()))
#define deg2Rad(x) (x*M_PI/180.0)

typedef struct object {
    float width;
    float height;
    float rotation;
    Vec3<float>* position;
} object;

typedef struct costs {
    float total;
    float tvCouchAngle;
    float couchTVAngle;
    float couchTVDistance;
    float couchSideTableAngle;
    float couchSideTableDistance;
} costs;

typedef struct goal {
    float tStep;
    float rStep;
    float roomSize;
    int   maxItterations;
    float maxTemperature;
    float temperature;
    float couchTVAngle;
    float couchTVAngleWeight;
    float tvCouchAngle;
    float tvCouchAngleWeight;
    float couchTVDistance;
    float couchTVDistanceWeight;
    float couchSideTableDistance;
    float couchSideTableDistanceWeight;
    float couchSideTableAngle;
    float couchSideTableAngleWeight;
} goals;

typedef struct state {
    float cost;
    int   itteration;
    object couch;
    object tv;
    object sideTable;
} state;

enum furniture
{COUCH, TV , /*SIDE_TABLE,*/ NUM_FURNITURE };

enum STATE
{DONE, READY, RUNNING};

void initialize(goal *, state *);
STATE makeItHome(goal *,state *,vector<costs> *);
object copyObj(object &);
state copyState(state*);
void move(object &, goals *);
void moveT(object &,goals *);
void moveR(object &, goals *);
costs getCost(goal *, state *);
void ensureInRoom(goal*, state*);
bool aneal(goal *,float c1, float c2);
/*
    stringstream ss;
    ss << desiredAngle << endl;
    print(ss.str());
*/