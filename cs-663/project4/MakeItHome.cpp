﻿#include "MakeItHome.h"
#include <stdlib.h>     /* srand, rand */
#include <random>
#include <sstream>
#include <algorithm>

/*
typedef struct object {
    float width;
    float height;
    float rotation;
    Vec3<float>* position;
} object;

typedef struct goal {
    float roomSize;
    int   maxItterations;
    float couchTVAngle;
    float tvCouchAngle;
    float couchTVDistance;
} goals;

typedef struct state {
    float cost;
    int   itteration;
    object couch;
    object tv;
} state;
*/

void initialize(goal *goals, state *currentState) {
    srand(time(NULL));
    currentState->itteration = 0;

    (*currentState->couch.position)[0] = rand() % int(goals->roomSize - 2)+1;
    (*currentState->couch.position)[2] = rand() % int(goals->roomSize - 2)+1;
    currentState->couch.rotation = rand() % 360;
    (*currentState->tv.position)[0] = rand() % int(goals->roomSize - 2) + 1;
    (*currentState->tv.position)[2] = rand() % int(goals->roomSize - 2)+1;
    currentState->tv.rotation = rand() % 360;
    (*currentState->sideTable.position)[0] = rand() % int(goals->roomSize - 2) + 1;
    (*currentState->sideTable.position)[2] = rand() % int(goals->roomSize - 2) + 1;
    currentState->sideTable.rotation = rand() % 360;
}

STATE makeItHome(goal *goals, state *currentState, vector<costs> * costs) {
    // if we already did the max number of itterations, stop.
    if (currentState->itteration == goals->maxItterations) {
        return DONE;
    }
    if (currentState->itteration == 800) {
        goals->temperature = 10;
    }else if (currentState->itteration == 400) {
        goals->temperature = 100;
    }
    //goals->temperature - 1;
    // increase our itteration count
    currentState->itteration++;
    object obj;
    int choice = rand() % NUM_FURNITURE;
    object * curObj;
    // pick a piece of furniture and move it
    switch (choice)
    {
        case COUCH:
            obj = copyObj(currentState->couch);
            curObj = &currentState->couch;
            move(currentState->couch,goals);
            break;
        case TV:
            obj = copyObj(currentState->tv);
            curObj = &currentState->tv;
            move(currentState->tv, goals);
            break;
        /*case SIDE_TABLE:
            obj = copyObj(currentState->sideTable);
            curObj = &currentState->sideTable;
            move(currentState->sideTable, goals);
            break;*/
    }
    ensureInRoom(goals, currentState);
    // revert if this is worse
    float newCost = getCost(goals, currentState).total;
    costs->push_back(getCost(goals, currentState));
    if (aneal(goals, currentState->cost,newCost)) {// revert
        currentState->cost = newCost;
    }
    else {// keep
        curObj->rotation = obj.rotation;
        (*curObj->position)[0] = (*obj.position)[0];
        (*curObj->position)[1] = (*obj.position)[1];
        (*curObj->position)[2] = (*obj.position)[2];
    }
    ensureInRoom(goals, currentState);
    free(obj.position);
    return RUNNING;
}

void move(object &obj,goals *goal) {
    int choice = rand() % 2;
    switch (choice)
    {
    case 0:
        moveT(obj, goal);
        break;
    case 1:
        moveR(obj, goal);
        break;
    default:
      break;
    }
}

void moveT(object& obj,goals *goal) {
    float stepSize = max(goal->tStep * (goal->temperature / goal->maxTemperature),1);
    int choice = rand() % 4;
    switch (choice)
    {
    case 0:
        (*obj.position)[0] = (*obj.position)[0] + stepSize;
        break;
    case 1:
        (*obj.position)[0] = (*obj.position)[0] - stepSize;
        break;
    case 2:
        (*obj.position)[2] = (*obj.position)[2] + stepSize;
        break;
    case 3:
        (*obj.position)[2] = (*obj.position)[2] - stepSize;
        break;
    }
}

void moveR(object& obj, goals* goal) {
    float stepSize = max(goal->rStep * (goal->temperature / goal->maxTemperature),4);
    int choice = rand() % 2;
    switch (choice)
    {
    case 0:
        obj.rotation = obj.rotation + stepSize;
        break;
    case 1:
        obj.rotation = obj.rotation - stepSize;
        break;
    }

    if (obj.rotation > 360) {
        obj.rotation = obj.rotation - 360;
    }
    if (obj.rotation < 0) {
        obj.rotation = obj.rotation + 360;
    }
}

costs getCost(goal* goals, state* currentState) {
    costs newCost = {0,0,0,0,0,0};
    //Make sure the tv points to the couch
    float x = (*currentState->couch.position)[0] - (*currentState->tv.position)[0];
    float y = (*currentState->couch.position)[2] - (*currentState->tv.position)[2];
    float desiredAngle = atan2(x, y) * 180 / M_PI + goals->tvCouchAngle;
    if (desiredAngle < 0) {
        desiredAngle = 360 + desiredAngle;
    }
    newCost.tvCouchAngle = abs(currentState->tv.rotation - desiredAngle) * goals->tvCouchAngleWeight;
    
    //Make sure the couch points to the tv
    x = (*currentState->tv.position)[0] - (*currentState->couch.position)[0];
    y = (*currentState->tv.position)[2] - (*currentState->couch.position)[2];
    desiredAngle = atan2(x, y) * 180 / M_PI + goals->couchTVAngle;
    if (desiredAngle < 0) {
        desiredAngle = 360 + desiredAngle;
    }

    newCost.couchTVAngle = abs(currentState->couch.rotation - desiredAngle) * goals->couchTVAngleWeight;

    //Make sure the distance between the couch and TV is correct
    float dist = sqrtf(pow((*currentState->tv.position)[0] - (*currentState->couch.position)[0],2) +
                       pow((*currentState->tv.position)[2] - (*currentState->couch.position)[2],2));
    newCost.couchTVDistance = abs(dist - goals->couchTVDistance) *goals->couchTVDistanceWeight;

    //Make sure the side table is the right distance from the couch
    dist = sqrtf(pow((*currentState->sideTable.position)[0] - (*currentState->couch.position)[0], 2) +
        pow((*currentState->sideTable.position)[2] - (*currentState->couch.position)[2], 2));
    newCost.couchSideTableDistance += abs(dist - (currentState->couch.width / 2) + .25) *goals->couchSideTableDistanceWeight;

    //make sure the sidetable is to the side of the couch
    x = (*currentState->sideTable.position)[0] - (*currentState->couch.position)[0];
    y = (*currentState->sideTable.position)[2] - (*currentState->couch.position)[2];
    desiredAngle = atan2(x, y) * 180 / M_PI + 90;
    if (desiredAngle < 0) {
        desiredAngle = 360 + desiredAngle;
    }
    newCost.couchSideTableAngle += abs(currentState->couch.rotation - desiredAngle) *goals->couchSideTableAngleWeight;
    
    newCost.total += newCost.couchSideTableAngle
        + newCost.couchSideTableDistance 
        + newCost.couchTVAngle
        + newCost.couchTVDistance
        + newCost.tvCouchAngle;
    return newCost;
}

bool aneal(goal* goals, float c1, float c2) {
    if (c1 > c2){
        return true;
    }
    float B = max((goals->maxTemperature-goals->temperature-100)/1000,0);
    float p = min(exp(B*(c1-c2)), 1);
    float r = rand() / static_cast<float>(RAND_MAX);
    return r < p;
}

void ensureInRoom(goal* goals, state* currentState) {
    //check each point of each piece of furniture;
    bool inRoom = true;
    float xMod[] = {1,1,-1,-1};
    float yMod[] = {1,-1,1,-1};
    (*currentState->couch.position)[0] = max(min((*currentState->couch.position)[0], (goals->roomSize - currentState->couch.width / 2)), currentState->couch.width / 2);
    (*currentState->couch.position)[2] = max(min((*currentState->couch.position)[2], (goals->roomSize - currentState->couch.width / 2)), currentState->couch.width / 2);

    (*currentState->tv.position)[0] = max(min((*currentState->tv.position)[0], (goals->roomSize - currentState->couch.width / 2)), currentState->couch.width / 2);
    (*currentState->tv.position)[2] = max(min((*currentState->tv.position)[2], (goals->roomSize - currentState->couch.width / 2)), currentState->couch.width / 2);

    (*currentState->sideTable.position)[0] = max(min((*currentState->sideTable.position)[0], (goals->roomSize - currentState->sideTable.width / 2)), currentState->sideTable.width / 2),
    (*currentState->sideTable.position)[2] = max(min((*currentState->sideTable.position)[2], (goals->roomSize - currentState->sideTable.width / 2)), currentState->sideTable.width / 2);
}

object copyObj(object& oldObj) {
    object obj;
    obj.width = oldObj.width;
    obj.height = oldObj.height;
    obj.position = new Vec3<float>();
    (*obj.position)[0] = (*oldObj.position)[0];
    (*obj.position)[1] = (*oldObj.position)[1];
    (*obj.position)[2] = (*oldObj.position)[2];
    obj.rotation = oldObj.rotation;
    return obj;
}

state copyState(state* currentState) {
    state s = { 
        currentState->cost,
        currentState->itteration,
        copyObj(currentState->couch),
        copyObj(currentState->tv),
        copyObj(currentState->sideTable) };
    return s;
}