from copy import deepcopy
from copy import copy
from piece import Piece
import random

class Board:
  def __init__(self, b,newMethod,newPos = (-1,-1)):
    self.board = b
    #self.goals = deepcopy(goals)
    self.rows = len(self.board)
    self.columns = len(self.board[0])
    self.moves = []
    self.moveList = []
    self.method = newMethod
    self.hash = 0
    self.h = -1
    self.f = -1
    if newPos[0] == -1:
      for i in range(self.rows):
        for j in range(self.columns):
          if self.board[i][j].value == 0:
            self.pos = (i,j)
    else:
      self.pos = newPos
    self.lastMove = 'X'


  def __eq__(self,other):
    return hash(self) == hash(other)

  def __lt__(self,other):
    if self.f == -1 or other.f == -1:
      return self.h < other.h
    else:
      return self.f < other.f

  def __hash__(self):
    if self.hash == 0:
      self.hash = hash(self.getMappings())
    return self.hash

  def heuristic(self,goals):
    if self.h == -1:
      self.h = 0
      for i in range(self.rows):
        for j in range(self.columns):
          if self.board[i][j].value != -1:
            self.h += self.board[i][j].heuristic(i,j,self.method)
      #check in case we setup the pieces with imposible positioning due to ordering
      matches = True
      for i in range(self.rows):
        for j in range(self.columns):
          if self.board[i][j].value != goals[i][j]:
            matches = False
            break
        if not matches:
          break
      if matches:
        self.h = 0
    if self.method == 0:
      return min(self.h,1)
    return self.h

  # We are assuming that the string given in the string are row major form.
  def fromString(N,M,boardStr,goalStr,newMethod):
    board = []
    for i in range(N):
      board.append([])
      for j in range(M):
        board[i].append(Piece(int(boardStr[i*M+j])))
    goals = []
    for i in range(N):
      goals.append([])
      for j in range(M):
        goals[i].append(int(goalStr[i*M+j]))
    # loop through each goal to assign pieces to this goal
    for goalRow in range(len(goals)):
      for goalCol in range(len(goals[0])):
        # this is a wall, skip it
        if goals[goalRow][goalCol] == '-1':
          continue
        found = False
        # find a matching piece and set their goal to ourselves
        for pieceRow in range(len(board)):
          if found:
            break
          for pieceCol in range(len(board[0])):
            piece = board[pieceRow][pieceCol]
            # we match the goal, so set our goal position to the goal
            if piece.value == goals[goalRow][goalCol] and piece.goal == (-1,-1):
              piece.goal = (goalRow,goalCol)
              found = True
              break

    b = Board(board,newMethod)
    # stored in row, column, instead of x,y
    b.pos = (int(boardStr.index('0')/len(board[0])),int(boardStr.index('0')%len(board[0])))
    b.moves.append(str(b.pos))
    return b

  def copy(self):
    newBoard = []
    for i in range(self.rows):
      newBoard.append(copy(self.board[i]))
    newB = Board(newBoard,self.method, newPos=(self.pos[0],self.pos[1]))
    newB.moves = copy(self.moves)
    newB.moveList = copy(self.moveList)
    newB.h = self.h
    newB.lastMove = self.lastMove
    return newB

  def get(self, N:int, M:int):
    return self.board[N][M]

  def applyMoves(self,directions):
    for direction in directions:
      self.move(direction)


  # accepts N,E,S,W
  def move(self,direction: str):
    success = False
    newPos = self.pos
    match direction:
      case 'N':
        if self.pos[0] > 0 and self.canMove(direction):
          success = True
          newPos = (self.pos[0]-1,self.pos[1])
      case 'E':
        if self.pos[1] < self.columns-1 and self.canMove(direction):
          success = True
          newPos = (self.pos[0],self.pos[1]+1)
      case 'S':
        if self.pos[0] < self.rows-1 and self.canMove(direction):
          success = True
          newPos = (self.pos[0]+1,self.pos[1])
      case 'W':
        if self.pos[1] > 0 and self.canMove(direction):
          success = True
          newPos = (self.pos[0],self.pos[1]-1)
    if success:
      # get our pieces current heuristics
      oldH = self.board[self.pos[0]][self.pos[1]].heuristic(self.pos[0],self.pos[1],self.method)
      oldH += self.board[newPos[0]][newPos[1]].heuristic(newPos[0],newPos[1],self.method)
      # get our new pieces' heuristics
      newH = self.board[self.pos[0]][self.pos[1]].heuristic(newPos[0],newPos[1],self.method)
      newH += self.board[newPos[0]][newPos[1]].heuristic(self.pos[0],self.pos[1],self.method)
      self.h = self.h - (oldH-newH)
      tEmpty = self.board[self.pos[0]][self.pos[1]]
      self.board[self.pos[0]][self.pos[1]] = self.board[newPos[0]][newPos[1]]
      self.board[newPos[0]][newPos[1]] = tEmpty
      self.pos = newPos
      self.moves.append(str(newPos))
      self.lastMove = direction
      self.moveList.append(direction)

      # set new heuristic:
      
    return success

  def canMove(self,direction):
    blocked = False
    match direction:
      case 'N':
        if self.pos[0] <= 0\
        or self.board[self.pos[0]-1][self.pos[1]].value == -1:
          blocked = True
      case 'E':
        if self.pos[1] >= self.columns-1\
        or self.board[self.pos[0]][self.pos[1]+1].value == -1:
          blocked = True
      case 'S':
        if self.pos[0] >= self.rows-1\
        or self.board[self.pos[0]+1][self.pos[1]].value == -1:
          blocked = True
      case 'W':
        if self.pos[1] <= 0\
        or self.board[self.pos[0]][self.pos[1]-1].value == -1:
          blocked = True
    return not blocked

  def getMoves(self):
    directions = []
    if self.pos[0] > 0\
    and self.board[self.pos[0]-1][self.pos[1]].value != -1:
      directions.append('N')

    if self.pos[1] < self.columns-1\
    and self.board[self.pos[0]][self.pos[1]+1].value != -1:
      directions.append("E")

    if self.pos[0] < self.rows-1\
    and self.board[self.pos[0]+1][self.pos[1]].value != -1:
      directions.append("S")

    if self.pos[1] > 0\
    and self.board[self.pos[0]][self.pos[1]-1].value != -1:
      directions.append("W")
    random.shuffle(directions)
    return directions

#Printing*******************************************************************************************
  def __str__(self):
    maxValue = 0
    boardStrings = []
    for i in range(self.rows):
      for j in range(self.columns):
        maxValue = max(maxValue,self.board[i][j].value)
    for i in range(self.rows):
      rowStrings = []
      for j in range(self.columns):
        rowStrings.append(str(self.board[i][j]).rjust(len(str(maxValue))))
        maxValue = max(maxValue,self.board[i][j].value)
      boardStrings.append(" ".join(rowStrings))
    return "\n".join(boardStrings)

  def printGoals(goals):
    maxValue = 0
    boardStrings = []
    rows = len(goals)
    columns = len(goals[0])
    for i in range(rows):
      for j in range(columns):
        maxValue = max(maxValue,goals[i][j])
    for i in range(rows):
      rowStrings = []
      for j in range(columns):
        rowStrings.append(str(goals[i][j]).rjust(len(str(maxValue))))
        maxValue = max(maxValue,goals[i][j])
      boardStrings.append(" ".join(rowStrings))
    print("\n".join(boardStrings))

  def getMappings(self):
    boardStrings = []
    for i in range(self.rows):
      boardStrings.append(str(self.board[i]))
    return "".join(boardStrings)

  def print_debug(self):
    print(self)
    print('-------------------------')
    print(self.getMappings())
    print("h: " + str(self.h))
    print("method: " + str(self.method))

  def isGood(self,goal):
    good = 0
    for i in range(self.rows):
      for j in range(self.columns):
        if self.board[i][j].value != goal[i][j]:
          print((i,j))
          good +=1
    return good
