from board import Board
from hc import processHC
from sa import processSA
from other import processOther
import sys
import datetime
from copy import deepcopy

startTime = datetime.datetime.now()
if len(sys.argv) != 8:
    print("usage: python KnightsAndQueens.py N M Q K tmax fileName methodName")
    exit(1)

columns = int(sys.argv[1])
rows = int(sys.argv[2])
numQueens = int(sys.argv[3])
numKnights = int(sys.argv[4])
tMax = int(sys.argv[5])
fileName = sys.argv[6]
methodName = sys.argv[7]

b1 = Board(rows,columns,numKnights,numQueens)

bestBoard = deepcopy(Board(rows,columns,numKnights,numQueens,initialize=False))
match methodName:
  case 'HC':
    bestBoard = processHC(b1,tMax)
  case 'SA':
    bestBoard = processSA(b1,tMax)
  case 'Other':
    bestBoard = processOther(b1,tMax)

text_file = open(fileName, "w")
text_file.write(str(bestBoard))
text_file.close()
print(bestBoard)
curTime = datetime.datetime.now()
delta = (curTime - startTime).seconds
print('runtime: ' + str(delta) + 's')