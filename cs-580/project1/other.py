from board import Board
from copy import copy, deepcopy
from boardTree import Node
import datetime
import time


restartCountDefault = 2
maxMoves = 10

def processOther(b: Board, timeLimit: int):
  # start a bunch of random threads that each perform a sort of hill climbing
  # selecting random pieces(that have conflicts) X iterations of getting better
  # after all threads stop, choose the best one
  # Add current Boards to a tree
  # repeat using the new board as a base
  # If board can no longer be improved, move up tree one level and try again
  startTime = datetime.datetime.now()
  n = Node(b,b.getTotalConflictsFast())
  bestBoard = (b,b.getTotalConflictsFast())
  while bestBoard[1] > 0:
    tBest = n.getBest(max(len(b.queens)+len(b.knights),maxMoves),startTime,timeLimit)
    if tBest < bestBoard[1]:
      bestBoard = (n.getCurrentBestBoard(),tBest)
    b.randomize()
    n = Node(b,b.getTotalConflictsFast())

    curTime = datetime.datetime.now()
    delta = (curTime - startTime).seconds
    #Quit if taking too long
    if delta >= timeLimit:
      return bestBoard[0]
  
  return bestBoard[0]
