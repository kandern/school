class Piece:

  def __init__(self, coordinates, conflicts: int):
    self.coordinates = coordinates
    self.conflicts = conflicts

  def __lt__(self, other):
    if self.conflicts > other.conflicts:
       return True
    # if self.conflicts == other.conflicts:
    #   if self.coordinates[0] < other.coordinates[0]:
    #     return True
    #   if self.coordinates[0] == other.coordinates[0]:
    #     if self.coordinates[1] < other.coordinates[1]:
    #       return True

  def __str__(self):
    PosString  = "position: " + str(self.getCoordinates()) + '\n' 
    PosString += "  conflicts: " + str(self.getConflicts())
    return PosString

  def getCoordinates(self):
    return self.coordinates

  def getConflicts(self):
    return self.conflicts