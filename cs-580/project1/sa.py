from board import Board
from copy import copy, deepcopy
import datetime
import time
import random
import math

zero = .000000001

def processSA(b: Board, timeLimit: int):
  startTime = datetime.datetime.now()
  currentConflicts = b.getTotalConflicts(False)
  # temp board in case we take too long
  bestBoard = (Board(0,0,0,0,initialize = False),currentConflicts)
  bestBoard[0].fromBoard(b.board)
  t = 1
  while t>0 and currentConflicts > 0:
    T = schedule(t)
    if T < zero:
      return b
    neighbor = getNeighbor(b)
    acceptBad = getProb(neighbor[0],T)
    if neighbor[0] < 0 or acceptBad:
      # swap knight if we didn't choose queen
      if neighbor[1] == -1:
        currentConflicts = b.swapKnight(neighbor[2],neighbor[3])
      else:
        currentConflicts = b.swapQueen(neighbor[1],neighbor[3])

    t = t+1
    # if we ran out of time, return and print our failed attempt
    curTime = datetime.datetime.now()
    delta = (curTime - startTime).seconds
    if delta >= timeLimit:
      # if this is better than the best board save it
      break
  return b

# get's a random neighbor of our board, and returns it's score, piece index, and empty spot index
# (score,queenIndex,KnightIndex,emptyIndex)
def getNeighbor(b: Board):
  result = [-1,-1,-1,random.randrange(0,len(b.freeSpaces))]
  if (len(b.queens) != 0 and random.randrange(0,2) == 0) or len(b.knights) == 0:
    pieceIndex = random.randrange(0,len(b.queens))
    conflicts = b.queens[pieceIndex].conflicts
    result[1] = pieceIndex
    newConflicts = b.testQueen(pieceIndex,result[3])
    result[0] = newConflicts - conflicts
  else:
    pieceIndex = random.randrange(0,len(b.knights))
    conflicts = b.knights[pieceIndex].conflicts
    result[2] = pieceIndex
    newConflicts = b.testKnight(pieceIndex,result[3])
    result[0] = newConflicts - conflicts 
  return result

def schedule(t: int):
  return 1/t

def getProb(delta:int, T: float):
  dE = float(abs(delta))
  prob = math.exp(-(dE*10)/T)
  rand = random.uniform(0.0,1.0)
  return rand < prob