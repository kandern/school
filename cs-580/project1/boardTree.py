import random
from piece import Piece
from board import Board
from copy import deepcopy
import datetime
# queens:     [((x,y),numConflicts),...]
# knights:    [((x,y),numConflicts),...]
# freeSpaces: [(x,y),...]
class Node:
  ID = 0
  def __init__(self,b:Board,s: int):
    self.children = []
    self.board = b
    self.index = 0
    self.score = s
    self.bestScore = s
    self.best = -1
    self.id = Node.ID
    Node.ID = Node.ID+1

  def __lt__(self, other):
    if self.score < other.score:
       return True

  # gets the best score in our branch
  def getBest(self, numMoves: int,startTime,timeLimit):
    curTime = datetime.datetime.now()
    delta = (curTime - startTime).seconds
    if numMoves == 0 or delta >= timeLimit:
      return self.score
    #initialize children
    for i in range(len(self.board.queens)+len(self.board.knights)):
      #if we hit something with 0 conflicts stop processing this type of child piece
      if i >= len(self.board.queens):
        if self.board.knights[i-len(self.board.queens)].conflicts == 0:
          break
      else:
        if self.board.queens[i].conflicts == 0:
          i = len(self.board.queens) - 1
          continue

      self.children.append(Node(deepcopy(self.board),self.score))
      curIndex = len(self.children)-1
      if i >= len(self.board.queens):
        for j in range(len(self.children[curIndex].board.knights)):
          if self.children[curIndex].board.knights[j].conflicts == 0:
            break
          bestSpace = self.children[curIndex].board.getBestKnightPlacement(j)
          self.children[curIndex].board.swapKnight(curIndex-len(self.board.queens),bestSpace,sort=False,store=False)
      else:
        for j in range(len(self.children[curIndex].board.queens)):
          if self.children[curIndex].board.queens[j].conflicts == 0:
            break
          bestSpace = self.children[curIndex].board.getBestQueenPlacement(j)
          self.children[curIndex].board.swapQueen(i,bestSpace,sort=False,store=False)
      self.children[curIndex].score = self.children[curIndex].board.getTotalConflicts(True)
      # sort our pieces now that we moved everything around
      self.children[curIndex].board.queens.sort()
      self.children[curIndex].board.knights.sort()
    # sort the child boards and find our best child on our branch
    self.children.sort()
    if(len(self.children) == 0):
      return 0
    for i in range(len(self.children)):
      #none of our children beat us
      if self.children[i].score >= self.score:
        return self.bestScore
      childScore = self.children[i].getBest(numMoves-1,startTime,timeLimit)
      if childScore < self.bestScore:
        self.best = i
        self.bestScore = childScore
      if childScore == 0:
        return 0
    return self.bestScore

  # returns our best board in our branch
  def getCurrentBestBoard(self):
    if self.best == -1:
      return self.board
    else:
      return self.children[self.best].getCurrentBestBoard()