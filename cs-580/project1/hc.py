from board import Board
from copy import copy, deepcopy
import datetime
import time

restartCountDefault = 2

def processHC(b: Board, timeLimit: int):
  startTime = datetime.datetime.now()

  currentConflicts = b.getTotalConflicts(False)
  bestBoard = (Board(0,0,0,0,initialize = False),currentConflicts)
  #bestBoard[0].fromBoard(b.board)

  # (QueenIndex,freeIndex,TotalConflicts)
  bestNeighbor = (0,-1,currentConflicts)
  restartCount = 2
  while currentConflicts > 0:
    # if we got stuck random restart
    if restartCount == 0:
      b.randomize()
      currentConflicts = b.getTotalConflicts(False)
      # (QueenIndex,freeIndex,TotalConflicts)
      bestNeighbor = (0,-1,currentConflicts)
      restartCount = restartCountDefault
    currentConflicts = bestNeighbor[2]
    
    # we will loop through all Knights until we find one that reduces the number of conflicts
    for knightIndex in range(len(b.knights)):
      # if we ran out of time, return and print our failed attempt
      curTime = datetime.datetime.now()
      delta = (curTime - startTime).seconds
      #Quit if taking too long
      if delta >= timeLimit:
        # if this is better than the best board save it
        if currentConflicts < bestBoard[1]:
          bestBoard[0].fromBoard(b.board)
        return bestBoard[0]

      # if we are at a knight with no conflicts, moving does not help us
      if b.knights[knightIndex].conflicts == 0:
        break
      # if we happened upon a solution, quit
      if bestNeighbor[2] == 0:
        break
      knightConflicts = b.knights[knightIndex].conflicts
      newIndex = -1
      for freeIndex in range(len(b.freeSpaces)):
        newConflicts = b.testKnight(0,freeIndex)
        if newConflicts < knightConflicts:
          knightConflicts = newConflicts
          newIndex = freeIndex
          if newConflicts == 0:
            break
      # if we beat our old score, save it, and sort our knights again
      if knightConflicts != b.knights[knightIndex].conflicts:
        bestNeighbor = (knightIndex,freeIndex,b.swapKnight(knightIndex,newIndex))
        break

    #if we didn't find a knight that reduced our conflicts, move a queen
    if currentConflicts == bestNeighbor[2]:
      # we will loop through all queens until we find one that reduces the number of conflicts
      for queenIndex in range(len(b.queens)):
        # if we ran out of time, return and print our failed attempt
        curTime = datetime.datetime.now()
        delta = (curTime - startTime).seconds
        #Quit if taking too long
        if delta >= timeLimit:
          # if this is better than the best board save it
          if currentConflicts < bestBoard[1]:
            bestBoard[0].fromBoard(b.board)
          return bestBoard[0]
        # if we are at a queen with no conflicts, moving does not help us
        if b.queens[queenIndex].conflicts == 0:
          break
        # if we happened upon a solution, quit
        if bestNeighbor[2] == 0:
          break

        # test each neighbor of this queen storing the best one in bestNeighbor
        queenConflicts = b.queens[queenIndex].conflicts
        newIndex = -1
        for freeIndex in range(len(b.freeSpaces)):
          newConflicts = b.testQueen(0,freeIndex)
          if newConflicts < queenConflicts:
            queenConflicts = newConflicts
            newIndex = freeIndex
            if newConflicts == 0:
              break
        # if we beat our old score, save it, and sort our queens again
        if queenConflicts != b.queens[queenIndex].conflicts:
          bestNeighbor = (queenIndex,freeIndex,b.swapQueen(queenIndex,newIndex))
          break

    # if we have gotten the same count too many times, randomize and start over
    if currentConflicts == bestNeighbor[2]:
      restartCount = restartCount - 1
    else:
      restartCount = restartCountDefault

    # if we ran out of time, return and print our failed attempt
    curTime = datetime.datetime.now()
    delta = (curTime - startTime).seconds
    #Quit if taking too long
    if delta >= timeLimit:
      # if this is better than the best board save it
      if currentConflicts < bestBoard[1]:
        bestBoard[0].fromBoard(b.board)
      return bestBoard[0]

  return b
