import random
from piece import Piece
# queens:     [((x,y),numConflicts),...]
# knights:    [((x,y),numConflicts),...]
# freeSpaces: [(x,y),...]

class Board:

  def __init__(self, x: int, y: int,numK: int,numQ: int,initialize = True):
    self.board = [['E' for i in range(x)] for i in range(y)]
    self.freeSpaces = []
    self.numKnights = numK
    self.knights = []
    self.numQueens = numQ
    self.queens = []
    if numQ + numK > x*y:
      raise Exception("Too many Queens and Knights for board size")
    if initialize:
      self.randomize()

  def fromBoard(self, newBoard):
    self.freeSpaces.clear()
    self.queens.clear()
    self.knights.clear()
    self.board = [['E' for i in range(len(newBoard[0]))] for i in range(len(newBoard))]
    # create our list of empty spaces
    for y in range(len(newBoard)):
      for x in range(len(newBoard[y])):
        self.board[y][x] = newBoard[y][x]
        match newBoard[y][x]:
          case 'E':
            self.freeSpaces.append((x,y))
          case 'K':
            self.knights.append(Piece((x,y),0))
          case 'Q':
            self.queens.append(Piece((x,y),0))
    self.numKnights = len(self.knights)
    self.numQueens = len(self.queens)
    return self.getTotalConflicts(True)


  def randomize(self):
    self.freeSpaces.clear()
    self.queens.clear()
    self.knights.clear()
    # create our list of empty spaces
    for i in range(len(self.board)):
      for j in range(len(self.board[0])):
        self.freeSpaces.append((j,i))
        self.board[i][j] = 'E'

    # initialize knights
    for i in range(self.numKnights):
      freeSpace = self.freeSpaces.pop(random.randrange(0,len(self.freeSpaces)))
      self.board[freeSpace[1]][freeSpace[0]] = 'K'
      self.knights.append(Piece(freeSpace,0))

    # initialize queens
    for i in range(self.numQueens):
      freeSpace = self.freeSpaces.pop(random.randrange(0,len(self.freeSpaces)))
      self.board[freeSpace[1]][freeSpace[0]] = 'Q'
      self.queens.append(Piece(freeSpace,0))

    # initialize our number of conflicts
    self.getTotalConflicts(True)

    # sort our list of queens and knigts by the number of conflicts they have
    self.queens.sort()
    self.knights.sort()

  # moves the queen to the 
  def swapQueen(self, queenIndex: int, freeSpaceIndex: int, sort = True, store = True):
    # get current places
    emptySpace = self.freeSpaces.pop(freeSpaceIndex)
    queenSpace = self.queens[queenIndex].getCoordinates()
    # swap our places
    self.freeSpaces.append(self.queens[queenIndex].coordinates)
    self.queens[queenIndex].coordinates = emptySpace
    # swap the actual board places
    self.board[queenSpace[1]][queenSpace[0]] = 'E'
    self.board[emptySpace[1]][emptySpace[0]] = 'Q'
    totalConflicts = 0
    if store:
      totalConflicts = self.getTotalConflicts(True)
    if sort:
      self.queens.sort()
    return totalConflicts

  def swapKnight(self,knightIndex: int, freeSpaceIndex: int, sort = True, store = True):
    # get current places
    emptySpace = self.freeSpaces.pop(freeSpaceIndex)
    knightSpace = self.knights[knightIndex].getCoordinates()
    # swap our places
    self.freeSpaces.append(self.knights[knightIndex].coordinates)
    self.knights[knightIndex].coordinates = emptySpace
    # swap the actual board places
    self.board[knightSpace[1]][knightSpace[0]] = 'E'
    self.board[emptySpace[1]][emptySpace[0]] = 'K'
    totalConflicts = 0
    if store:
      totalConflicts = self.getTotalConflicts(True)
    if sort:
      self.knights.sort()
    return totalConflicts

  #############################################Testing#############################################
  def testQueen(self, queenIndex: int, freeSpaceIndex: int):
    queenPos = self.queens[queenIndex].getCoordinates()
    freePos = self.freeSpaces[freeSpaceIndex]
    # hide queen
    self.board[queenPos[1]][queenPos[0]] = 'E'
    # change queen to new space
    self.queens[queenIndex].coordinates = freePos
    # place queen Letter position
    self.board[freePos[1]][freePos[0]] = 'Q'
    # getTotalConflicts
    conflicts = self.getQueenConflicts(self.queens[queenIndex])
    # restore queen
    self.queens[queenIndex].coordinates = queenPos
    self.board[queenPos[1]][queenPos[0]] = 'Q'
    self.board[freePos[1]][freePos[0]] = 'E'
    # return TotalConflicts
    return conflicts

  def testKnight(self, knightIndex: int, freeSpaceIndex: int):
    knightPos = self.knights[knightIndex].getCoordinates()
    freePos = self.freeSpaces[freeSpaceIndex]
    # hide knight
    self.board[knightPos[1]][knightPos[0]] = 'E'
    # change knight to new space
    self.knights[knightIndex].coordinates = freePos
    # place knight Letter position
    self.board[freePos[1]][freePos[0]] = 'K'
    # getTotalConflicts
    conflicts = self.getKnightConflicts(self.knights[knightIndex])
    # restore knight
    self.knights[knightIndex].coordinates = knightPos
    self.board[knightPos[1]][knightPos[0]] = 'K'
    self.board[freePos[1]][freePos[0]] = 'E'
    # return TotalConflicts
    return conflicts

  # Given Queen at spot Q,cchecks for conflicts at each spot C 
  # [C][ ][C][ ][C]
  # [ ][C][C][C][ ]
  # [C][C][Q][C][C]
  # [ ][C][C][C][ ]
  # [C][ ][C][ ][C]
  def getQueenConflicts(self,queen: Piece):
    #looping variables
    numH = len(self.board[0])
    numV = len(self.board)
    signs = [-1,1]
    #countingVariables
    conflicts = 0
    origCoords = queen.getCoordinates()
    # check horizontal
    for sign in signs:
      for xOffset in range(1,numH):
        x = origCoords[0] + ( xOffset * sign )
        if x >= numH or x < 0:
          break
        match self.board[origCoords[1]][x]:
          case 'K':
            break
          case 'Q':
            conflicts += 1
            break

    # check vertical
    for sign in signs:
      for yOffset in range(1,numV):
        y = origCoords[1] + ( yOffset * sign )
        if y >= numV or y < 0:
            break
        match self.board[y][origCoords[0]]:
          case 'K':
            break
          case 'Q':
            conflicts += 1
            break

    # check diagonals
    for xSign in signs:
      for xOffset in range(1,numH):
        x = origCoords[0] + (xOffset * xSign)
        if x < 0 or x >= numH:
            continue
        # alternate between -1 and 1
        dontCheck = []
        for ySign in signs:
          y = origCoords[1] + (xOffset * ySign)
          # check to see if we our check will be in the board
          if y < 0 or y >= numV or sign in dontCheck:
            continue
          # check for pieces
          match self.board[y][x]:
            case 'K':
              dontCheck.append(sign)
              break
            case 'Q':
              conflicts += 1
              dontCheck.append(sign)
              break
        if len(signs) == 0:
          break
    return conflicts

  # Given Knight at spot K, checks for conflicts at each spot C 
  # [ ][C][ ][C][ ]
  # [C][ ][ ][ ][C]
  # [ ][ ][K][ ][ ]
  # [C][ ][ ][ ][C]
  # [ ][C][ ][C][ ]
  def getKnightConflicts(self,knight: Piece):
    conflicts = 0
    origCoords = knight.getCoordinates()
    signs = [-1,1]
    for sign in signs:
      y = origCoords[1] + (sign*2)
      x = origCoords[0] + (1)
      if y >= 0 and y < len(self.board) and x < len(self.board[y]):
        if(self.board[y][x] == 'K'):
          conflicts += 1
      
      y = origCoords[1] + (sign)
      x = origCoords[0] + (2)
      if y >= 0 and y < len(self.board) and x < len(self.board[y]):
        if(self.board[y][x] == 'K'):
          conflicts += 1

      y = origCoords[1] + (sign*2)
      x = origCoords[0] - (1)
      if y >= 0 and y < len(self.board) and x < len(self.board[y]) and x >= 0:
        if(self.board[y][x] == 'K'):
          conflicts += 1
          
      y = origCoords[1] + (sign)
      x = origCoords[0] - (2)
      if y >= 0 and y < len(self.board) and x < len(self.board[y]) and x >= 0:
        if(self.board[y][x] == 'K'):
          conflicts += 1
    return conflicts

  def getNumQueensWithConflicts(self):
    totalQueens = 0
    for i in range(len(self.queens)):
      if self.queens[i].conflicts > 0:
        totalQueens = totalQueens + 1
      else:
        break
    return totalQueens

  def getNumKnightsWithConflicts(self):
    totalKnights = 0
    for i in range(len(self.knights)):
      if self.knights[i].conflicts > 0:
        totalKnights = totalKnights + 1
      else:
        break
    return totalKnights

  def getTotalConflictsFast(self):
    return self.getTotalKnightConflictsFast() + self.getTotalQueenConflictsFast()
    
  def getTotalKnightConflictsFast(self):
    totalConflicts = 0
    for i in range(len(self.knights)):
      totalConflicts += self.knights[i].conflicts
    return totalConflicts
    
  def getTotalQueenConflictsFast(self):
    totalConflicts = 0
    for i in range(len(self.queens)):
      totalConflicts += self.queens[i].conflicts
    return totalConflicts

  def getTotalConflicts(self,store:bool):
    totalConflicts = 0
    for i in range(len(self.queens)):
      conflicts = self.getQueenConflicts(self.queens[i])
      if store:
        self.queens[i].conflicts = conflicts
      totalConflicts += conflicts
    for i in range(len(self.knights)):
      conflicts =  self.getKnightConflicts(self.knights[i])
      if store:
        self.knights[i].conflicts = conflicts
      totalConflicts += conflicts
    return totalConflicts

  def getBestQueenPlacement(self, queenIndex):
    bestFreeIndex = -1
    bestScore = self.queens[queenIndex].conflicts
    for i in range(len(self.freeSpaces)):
      tScore = self.testQueen(queenIndex,i)
      if tScore < bestScore:
        bestScore = tScore
        bestFreeIndex = i
    return bestFreeIndex
  
  def getBestKnightPlacement(self, knightIndex):
    bestFreeIndex = -1
    bestScore = self.knights[knightIndex].conflicts
    for i in range(len(self.freeSpaces)):
      tScore = self.testKnight(knightIndex,i)
      if tScore < bestScore:
        bestScore = tScore
        bestFreeIndex = i
    return bestFreeIndex

  ############################################Reporting############################################

  def __str__(self):
    outString = ''
    for row in self.board:
      rowString = ' '
      rowString = rowString.join(row)
      outString += rowString +'\n'
    outString += str(self.getTotalConflicts(True))
    return outString

  ##############################################DEBUG##############################################
  def debug_printQueensConflicts(self):
    outString = ''
    for piece in self.queens:
      outString += str(piece.conflicts) +', '
    outString += "\n"
    return outString

  def debug_GetFreeSpaces(self):
    self.freeSpaces.sort(key=lambda tup: tup[0])
    return self.freeSpaces

  def debug_GetPieces(self,array):
    outString = ''
    for piece in array:
      outString += str(piece) +'\n'
    return outString

  def debug_print(self):
    print("Board:")
    print(self)
    print("Free Spaces:")
    print(self.debug_GetFreeSpaces())
    print("Queens:")
    print(self.debug_GetPieces(self.queens))
    print("Knights:")
    print(self.debug_GetPieces(self.knights))
