from board import Board
from hc import processHC
from sa import processSA
from other import processOther
import sys
import datetime
from copy import deepcopy
import time
numQueens = 0
numKnights = 0

methods = {}
methodList = []
#methodList.append("HC")
#methodList.append("SA")
methodList.append("Other")
for method in methodList:
  print(method)
  methods[method] = {}
  for size in [5,10,15,20]:
    methods[method][size] = {}
    for piece in ['q','k','q+k']:
      match piece:
        case 'q':
          numKnights = 0
          numQueens = size
        case 'k':
          numKnights = size
          numQueens = 0
        case 'q+k':
          numKnights = size
          numQueens = size
      times = []
      for i in range( 10 ):
        b1 = Board(size,size,numKnights,numQueens)
        startTime = datetime.datetime.now()
        match method:
          case 'HC':
            bestBoard = processHC(b1,120)
          case 'SA':
            bestBoard = processSA(b1,120)
          case 'Other':
            bestBoard = processOther(b1,120)
        curTime = datetime.datetime.now()
        deltaS = (curTime - startTime).seconds
        deltaMS = (curTime - startTime).microseconds * 1000
        times.append(float(str(deltaS) + '.' + str(deltaMS)))
        print(str(size) + " " + piece + " " + str(deltaS) + '.' + str(deltaMS))
      methods[method][size][piece] = deepcopy(times)
print("done")
for method in methods:
  print("method: " + str(method))
  for size in methods[method]:
    print("  Size: " + str(size))
    for piece in methods[method][size]:
      avg = 0
      for i in methods[method][size][piece]:
        avg += i
      avg = avg/len(methods[method][size][piece])
      print("    " + str(piece) + ": " + str(avg))

exit(0)

text_file = open(fileName, "w")
text_file.write(str(bestBoard))
text_file.close()
print(bestBoard)
curTime = datetime.datetime.now()
deltaS = (curTime - startTime).seconds
deltaMS = (curTime - startTime).microseconds * 1000
print('runtime: ' + str(deltaS) + "." + str(deltaMS))