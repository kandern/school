import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier
from sklearn.preprocessing import LabelEncoder
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.ensemble import VotingClassifier
import sklearn
import math
import sys

import time
import os.path

def main(filepath,method):

  
  #read in the data using pandas
  df = pd.read_csv(filepath,delimiter=";")
  df = sanatize(df)
  X = df.drop(columns=['y'])
  Y = df['y'].values
  XTrain, XTest, YTrain, YTest = train_test_split(X, Y, test_size=0.2, stratify=Y)

  if method == "knn":
    return (knn(XTrain,YTrain,XTest,YTest))
  if method == "dt":
    return (dt(XTrain,YTrain,XTest,YTest))
  if method == "rf":
    return (rf(XTrain,YTrain,XTest,YTest))
  if method == "vc":
    return (sc(XTrain,YTrain,XTest,YTest))
  return "invalid method"

def knn(XTrain,YTrain,XTest,YTest):
  knn = KNeighborsClassifier(n_neighbors = 31,n_jobs=-1)
  knn.fit(XTrain,YTrain)
  return knn.score(XTest, YTest)


def dt(XTrain,YTrain,XTest,YTest):
  dt = DecisionTreeClassifier(max_depth=13,min_samples_split=19,criterion="entropy",)
  dt.fit(XTrain, YTrain)
  return dt.score(XTest,YTest)

def rf(XTrain,YTrain,XTest,YTest):
  dt = RandomForestClassifier(n_estimators=53)
  dt.fit(XTrain, YTrain)
  return dt.score(XTest,YTest)

def sc(XTrain,YTrain,XTest,YTest):
  classifiers = [("kn", RandomForestClassifier(n_estimators=53, criterion='entropy',min_samples_split=19,max_depth=14)),
                 ("rf", GradientBoostingClassifier())]
  sc = sklearn.ensemble.VotingClassifier(estimators=classifiers,weights=[.5,1])
  sc.fit(XTrain, YTrain)
  return sc.score(XTest,YTest)

def sanatize(df):
  df['job'] = LabelEncoder().fit_transform(df.job.values)
  df['marital'] = LabelEncoder().fit_transform(df.marital.values)
  df['education'] = LabelEncoder().fit_transform(df.education.values)
  df['default'] = LabelEncoder().fit_transform(df.default.values)
  df['housing'] = LabelEncoder().fit_transform(df.housing.values)
  df['loan'] = LabelEncoder().fit_transform(df.loan.values)
  df['contact'] = LabelEncoder().fit_transform(df.contact.values)
  df['month'] = LabelEncoder().fit_transform(df.month.values)
  df['poutcome'] = LabelEncoder().fit_transform(df.poutcome.values)
  return df

print(main(str(sys.argv[1]),str(sys.argv[2])))