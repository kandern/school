import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsRegressor
from sklearn.tree import DecisionTreeRegressor
from sklearn.ensemble import RandomForestRegressor
from sklearn.ensemble import StackingRegressor
from sklearn.ensemble import VotingRegressor
from sklearn.linear_model import LinearRegression
import time
import os.path
import sys

def main(filepath,method):
  #read in the data using pandas
  df = pd.read_csv(filepath,delimiter=" ")
  #df = sanatize(df)
  X = df.drop(columns=['d'])
  Y = df['d'].values
  XTrain, XTest, YTrain, YTest = train_test_split(X, Y, test_size=0.2)

  if method == "knn":
    return (knn(XTrain,YTrain,XTest,YTest))
  if method == "dt":
    return (dt(XTrain,YTrain,XTest,YTest))
  if method == "rf":
    return (rf(XTrain,YTrain,XTest,YTest))
  if method == "vc":
    return (sc(XTrain,YTrain,XTest,YTest))
  return "invalid method"


def knn(XTrain,YTrain,XTest,YTest):
  knn = KNeighborsRegressor(n_neighbors = 4)
  knn.fit(XTrain,YTrain)
  return knn.score(XTest, YTest)


def dt(XTrain,YTrain,XTest,YTest):
  dt = DecisionTreeRegressor(max_depth=15)
  dt.fit(XTrain, YTrain)
  return dt.score(XTest,YTest)

def rf(XTrain,YTrain,XTest,YTest):
  dt = RandomForestRegressor(n_estimators=750, criterion='squared_error',max_depth=34)
  dt.fit(XTrain, YTrain)
  return dt.score(XTest,YTest)


def sc(XTrain,YTrain,XTest,YTest):
  classifiers = [("kn", KNeighborsRegressor(n_neighbors = 4,n_jobs=-1)),
                 ("dc", DecisionTreeRegressor(criterion='squared_error',splitter="best",max_depth=34)),
                 ("dcd", DecisionTreeRegressor(criterion='friedman_mse',splitter="best",max_depth=34))]
  sc = VotingRegressor(classifiers,weights=[1,.1,.14])
  sc.fit(XTrain, YTrain)
  return sc.score(XTest,YTest)

def sanatize(df):
  return df

print(main(str(sys.argv[1]),str(sys.argv[2])))