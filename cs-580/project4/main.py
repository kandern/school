from sklearn.neural_network import MLPClassifier
import pandas as pd
import numpy as np
from sklearn.ensemble import RandomForestClassifier
import sys
import tensorflow as tf
from tensorflow.keras.models import Sequential, load_model
from tensorflow.keras.layers import Dense
from sklearn.metrics import accuracy_score
from sklearn.preprocessing import LabelEncoder


def main(method):
  testFile = "test.csv"
  trainFile = "train.csv"
  #read in the data using pandas
  df = pd.read_csv(trainFile,delimiter=",")
  df = sanatize(df)
  XTrain = df.drop(columns=['lettr'])
  YTrain = df['lettr'].values
  df = pd.read_csv(testFile,delimiter=",")
  df = sanatize(df)
  XTest = df.drop(columns=['lettr'])
  YTest = df['lettr'].values

  if method == "pc":
    return (pc(XTrain,YTrain,XTest,YTest))
  if method == "tf":
    return (tff(XTrain,YTrain,XTest,YTest))
  if method == "rf":
    return (rf(XTrain,YTrain,XTest,YTest))


def tff(XTrain,YTrain,XTest,YTest):
  best = (-1,-1)
  return doTF(XTrain,YTrain,XTest,YTest)

def doTF(XTrain,YTrain,XTest,YTest):
  num_hidden_layers = 12
  numUnits = 200
  activationMethod = 'gelu'
  epochs = 199
  model = Sequential()
  model.add(Dense(units=numUnits, activation=activationMethod, input_dim=len(XTrain.columns)))
  for x in range(num_hidden_layers):
    model.add(Dense(units=numUnits, activation=activationMethod))
  model.add(Dense(units=26, activation='sigmoid'))
  model.compile(loss=tf.keras.losses.SparseCategoricalCrossentropy(), optimizer='adam', metrics='accuracy')
  model.fit(XTrain, YTrain, epochs=epochs, batch_size=32)
  return model.evaluate(XTest,YTest)[1]

def pc(XTrain,YTrain,XTest,YTest):
  dt = MLPClassifier(random_state=1,
                    max_iter=1000,
                    hidden_layer_sizes=(705,800),
                    activation='tanh').fit(XTrain, YTrain)
  dt.predict_proba(XTest)
  dt.predict(XTest)
  score = dt.score(XTest,YTest)
  return score

def rf(XTrain,YTrain,XTest,YTest):
  dt = RandomForestClassifier(random_state=1,
                              n_estimators=801,
                              criterion='gini',
                              max_depth=31,
                              min_samples_split=2,
                              min_samples_leaf=1)
  dt.fit(XTrain, YTrain)
  score = dt.score(XTest,YTest)
  return score

def sanatize(df):
  df['lettr'] = LabelEncoder().fit_transform(df.lettr.values)
  return df

print(main(str(sys.argv[1])))