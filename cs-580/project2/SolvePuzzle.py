from board import Board
from BFS import processBFS
from DFS import processDFS
from AStar import processAStar
import sys
import datetime
import re
from random import randrange
import cProfile
startTime = datetime.datetime.now()
if len(sys.argv) != 5:
    print("usage: python SolveProblem.py problem.txt method tmax solution.txt")
    exit(1)

problemFile = sys.argv[1]
methodName = sys.argv[2]
tMax = int(sys.argv[3])
fileName = sys.argv[4]
def main():
  regex = re.compile("(\d+) (\d+)")
  text_file = open(problemFile, "r")

  #read whole file to a string
  lines = text_file.readlines()
  matches = regex.match(lines[0])
  N = int(matches.group(1))
  M = int(matches.group(2))
  lines.pop(0)
  lines.pop(0)
  problem = "".join(lines[:N])
  goal = "".join(lines[N+1:])
  boardArr = re.sub("\s+",",",problem.strip()).split(",")
  goalArr  = re.sub("\s+",",",goal.strip()).split(",")
  goals = []
  for i in range(N):
    goals.append([])
    for j in range(M):
      goals[i].append(int(goalArr[i*M+j]))
  solution = ""
  match methodName:
    case 'BFS':
      b = Board.fromString(N,M,boardArr,goalArr,0)
      solution = processBFS(b,tMax,goals)
    case 'DFS':
      b = Board.fromString(N,M,boardArr,goalArr,0)
      solution = processDFS(b,tMax,goals)
    case 'AStarH0':
      b = Board.fromString(N,M,boardArr,goalArr,0)
      solution = processAStar(b,tMax,goals)
    case 'AStarH1':
      b = Board.fromString(N,M,boardArr,goalArr,1)
      solution = processAStar(b,tMax,goals)
    case 'AStarH2':
      b = Board.fromString(N,M,boardArr,goalArr,2)
      solution = processAStar(b,tMax,goals)

  text_file = open(fileName, "w")
  text_file.write(solution)
  text_file.close()
  print(solution)
  curTime = datetime.datetime.now()
  delta = (curTime - startTime).seconds
  print('runtime: ' + str(delta) + 's')
#cProfile.run('main()')
main()