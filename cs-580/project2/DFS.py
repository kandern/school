from board import Board
from copy import deepcopy
import datetime
import time

def processDFS(b: Board, timeLimit: int,goal):
  startTime = datetime.datetime.now()
  visited = []
  boardStack = []
  nextStack = []
  curBoard = b
  itrsPerLevel = 5
  curItrLevel = 1
  # if we ran out of time, return and print our failed attempt
  while curBoard.heuristic(goal) > 0 and (datetime.datetime.now() - startTime).seconds < timeLimit:
    print(len(curBoard.moveList))
    if hash(curBoard) in visited:
      if len(boardStack) == 0:
        if len(nextStack) == 0:
          break
        else:
          boardStack = nextStack
          nextStack = []
          curItrLevel += 1
          #print(curItrLevel*itrsPerLevel)
      curBoard = boardStack.pop()
      continue
    visited.append(hash(curBoard))
    # Add our children to the stack
    for direction in curBoard.getMoves():
      newB = Board.copy(curBoard)
      if newB.move(direction):
        if len(curBoard.moveList) >= itrsPerLevel*curItrLevel:
          nextStack.append(newB)
        else:
          boardStack.append(newB)
    if len(boardStack) == 0:
      boardStack = nextStack
      nextStack = []
      curItrLevel += 1
      curBoard = boardStack.pop()
  if curBoard.heuristic(goal) != 0:
    return "failed"
  return "\n".join(curBoard.moves)