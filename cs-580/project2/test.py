from board import Board
from BFS import processBFS
from DFS import processDFS
from AStar import processAStar
import sys
import datetime
import re
from random import randrange
from copy import deepcopy
import os
tmax = 120

def _main(N,M,numWalls,numItr,methodName,tMax):
  print("creatingProblem")
  os.system("python.exe createProblem.py " + str(N) + " " + str(M) + " " + str(numWalls) + " " + str(numItr))
  print("solvingProblem")
  startTime = datetime.datetime.now()
  os.system("python.exe SolvePuzzle.py problem.txt " + methodName + " "+ str(tMax) + " solution.txt")
  endTime = datetime.datetime.now()
  deltaTime = (endTime - startTime)
  delta = float(str(deltaTime.seconds) + "." + str(deltaTime.microseconds*1000))
  #check for failure
  text_file = open("solution.txt", "r")
  data = text_file.read()
  if data == "failed":
    print("failed")
  return delta

if len(sys.argv) != 7:
  print("usage: python test.py N M numWalls numItr methodName tMax")
  exit(1)

inN = int(sys.argv[1])
inM = int(sys.argv[2])
inNumWalls = sys.argv[3]
inNumItr = sys.argv[4]
inMethod = sys.argv[5]
inMax = int(sys.argv[6])
tots = 0
numRuns = 1

for i in range(int(numRuns)):
  tots += _main(inN,inM,inNumWalls,inNumItr,inMethod,inMax)
print(tots/numRuns)