from board import Board
import datetime
from bisect import insort
from bisect import insort_left
from bisect import bisect_left
import time

closedSet = []# hashes of completed nodes
openSet = []# actual boards that we will look at
gScore = {}# dictionary of hashes to scores
fScore = {}# dicctionary of hashes to scores
def processAStar(b: Board, timeLimit: int,goal):
  startTime = datetime.datetime.now()
  u = b
  gScore[hash(b)] = 0
  fScore[hash(b)] = b.heuristic(goal)
  openSet.append(b)
  b.f = b.heuristic(goal)
  while 1 > 0:
    # extract node with min fScore
    u = openSet.pop(0)
    if u.heuristic(goal) == 0:
      return "\n".join(u.moves)
    u.heuristic(goal)
    insort(closedSet,hash(u))
    for v in getNeighbors(u):
      if inClosed(hash(v)):
        continue
      g = getGscore(u) + u.heuristic(goal)
      if not inOpenSet(v):
        if v.heuristic(goal) == -1:
          print(v)
          exit
        insort(openSet,v)
      elif g > getGscore(v):
        continue
      gScore[hash(v)] = g
      fScore[hash(v)] = gScore[hash(v)] + v.heuristic(goal)

    # if we ran out of time, return and print our failed attempt
    curTime = datetime.datetime.now()
    delta = (curTime - startTime).seconds
    #Quit if taking too long
    if delta >= timeLimit:
      break
  if u.heuristic(goal) != 0:
    return "failed"
  return "\n".join(u.moves)

def inClosed(hash):
  posI = bisect_left(closedSet,hash)
  return not posI == len(closedSet) and closedSet[posI] == hash

def inOpenSet(board):
  posI = bisect_left(openSet,board)
  return not posI == len(openSet) and openSet[posI] == board


def getNeighbors(board):
  neighbors = []
  for direction in board.getMoves():
    newB = Board.copy(board)
    newB.move(direction)
    neighbors.append(newB)
  return neighbors

def getGscore(board):
  if hash(board) not in gScore:
    gScore[hash(board)] = float('inf')
  return gScore[hash(board)]

def getFscore(board):
  if hash(board) not in gScore:
    gScore[hash(board)] = float('inf')
  return gScore[hash(board)]