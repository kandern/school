from board import Board
from copy import deepcopy
import datetime
import time

def processBFS(b: Board, timeLimit: int,goal):
  startTime = datetime.datetime.now()
  visited = []
  boardStack = []
  curBoard = b
  bestH = 0
  # if we ran out of time, return and print our failed attempt
  while curBoard.heuristic(goal) > 0 and (datetime.datetime.now() - startTime).seconds < timeLimit:
    if hash(curBoard) in visited:
      if len(boardStack) == 0 :
        break
      curBoard = boardStack.pop(0)
      continue
    visited.append(hash(curBoard))
    # Add our children to the stack
    for direction in curBoard.getMoves():
      newB = Board.copy(curBoard)
      newB.move(direction)
      if newB.heuristic(goal) == 0:
        return "\n".join(newB.moves)
      boardStack.append(newB)
    curBoard = boardStack.pop(0)
  if curBoard.heuristic(goal) != 0:
    return "failed"
  return "\n".join(curBoard.moves)