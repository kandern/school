from copy import deepcopy
from copy import copy
import random
from math import ceil
from math import floor

class Board:
  def __init__(self, b,newMethod,newPos = (-1,-1)):
    self.board = b
    #self.goals = deepcopy(goals)
    self.rows = len(self.board)
    self.columns = len(self.board[0])
    self.moves = []
    self.moveList = []
    self.method = newMethod
    self.hash = 0
    self.h = -1
    self.f = -1
    if newPos[0] == -1:
      for i in range(self.rows):
        for j in range(self.columns):
          if self.board[i][j] == 0:
            self.pos = (i,j)
    else:
      self.pos = newPos
    self.lastMove = 'X'

  def __eq__(self,other):
    return hash(self) == hash(other)

  def __lt__(self,other):
    if self.f == -1 or other.f == -1:
      return self.h < other.h
    else:
      return self.f < other.f

  def __hash__(self):
    if self.hash == 0:
      self.hash = hash(self.getMappings())
    return self.hash

  def heuristic(self,goals):
    if self.h != -1:
      return self.h
    match self.method:
      case 0:
        return self.h0(goals)
      case 1:
        return self.h1(goals)
      case 2:
        return self.h2(goals)

  def h0(self,goal):
    return min(1,self.h1(goal))

  def h1(self,goal):
    numWrong = 0
    for row in range(len(goal)):
      for column in range(len(goal[0])):
        if self.board[row][column] != goal[row][column]:
          numWrong +=1
    return numWrong

  def h2(self,goal):
    score = 0
    for row in range(len(goal)):
      for column in range(len(goal[0])):
        score += self.getManhattan(row,column,goal[row][column])
    return score

  def getManhattan(self,row,column,value):
    if self.board[row][column] == value:
      return 0
    for i in range(1,max(len(self.board),len(self.board[0]))):
      #Check Left:
      col = column-i
      if col>=0:
        for j in range(-i,i+1):
          if row+j >=0 and row+j < len(self.board):
            if self.board[row+j][col] == value:
              return i+j
      #Check Right:
      col = column+i
      if col < len(self.board[0]):
        for j in range(-i,i+1):
          if row+j >=0 and row+j < len(self.board):
            if self.board[row+j][col] == value:
              return i+j
      #Check Top:
      r = row-i
      if r >= 0:
        for j in range(-i+1,i):
          if column + j >= 0 and column + j < len(self.board[0]):
            if self.board[r][column + j] == value:
              return i+j
      #Check Bottom:
      r = row+i
      if r < len(self.board):
        for j in range(-i+1,i):
          if column + j >= 0 and column + j < len(self.board[0]):
            if self.board[r][column + j] == value:
              return i+j

  # We are assuming that the string given in the string are row major form.
  def fromString(N,M,boardStr,goalStr,newMethod):
    board = []
    for i in range(N):
      board.append([])
      for j in range(M):
        board[i].append(int(boardStr[i*M+j]))
    goals = []
    for i in range(N):
      goals.append([])
      for j in range(M):
        goals[i].append(int(goalStr[i*M+j]))
    b = Board(board,newMethod)
    # stored in row, column, instead of x,y
    b.pos = (int(boardStr.index('0')/len(board[0])),int(boardStr.index('0')%len(board[0])))
    b.moves.append(str(b.pos))
    return b

  def copy(self):
    newBoard = []
    for i in range(self.rows):
      newBoard.append(copy(self.board[i]))
    newB = Board(newBoard,self.method, newPos=(self.pos[0],self.pos[1]))
    newB.moves = copy(self.moves)
    newB.moveList = copy(self.moveList)
    newB.lastMove = self.lastMove
    return newB

  def get(self, N:int, M:int):
    return self.board[N][M]

  def applyMoves(self,directions):
    for direction in directions:
      self.move(direction)


  # accepts N,E,S,W
  def move(self,direction: str):
    success = False
    newPos = self.pos
    match direction:
      case 'N':
        if self.pos[0] > 0 and self.canMove(direction):
          success = True
          newPos = (self.pos[0]-1,self.pos[1])
      case 'E':
        if self.pos[1] < self.columns-1 and self.canMove(direction):
          success = True
          newPos = (self.pos[0],self.pos[1]+1)
      case 'S':
        if self.pos[0] < self.rows-1 and self.canMove(direction):
          success = True
          newPos = (self.pos[0]+1,self.pos[1])
      case 'W':
        if self.pos[1] > 0 and self.canMove(direction):
          success = True
          newPos = (self.pos[0],self.pos[1]-1)
    if success:
      tEmpty = self.board[self.pos[0]][self.pos[1]]
      self.board[self.pos[0]][self.pos[1]] = self.board[newPos[0]][newPos[1]]
      self.board[newPos[0]][newPos[1]] = tEmpty
      self.pos = newPos
      self.moves.append(str(newPos))
      self.lastMove = direction
      self.moveList.append(direction)

      # set new heuristic:
      
    return success

  def canMove(self,direction):
    blocked = False
    match direction:
      case 'N':
        if self.pos[0] <= 0\
        or self.lastMove == 'S'\
        or self.board[self.pos[0]-1][self.pos[1]] == -1:
          blocked = True
      case 'E':
        if self.pos[1] >= self.columns-1\
        or self.lastMove == 'W'\
        or self.board[self.pos[0]][self.pos[1]+1] == -1:
          blocked = True
      case 'S':
        if self.pos[0] >= self.rows-1\
        or self.lastMove == 'N'\
        or self.board[self.pos[0]+1][self.pos[1]] == -1:
          blocked = True
      case 'W':
        if self.pos[1] <= 0\
        or self.lastMove == 'E'\
        or self.board[self.pos[0]][self.pos[1]-1] == -1:
          blocked = True
    return not blocked

  def getMoves(self):
    directions = []
    if self.canMove('N'):
      directions.append('N')

    if self.canMove('S'):
      directions.append('S')

    if self.canMove('E'):
      directions.append('E')

    if self.canMove('W'):
      directions.append('W')

    random.shuffle(directions)
    return directions

#Printing*******************************************************************************************
  def __str__(self):
    maxValue = 0
    boardStrings = []
    for i in range(self.rows):
      for j in range(self.columns):
        maxValue = max(maxValue,self.board[i][j])
    for i in range(self.rows):
      rowStrings = []
      for j in range(self.columns):
        rowStrings.append(str(self.board[i][j]).rjust(len(str(maxValue))))
        maxValue = max(maxValue,self.board[i][j])
      boardStrings.append(" ".join(rowStrings))
    return "\n".join(boardStrings)

  def printGoals(goals):
    maxValue = 0
    boardStrings = []
    rows = len(goals)
    columns = len(goals[0])
    for i in range(rows):
      for j in range(columns):
        maxValue = max(maxValue,goals[i][j])
    for i in range(rows):
      rowStrings = []
      for j in range(columns):
        rowStrings.append(str(goals[i][j]).rjust(len(str(maxValue))))
        maxValue = max(maxValue,goals[i][j])
      boardStrings.append(" ".join(rowStrings))
    print("\n".join(boardStrings))

  def getMappings(self):
    boardStrings = []
    for i in range(self.rows):
      boardStrings.append(str(self.board[i]))
    return "".join(boardStrings)

  def print_debug(self):
    print(self)
    print('-------------------------')
    print(self.getMappings())
    print("h: " + str(self.h))
    print("method: " + str(self.method))

  def isGood(self,goal):
    good = 0
    for i in range(self.rows):
      for j in range(self.columns):
        if self.board[i][j] != goal[i][j]:
          print((i,j))
          good +=1
    return good
