
class Piece:
  def __init__(self,value=0):
    self.value = value
    self.goal = (-1,-1)
  def __str__(self):
    return str(self.value)
  def getDistance(self,N,M):
    return abs(self.goal[0]-N) + abs(self.goal[1]-M)
  def heuristic(self,N,M,method = 2):
    match method:
      case 0:
        return min(1,self.getDistance(N,M))
      case 1:
        return max(1,self.getDistance(N,M))
      case 2:
        return self.getDistance(N,M)
    return min(1,self.getDistance(N,M))