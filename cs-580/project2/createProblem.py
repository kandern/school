from board import Board
import sys
import datetime
import re
from random import randrange

def makeBoard(N,M,numWalls,numItr,fileName):
  outString = str(inN) + " " + str(inM) + "\n\n"
  prob = []
  for i in range(int(N)):
    for j in range(int(M)):
      prob.append(str(randrange(1,N*M)))

  # place walls
  wallsPlaced = 0
  while wallsPlaced < int(numWalls):
    for i in range(int(N)):
      for j in range(int(M)):
        if prob[i*M+j] != '-1' and prob[i*M+j] != '0':
          randomNum = randrange(0,100)
          if randomNum <= 10:
            prob[i*M+j] = '-1'
            wallsPlaced += 1
  prob[0]= "0"
  probStr = ""
  for i in range(int(N)):
    for j in range(int(M)):
      probStr += prob[i*M+j] + " "
    probStr += "\n"
  boardArr = re.sub("\s+",",",probStr.strip()).split(",")
  b = Board.fromString(N,M,boardArr,boardArr,0)
  goalStr = str(b) + "\n\n"

  itrs = 0
  while itrs < int(numItr):
    moves = b.getMoves()
    if(len(moves) == 0):
      return makeBoard(N,M,numWalls,numItr,fileName)
    x = randrange(len(moves))
    if b.canMove(moves[x]):
      itrs += 1
      b.move(moves[x])

  outString += str(b) + "\n\n"
  outString += goalStr
  for i in b.moves:
    outString += i + "\n"


  text_file = open(fileName, "w")
  text_file.write(outString)
  text_file.close()

if len(sys.argv) != 5:
    print("usage: python createProblem.py N M numWalls numItr")
    exit(1)

inN = int(sys.argv[1])
inM = int(sys.argv[2])
inNumWalls = sys.argv[3]
inNumItr = sys.argv[4]
inFileName = "problem.txt"
makeBoard(inN,inM,inNumWalls,inNumItr,inFileName)


def makeBoard2(N,M,numWalls,numItr,fileName):
  outString = str(inN) + " " + str(inM) + "\n\n"
  prob = []
  for i in range(int(N)):
    for j in range(int(M)):
      prob.append(str(randrange(N*M)))

  # place walls
  wallsPlaced = 0
  while wallsPlaced < int(numWalls):
    for i in range(int(N)):
      for j in range(int(M)):
        if prob[i*M+j] != '-1' and prob[i*M+j] != '0':
          randomNum = randrange(0,100)
          if randomNum <= 10:
            prob[i*M+j] = '-1'
            wallsPlaced += 1
  probStr = ""
  for i in range(int(N)):
    for j in range(int(M)):
      probStr += prob[i*M+j] + " "
    probStr += "\n"
  boardArr = re.sub("\s+",",",probStr.strip()).split(",")
  b = Board.fromString(N,M,boardArr,boardArr,0)
  goalStr = str(b) + "\n\n"

  itrs = 0
  while itrs < int(numItr):
    moves = b.getMoves()
    if(len(moves) == 0):
      return makeBoard(N,M,numWalls,numItr,fileName)
    x = randrange(len(moves))
    if b.canMove(moves[x]):
      itrs += 1
      b.move(moves[x])

  outString += str(b) + "\n\n"
  outString += goalStr
  for i in b.moves:
    outString += i + "\n"


  text_file = open(fileName, "w")
  text_file.write(outString)
  text_file.close()

if len(sys.argv) != 5:
    print("usage: python createProblem.py N M numWalls numItr")
    exit(1)

inN = int(sys.argv[1])
inM = int(sys.argv[2])
inNumWalls = sys.argv[3]
inNumItr = sys.argv[4]
inFileName = "problem.txt"
makeBoard(inN,inM,inNumWalls,inNumItr,inFileName)