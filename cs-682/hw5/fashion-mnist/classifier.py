import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np

NUM_CLASSES = 10

alg = 3

class Classifier(nn.Module):
    def __init__(self):
        super(Classifier, self).__init__()
        if alg == 1:
            self.base()
        if alg == 2:
            self.Two()
        if alg == 3:
            self.Three()

    def forward(self, x):
        if alg == 1:
            return self.forwardBase(x)
        if alg == 2:
            return self.forwardTwo(x)
        if alg == 3:
            return self.forwardThree(x)

##############SETUP Fcns##########################################
    def base(self):
        # Base
        self.fc1 = nn.Linear(28 * 28, 256)
        self.fc2 = nn.Linear(256, NUM_CLASSES)

    def Two(self):
        # convolutional NN
        self.conv1 = nn.Conv2d(1, 64, 5)
        self.conv2 = nn.Conv2d(64, 32, 3)
        self.conv3 = nn.Conv2d(32, 16, 3)
        self.pool = nn.MaxPool2d(2, 2)
        self.fc1 = nn.Linear(16 * 10 * 10, 120)
        self.fc2 = nn.Linear(120, 256)
        self.fc3 = nn.Linear(256, NUM_CLASSES)

    def Three(self):
        # convolutional NN
        self.conv1 = nn.Conv2d(1, 32, 3)
        self.bn1 = nn.BatchNorm2d(32)
        self.conv2 = nn.Conv2d(32, 64, 3)
        self.bn2 = nn.BatchNorm2d(64)
        self.conv3 = nn.Conv2d(64, 128, 3)
        self.bn3 = nn.BatchNorm2d(128)
        self.pool = nn.MaxPool2d(2, 2)
        self.dropout1 = nn.Dropout2d(p=0.25)
        self.fc1 = nn.Linear(128 * 5 * 5, 256)
        self.bn4 = nn.BatchNorm1d(256)
        self.dropout2 = nn.Dropout(p=0.5)
        self.fc2 = nn.Linear(256, NUM_CLASSES)

##############Forward Fcns##########################################
    def forwardBase(self, x):
        # [64, 1, 28, 28]
        B = x.shape[0]
        # [64, 1, 28, 28]
        x = x.view(B, -1)
        # [64, 784]
        x = F.relu(self.fc1(x))
        # [64, 256]
        x = self.fc2(x)
        # [64, 10]
        return x
    
    def forwardTwo(self, x):
        # [64, 1, 28, 28]
        x = F.relu(self.conv1(x))
        # [64, 64, 24, 24]
        x = F.relu(self.conv2(x))
        # [64, 32, 22, 22]
        x = F.relu(self.conv3(x))
        # [64, 16, 20, 20]
        # Apply fully connected layers
        x = self.pool(x)
        # [64, 16, 10, 10]
        x = torch.flatten(x, start_dim=1)
        # [64, 120]
        x = self.fc1(x)
        # [64, 120]
        x = self.fc2(x)
        # [64, 256]
        x = self.fc3(x)
        # [64, 10]
        return x

    def forwardThree(self, x):
        # [64, 1, 28, 28]
        x = self.conv1(x)
        # [64, 32, 26, 26]
        x = self.bn1(x)
        # [64, 32, 26, 26]
        x = F.relu(x)
        # [64, 32, 26, 26]
        x = self.conv2(x)
        # [64, 64, 24, 24]
        x = self.bn2(x)
        # [64, 64, 24, 24]
        x = F.relu(x)
        # [64, 64, 24, 24]
        x = self.conv3(x)
        # [64, 128, 22, 22]
        x = self.bn3(x)
        # [64, 128, 22, 22]
        x = F.relu(x)
        # [64, 128, 22, 22]
        x = self.pool(x)
        # [64, 128, 11, 11]
        x = self.dropout1(x)
        # [64, 128, 11, 11]
        x = self.pool(x)
        # [50, 128, 5, 5]
        x = torch.flatten(x, start_dim=1)
        # [64, 3200]
        x = self.fc1(x)
        # [50, 256]
        x = self.bn4(x)
        # [50, 256]
        x = F.relu(x)
        # [50, 256]
        x = self.dropout2(x)
        # [50, 256]
        x = self.fc2(x)
        # [50, 10]
        return x
