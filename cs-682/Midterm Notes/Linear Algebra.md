1.  Vector Addition: If two vectors u and v are of the same dimension, then their sum u + v is obtained by adding their corresponding components.
2.  Scalar Multiplication: If a is a scalar and v is a vector, then their product av is obtained by multiplying each component of v by a.
3.  Dot Product: Given two vectors u and v, their dot product is defined as the sum of the products of their corresponding components. This operation is useful for computing angles between vectors, projections of one vector onto another, and distances between points in Euclidean space.
4.  Matrix Multiplication: Given two matrices A and B, their product AB is defined as the matrix whose (i,j)-th entry is the dot product of the i-th row of A and the j-th column of B. This operation is useful for transforming vectors and for solving systems of linear equations.
5.  Transpose: Given a matrix A, its transpose A^T is obtained by interchanging its rows and columns. This operation is useful for computing orthogonal projections and for solving systems of linear equations.
6.  Determinant: Given a square matrix A, its determinant |A| is a scalar that encodes information about the matrix's invertibility, orientation, and scaling factor. This operation is useful for computing eigenvalues and eigenvectors.
7.  Inverse: Given a square matrix A, its inverse $A^{-1}$ is a matrix that satisfies $A*A^-1 = A^{-1}*A = I$, where $I$ is the identity matrix. This operation is useful for solving systems of linear equations, computing projections, and computing eigenvalues and eigenvectors.
	1. $A^{-1} = 1/det(A) * adj(A)$
	2. $det(A) = determinante(A)$
	3. $adj(A) = adjugate(A)$
8.  Eigenvalues and Eigenvectors: Given a square matrix A, an eigenvector v and its corresponding eigenvalue λ satisfy the equation Av = λv. This operation is useful for understanding the behavior of linear transformations and for performing dimensionality reduction.
   In the context of eigenvalues and eigenvectors, "v" typically refers to the eigenvector that corresponds to a particular eigenvalue.
   
   Given a square matrix A, an eigenvector v and its corresponding eigenvalue λ satisfy the equation Av = λv. The eigenvector v is a non-zero vector that points in the direction that is scaled by the matrix A when multiplied by λ. In other words, the eigenvector remains in the same direction after being multiplied by A, but it is scaled by a factor of λ.
   
   The eigenvectors and eigenvalues of a matrix are useful for understanding the behavior of linear transformations, for example, in the context of principal component analysis (PCA) or image compression.
9.  Singular Value Decomposition (SVD): Given a matrix A, its SVD is a factorization of the form A = UΣV^T, where U and V are orthogonal matrices and Σ is a diagonal matrix with nonnegative entries called singular values. This operation is useful for matrix compression, image reconstruction, and feature extraction.
10.  Convolution: Given a matrix A (e.g., an image) and a matrix B (e.g., a filter), their convolution A * B is a matrix whose (i,j)-th entry is the dot product of the submatrix of A centered at (i,j) and the matrix B. This operation is useful for detecting features in images, such as edges and textures.
11. Cross product is another linear algebra operation, but it is not commonly used in computer vision. The cross product is defined only for 3-dimensional vectors and it results in a vector that is orthogonal to the two input vectors. It is commonly used in physics and engineering, particularly for calculating torque, angular momentum, and the normal vector to a plane. In computer vision, however, the cross product is not typically used because images and feature vectors are represented as 2-dimensional or n-dimensional vectors, not 3-dimensional vectors. The dot product, matrix multiplication, and other linear algebra operations are more commonly used in computer vision because they can operate on vectors and matrices of arbitrary dimensionality.
12. 2 1D filters to 1 2D filter.$$\begin{bmatrix}4& 5& 6\end{bmatrix} \times \begin{bmatrix}1\\2\\3\end{bmatrix} = \begin{bmatrix}4&5&6\\8&10&12\\12&15&18\end{bmatrix}$$
13. $trace(matrix A) = sum(a_{11},a_{22},...,a_{nn})$
14. ![[Pasted image 20230410194142.png]]
15. ![[Pasted image 20230410194205.png]]