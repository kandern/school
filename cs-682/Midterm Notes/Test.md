#### Corrections
![[Pasted image 20230410193037.png]]



### 1
An important parameter of the imaging system is the field of view (FOV).  
Field of view is twice the angle between the optical axis (z-axis) and the end of  
the retinal plane (CCD array). Imagine that you have a camera system with focal  
length 16mm, and retinal plane (CCD array) is (16mm × 12mm) and that imaging  
surface is sampled 640 × 480 pixels in each dimension.
[[Question 1]]
### 2
Show the resulting image obtained after convolution of the original image with  
the following approximation of the derivative filter [−1, 0, 1] in the horizontal direction.
$$
\begin{bmatrix}
0& 0& 0& 0& 0& 0& 0& 0& 0& 0\\
0& 0& 0& 0& 0& 0& 0& 0& 0& 0\\
0& 0& 0& 1^{*}& 1& 1& 1& 0& 0& 0\\
0& 0& 0& 1& 1& 1& 1& 0& 0& 0\\
0& 0& 0& 1^{*}& 1& 1& 1^{*}& 0& 0& 0\\
0& 0& 0& 1& 1& 1& 1& 0& 0& 0\\
0& 0& 0& 1& 1& 1& 1& 0& 0& 0\\
0& 0& 0& 0& 0& 0& 0& 0& 0& 0\\
0& 0& 0& 0& 0& 0& 0& 0& 0& 0
\end{bmatrix}
$$

[[Question 2]]
### 3
Motion recovery. Consider a set of corresponding points x1 and x2 in retinal coordinates in two views, which are related by pure translation T
[[Question 3]]
### 4

[[Question 4]]
### 5
Suppose you observe 3 vanishing points in the image and assume that you know  
all the intrinsic parameters except the focal length. How would you determine the  
rotation of the camera with respect to the origin of the world coordinate frame ?  
Write down the geometry of the problem and describe the individual steps of the  
algorithm.
[[Question 5]]
### 6
[[Question 6]]