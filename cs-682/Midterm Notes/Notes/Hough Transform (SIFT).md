Slides week5
The Hough transform is a feature extraction technique that is commonly used in image processing and computer vision to identify shapes or patterns in an image, such as lines, circles, or other geometrical shapes. It was developed by Paul Hough in the 1960s.

The basic idea behind the Hough transform is to represent a geometric shape in an image as a set of parameters that can be estimated from the image data. For example, a straight line in an image can be represented by its slope and intercept. The Hough transform then maps each point in the image to a set of parameter values that could describe a geometric shape passing through that point. In the case of a straight line, this would be a set of (slope, intercept) pairs.

To perform the Hough transform, the image is first preprocessed to identify edges or other features that might correspond to the desired geometric shapes. Each edge point is then used to vote for the possible parameter values that could describe a shape passing through that point. The result is a set of parameter values that received a large number of votes, which are taken to be the best estimates of the parameters describing the shape.

The Hough transform can be extended to identify other types of shapes as well, such as circles, ellipses, or even more complex curves. In general, any shape that can be represented by a set of parameters can be detected using the Hough transform.

One advantage of the Hough transform is that it is relatively robust to noise and other types of image degradation. It can also handle situations where parts of the object being detected are occluded or missing from the image. However, the Hough transform can be computationally expensive, especially for large images or complex shapes.
![[Pasted image 20230409182035.png]]
### Running time
The running time of Hough transform is typically measured in terms of the number of votes cast by the algorithm. The number of votes depends on the resolution of the parameter space, which is defined by the discretization step size for each parameter. The more finely the parameter space is sampled, the higher the resolution and the more votes are cast.

For example, if the parameter space is discretized into 100 bins for each parameter, then the number of votes would be proportional to the product of the number of bins for each parameter, which in this case would be 100 x 100 = 10,000. However, if the parameter space is discretized into 200 bins for each parameter, then the number of votes would be 200 x 200 = 40,000, which is four times as many votes as before.

Therefore, the running time of Hough transform can be quite sensitive to the resolution of the parameter space, and choosing an appropriate resolution is an important consideration for optimizing the algorithm's performance.

![[Pasted image 20230409182134.png]]

### SIFT
SIFT (Scale-Invariant Feature Transform) is a popular feature detection and descriptor extraction algorithm in computer vision. It was developed by David Lowe in 1999 and has become a standard technique for various computer vision applications, such as object recognition, image registration, and 3D reconstruction.

The SIFT algorithm consists of several steps:

1.  Scale-space extrema detection: This step involves constructing a scale space pyramid of an input image by applying a Gaussian filter at different scales. The pyramid is used to detect features, such as blobs and edges, that are invariant to scale changes.
    
2.  Keypoint localization: This step involves identifying the scale and location of the detected features using a difference-of-Gaussian (DoG) operator. The operator is used to find the local extrema in the scale space.
    
3.  Orientation assignment: This step involves assigning a dominant orientation to each keypoint based on the gradient direction of the image pixels around the keypoint. This allows the SIFT descriptor to be invariant to rotations.
    
4.  Descriptor generation: This step involves constructing a descriptor for each keypoint based on the gradient magnitude and orientation of the image pixels within a local neighborhood around the keypoint. The descriptor is a histogram of the gradient orientations, and it is normalized to be invariant to illumination changes.
    
5.  Keypoint matching: This step involves comparing the descriptors of two images to find corresponding keypoints. The matching can be done using various methods, such as nearest neighbor matching and ratio test matching.
    

SIFT has many advantages, such as scale and rotation invariance, robustness to changes in lighting and viewpoint, and high repeatability. However, it also has some limitations, such as computational complexity and sensitivity to noise and occlusions.