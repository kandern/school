Stereo matching is the process of finding the corresponding points in a pair of stereo images. The goal is to identify which pixels in one image correspond to which pixels in the other image. This process is important for 3D reconstruction and depth estimation.

The basic steps in stereo matching are as follows:

1.  Image rectification: The first step is to rectify the images to ensure that corresponding points lie on the same horizontal scanline. This simplifies the correspondence problem and improves accuracy.
    
2.  Feature detection: Next, features such as corners or blobs are detected in both images. These features should be distinct and easily identifiable in both images.
    
3.  Feature matching: For each feature in the first image, a corresponding feature in the second image is identified. This is done by searching for features in the second image that are spatially close to the first feature and have similar appearance.
    
4.  Disparity computation: The difference in the horizontal position of the corresponding features in the two images is known as the disparity. This disparity can be used to compute the depth or distance of the object in the scene.
    
5.  Disparity refinement: The computed disparities may contain errors or inconsistencies. Several methods such as filtering, interpolation, and occlusion detection can be used to refine the disparity map.
    
6.  Depth estimation: Once the disparity map is refined, it can be used to estimate the depth or distance of each point in the scene.
    

There are various methods for stereo matching, including local methods, global methods, and learning-based methods. Local methods compute the disparity for each pixel based on the surrounding pixels. Global methods use optimization techniques to minimize a cost function that captures the similarity between the two images. Learning-based methods use deep neural networks to learn the correspondence between the two images.

In stereo matching, we compare corresponding windows between the left and right images to determine the depth of a scene. One common approach to comparing windows is to use the sum of squared differences (SSD) or the normalized cross-correlation (NCC).

The SSD measures the sum of squared differences between corresponding pixel values in the left and right windows. The smaller the SSD, the more similar the windows and the more likely they correspond to the same 3D point in the scene.

The NCC, on the other hand, computes the correlation between the left and right windows after subtracting their mean and normalizing them to unit variance. The closer the NCC is to 1, the more similar the windows and the more likely they correspond to the same 3D point in the scene.

Other similarity measures such as mutual information and gradient correlation can also be used in stereo matching, depending on the application and image characteristics.
![[Pasted image 20230409193620.png]]
