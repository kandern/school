When filtering you average the results.
#### Gausian Filter (week2-3 )
![[Pasted image 20230409105904.png]]
	 $\sigma$ how smooth you want the filter
	 $x$ the $x$ index of the filter
	 $y$ the $y$ index of the filter
- Removes high-frequency component i.e. noise
- repeated convolutions is equivelent to increasing $\sigma$
- Is separable, as it factors into two 1D Gaussians$$\begin{bmatrix}1& 2& 1\end{bmatrix} \times \begin{bmatrix}1\\2\\1\end{bmatrix} = \begin{bmatrix}1&2&1\\2&4&2\\1&2&1\end{bmatrix}$$
#### Non-linear Filtering
- Replace pixel with median value of neighborhood.
	- does not spread the noise, can remove spike noise, expensive to run
#### Image Derivatives
vertical derivative (left image)
 - Convolve with $\begin{bmatrix}-1& 0& 1\end{bmatrix}$
Horizontal Edges (right image)
 - Convolve with $\begin{bmatrix}-1\\ 0\\ 1\\\end{bmatrix}$
 - ![[Pasted image 20230409112855.png]]
#### Smoothing vs derivative filters
###### Smoothing
 - reduce noise
 - average intensity of pixels
 - Also known as low pass filter
 - remove low frequency components.
###### Derivative Filters
- detect edges by calculating gradient intensity.
- Also known as high pass Filter
