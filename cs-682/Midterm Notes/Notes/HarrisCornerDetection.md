The Harris corner detection algorithm works by analyzing the local intensity variations in an image and identifying regions where these variations are large in multiple directions. These regions are likely to correspond to corners or junctions, which are distinctive features in the image that can be used for tasks such as object recognition and image stitching.

covarient( will still find regardless of)
Is covarient to translation
Is covarient to rotation
Is not covarient to scaling! if it is scaled too big or too small, there may be more corners or none.

The algorithm consists of the following steps:

1.  Compute the gradient of the image in the x and y directions using a suitable filter, such as the Sobel filter.
    
2.  Compute the products of the gradient values at each pixel, in both x and y directions, to obtain the structure tensor:
    
    M = \[\[A, C\], \[C, B\]\],
    

where A, B, and C are the sums of the products of the x and y gradients in local windows around each pixel.

3.  Compute the Harris response function R at each pixel using the formula:
    
    R = det(M) - k * tr(M)^2,
    

where det(M) and tr(M) are the determinant and trace of the structure tensor, respectively, and k is a constant that controls the sensitivity of the algorithm.

4.  Threshold the Harris response values to obtain a binary map of corner locations. Typically, pixels with response values above a certain threshold are considered to be corners.
    
5.  Apply non-maximum suppression to the corner locations to suppress multiple responses to the same corner.
    

The Harris corner detection algorithm has several advantages over other corner detection methods, such as its computational efficiency and its ability to handle images with varying illumination and noise levels. However, it also has some limitations, such as its sensitivity to changes in scale and orientation, and its inability to detect corners with non-Gaussian distributions of gradient orientations.