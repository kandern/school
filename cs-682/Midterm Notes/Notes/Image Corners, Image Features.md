### Corners
[[HarrisCornerDetection]]

Corner detection is a technique used in computer vision to identify and locate corners or interest points in an image. Corners are locations in an image where the brightness or color of the pixels changes significantly in multiple directions. Corner detection is a fundamental step in many computer vision tasks such as object recognition, tracking, and 3D reconstruction.

There are several algorithms for corner detection, but most of them work based on the following general principles:
 - Filter image.  
 - Compute magnitude of the gradient everywhere.  
 - We construct M in a window.  
 - Use Linear Algebra to find $\lambda 1$ and $\lambda 1$.  
If they are both big, we have a corner


1.  Compute image gradients: The first step in most corner detection algorithms is to compute the image gradients, which represent the rate of change of image intensity or color.
    
2.  Calculate the structure tensor: The structure tensor is a matrix that encodes the second-order statistics of the image gradients. It is calculated by convolving the image gradients with a Gaussian filter to weight the contributions of nearby pixels and to suppress noise.
    
3.  Compute the corner response: The corner response is a scalar value that measures the likelihood of a pixel being a corner based on the eigenvalues of the structure tensor. It is computed by taking the determinant and trace of the structure tensor and using them to calculate a corner response function. The corner response function is a measure of how different the local image structure is from a flat region or an edge.
    
4.  Threshold the corner response: The corner response values are thresholded to select the pixels that are most likely to be corners.
    
5.  Apply non-maximum suppression: Non-maximum suppression is applied to remove redundant corners that are too close to each other.
    

Here is a more detailed explanation of the steps involved in the Harris corner detection algorithm, which is one of the most widely used algorithms for corner detection:

1.  Compute image gradients: The image gradients are calculated by convolving the image with derivative filters in the x and y directions.
    
2.  Calculate the structure tensor: The structure tensor is computed by convolving the image gradients with a Gaussian filter. The structure tensor matrix is given by:$$M=\begin{bmatrix}
			\sum_{x,y}I_x^2&\sum_{x,y}I_xI_y\\
			\sum_{x,y}I_xI_y&\sum_{x,y}I_y^2
		\end{bmatrix}=\begin{bmatrix}
			a&0\\
			0&b
		\end{bmatrix}$$ If either a or b is close to 0, then this is not a corner, so we want locations where both are large
1. Compute the corner response: The corner response function is calculated as:$$
   R= det(M) - k \times (trace(M))^2$$
   where det(M) is the determinant of the structure tensor, 
   ![[Pasted image 20230409140728.png]]
   trace(M) is its trace, and k is an empirically determined constant.
![[Pasted image 20230409140816.png]]
2.  Threshold the corner response: The corner response values are thresholded to select the pixels that are most likely to be corners.
    
3.  Apply non-maximum suppression: Non-maximum suppression is applied to remove redundant corners that are too close to each other.
    

Corner detection algorithms like the Harris algorithm can be tuned by adjusting the parameters, such as the Gaussian filter size and the threshold value, to optimize the trade-off between corner detection accuracy and computational efficiency.

#### Interpretting  the second Matrix
$$M=\begin{bmatrix}
			\sum_{x,y}I_x^2&\sum_{x,y}I_xI_y\\
			\sum_{x,y}I_xI_y&\sum_{x,y}I_y^2
		\end{bmatrix}=\begin{bmatrix}
			a&0\\
			0&b
		\end{bmatrix}=R^{-1}\begin{bmatrix}
			\lambda_1 &0\\
			0&\lambda_2
		\end{bmatrix}R$$
$R = det(M)-\alpha*trace(M)^2 = \lambda_1\lambda_2-\alpha(\lambda_1+\lambda_2)^2$ 
$R^{-1}$ is the invers matrix of R
![[Pasted image 20230409153246.png]]
### Blobs
1.  Scale-space representation: The first step is to create a scale-space representation of the image, which involves smoothing the image with Gaussian kernels at different scales to create a series of blurred images. Each blurred image is called an octave, and within each octave, there are multiple scales or levels.
    
2.  Laplacian of Gaussian: For each level of the scale-space representation, the Laplacian of Gaussian (LoG) is calculated by taking the Laplacian of the Gaussian-filtered image. The Laplacian is a measure of the second derivative of the image intensity, and it can be used to identify areas of high frequency and high contrast. By convolving the image with a Gaussian kernel before taking the Laplacian, the LoG detector can identify blobs at different scales.
    
3.  Thresholding: Once the LoG is computed for each level of the scale-space representation, a thresholding step is applied to identify the center of the blobs. Typically, a threshold value is chosen such that only the local maxima above a certain value are detected. This thresholding step is used to remove noise and identify significant blob centers.
    
4.  Non-maximum suppression: In some cases, multiple blobs may be detected in close proximity to each other. To avoid detecting the same blob multiple times, a non-maximum suppression step is applied. This step identifies the strongest blob in each local region and suppresses the rest.
    
5.  Scale and location extraction: Finally, the identified blob centers are extracted along with their corresponding scale and location information. This information can be used to match blobs across different images or to identify blobs in a video stream over time.
    

The LoG detector is a computationally expensive algorithm, especially when applied to large images or when searching for blobs at multiple scales. To improve the efficiency, a multi-scale approach, such as the Difference of Gaussian (DoG) or the Hessian-based Laplacian, can be used instead. These algorithms follow a similar set of steps but use different filters and approximations to reduce the computational cost.
#### Multiple levels of blobs

Image pyramids are used to detect blobs at multiple scales. Instead of using a single filter to detect blobs, we can use a series of filters with varying sizes, which is achieved by creating a series of images with decreasing resolution, known as an image pyramid. The filters are applied to each level of the pyramid, and the detected blobs at different scales are combined to form a final blob map. This approach is useful when the size of the blobs varies or is unknown, and we want to detect blobs at different scales in the image. By applying filters at different scales, we can capture blobs that may be missed by a single filter. Additionally, the use of image pyramids can reduce the computational cost of detecting blobs at multiple scales, as it avoids the need to resize the original image multiple times.

Increasing the filter size to detect blobs is a valid approach, but it may not always be effective. While larger filters can capture larger blobs, they may also capture other structures in the image that are not blobs, leading to false detections. Additionally, larger filters can also increase the computational cost of detecting blobs.

Moreover, increasing the filter size may not capture the variation in blob sizes, which is common in natural images. In contrast, using image pyramids allows us to capture blobs at different scales and avoids the limitations of a single filter.

Therefore, while increasing the filter size is a viable approach for blob detection, using image pyramids is generally considered a more robust and effective method for detecting blobs at multiple scales in an image.