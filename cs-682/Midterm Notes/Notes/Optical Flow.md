Optical flow is a technique used in computer vision to track the motion of objects in a video sequence. It refers to the pattern of apparent motion of image features between consecutive frames of a video. In other words, it measures the displacement of pixels between two consecutive frames.

The optical flow of an image is calculated by analyzing the changes in brightness or intensity of pixels in a sequence of images. Specifically, the algorithm looks for points in one image that match the same point in a subsequent image, and computes the displacement between these points. This process is repeated for all points in the image, creating a vector field that describes the motion of each point.

Optical flow has a wide range of applications in computer vision, including object tracking, video stabilization, and motion analysis. For example, it can be used to detect moving objects in a scene, estimate the speed and direction of motion, and predict the future position of objects in a video sequence. Optical flow can also be used to create visual effects in movies and video games, such as motion blur and time warp effects.

Optical Flow Equation:
$u=-M^{-1}b$
u = The 2d Vector representing the displacement in both x and y directions.
$M=\begin{bmatrix} \sum_{x,y}I_x^2&\sum_{x,y}I_xI_y \\ \sum_{x,y}I_xI_y&\sum_{x,y}I_y^2\end{bmatrix}$
$b = \begin{bmatrix} \sum_{x,y}I_xI_t\\\sum_{x,y}I_yI_t\end{bmatrix}$
$I_t(x,y) = I(x,y,t+1)-I(x,y,t)$
t(x,y) = Intensity at pixel
t+1 = Intensity at pixel on next frame

Lucas-Kanade is a widely-used algorithm for estimating optical flow, which is the motion of pixels between two consecutive frames of a video sequence. The algorithm is based on the assumption that the brightness of a pixel remains constant over time, and seeks to find the best flow vector that satisfies this constraint.

The Lucas-Kanade algorithm works by selecting a set of points (called "features" or "interest points") in the first frame of the video sequence and tracking their motion across the subsequent frames. For each feature, the algorithm computes a local estimate of the optical flow vector by solving a set of linear equations based on the brightness constancy assumption.

Specifically, for each feature point, the algorithm considers a small neighborhood around the point and constructs a linear equation of the form:

[A^T A] u = A^T b

Here, A is a matrix that contains the spatial derivatives of the image brightness in the x and y directions, evaluated at the feature point, and u is the optical flow vector that we want to estimate. The vector b represents the temporal derivative of the image brightness at the feature point, computed as the difference between the pixel values in the two consecutive frames.

The Lucas-Kanade algorithm solves this equation using least-squares regression to obtain an estimate of the flow vector u that minimizes the sum of the squared errors between the predicted brightness values (based on the current estimate of u) and the actual brightness values in the neighborhood.

This process is repeated for all the feature points in the image, and the resulting optical flow vectors are used to estimate the motion of objects in the scene. The Lucas-Kanade algorithm is efficient and computationally lightweight, and has been widely used in applications such as object tracking, motion estimation, and 3D reconstruction from video.