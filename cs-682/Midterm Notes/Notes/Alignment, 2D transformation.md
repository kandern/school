When aligning two pictures, there are different types of transformations that can be applied depending on the level of similarity between the images.

### Similarity transformation
In a similarity transformation, the two images have the same shape, but one image may be scaled, translated, and rotated with respect to the other. A similarity transformation can be represented by a combination of translation, rotation, and uniform scaling. This transformation preserves the angles between the lines and the ratio of the distances between points. In computer vision, similarity transformation is used for tasks such as image registration and object tracking.
    
### Affine transformation
An affine transformation is a more general transformation that includes similarity transformations as a special case. It is a linear mapping that preserves parallel lines and ratios of distances between points. An affine transformation can represent the effect of translation, rotation, scaling, and shearing. In computer vision, affine transformation is used for tasks such as image registration, texture mapping, and face recognition.
![[Pasted image 20230409184202.png]]

### Projective homography
A projective homography is a more general transformation that includes affine transformations as a special case. It is a non-linear mapping that preserves straight lines and ratios of distances between points. A projective homography can represent the effect of translation, rotation, scaling, shearing, and perspective distortion. In computer vision, projective homography is used for tasks such as 3D reconstruction, image stitching, and camera calibration.


The process of aligning two images typically involves finding the transformation that best matches the features in the images. This is done by extracting keypoint features from the images, such as SIFT, and matching them to find correspondences between the images. Once the correspondences are found, the transformation parameters can be estimated using methods such as RANSAC, least-squares fitting, or optimization techniques. The estimated transformation can then be applied to one of the images to align it with the other image.

### Matricies
![[Pasted image 20230409184029.png]]



## Finding Homography
Yes, the steps to find a projective homography between two images are:

1.  Feature detection and matching: Detect feature points in both images, such as corners or blobs, and find corresponding matches between them.
    
2.  Matching keypoints The second step is to match corresponding keypoints between the two images. This is typically done by computing a descriptor for each keypoint, which is a vector that summarizes the local appearance of the image around the keypoint. One common descriptor used in SIFT is the histogram of gradient orientations in a local patch around the keypoint.
   
   After computing the descriptors for each keypoint in both images, the goal is to find corresponding keypoints between the two images. One approach is to compare the descriptors of all keypoints in one image to the descriptors of all keypoints in the other image and find the best matches. This can be done efficiently using techniques such as nearest neighbor search or approximate nearest neighbor search.

3. Computing the homography Once corresponding keypoints have been identified, the next step is to compute the homography matrix that maps points in one image to points in the other image. This can be done using a technique called direct linear transform (DLT). DLT involves solving a system of linear equations, where each equation corresponds to a pair of corresponding keypoints and the unknowns are the entries of the homography matrix.
   
   Since there are only eight unknowns in the homography matrix, DLT requires at least four corresponding keypoints to be known in order to solve for the homography. However, it is common to use more than four correspondences and solve for the homography using least-squares optimization in order to reduce the effects of noise or outliers.

4.  Warping: Apply the computed homography matrix to one of the images to warp it into the coordinate space of the other image. This involves transforming each pixel in the original image to a new location in the warped image based on the homography matrix.

5.  Blending: Blend the warped image with the other image to create a composite image that aligns the two images. This can be done using methods such as alpha blending or feathering.


It is important to note that projective homography assumes that the two images are taken from different viewpoints and that there may be significant perspective distortion between them. This makes it a powerful tool for image alignment in applications such as panorama stitching, but it also requires a sufficient number of matched feature points and careful parameter tuning to achieve accurate results.