Week 10 slide 3
Convolutional neural networks (CNNs) are a type of artificial neural network designed specifically for image recognition tasks. CNNs are inspired by the way the human brain processes visual information. The basic idea behind CNNs is to learn a set of filters (kernels) that can be used to extract useful features from images.

CNNs consist of several layers. The input layer takes in the raw image data, and the output layer produces the classification result. The layers in between are called hidden layers, and they are responsible for learning the features of the image.

The core building block of a CNN is the convolutional layer. In this layer, a set of filters is applied to the input image to extract features. Each filter is a small matrix of weights that slides over the input image, performing a dot product at each position. The result of the dot product is a single number, which is the output of the filter at that position. The output of the convolutional layer is a set of feature maps, each of which corresponds to a specific filter.

After the convolutional layer, there is usually a non-linear activation function, such as ReLU, applied element-wise to the feature maps. This is followed by a pooling layer, which reduces the size of the feature maps by taking the maximum or average value within a small neighborhood.

The output of the pooling layer is then flattened into a one-dimensional vector and passed through one or more fully connected layers, which perform the classification task. These layers consist of neurons that are fully connected to the output of the previous layer.

During training, the weights of the filters in the convolutional layers and the weights of the neurons in the fully connected layers are learned using backpropagation and gradient descent optimization. The loss function used for training is typically the cross-entropy loss, which measures the difference between the predicted and actual class labels.

CNNs have achieved state-of-the-art performance on many image recognition tasks, such as object detection, image classification, and semantic segmentation. They have also been used for other tasks, such as natural language processing and speech recognition, with adaptations to the architecture.