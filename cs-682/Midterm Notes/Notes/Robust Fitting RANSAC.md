### Total Least squares
$E=\sum_{i=1}^n(ax_i+by_i-d)^2$
![[Pasted image 20230409171928.png]]
E = is the error, this should be minimized
a = slope
b = intercept
d = offset parameter
Heavily penalizes outliers

### Robust Estimators
![[Pasted image 20230409180357.png]]
### RANSAC (week 5 fitting)
RANSAC (Random Sample Consensus) is an iterative algorithm used for robust estimation from a set of data points that may contain outliers. The algorithm works by randomly selecting a subset of the data points, called the "inliers," and fitting a model to these points. The remaining points are then classified as either "inliers" or "outliers" based on their proximity to the model. This process is repeated multiple times, with the model with the largest number of inliers being selected as the best fit for the data.

The basic steps of the RANSAC algorithm are as follows:

1.  Randomly select a subset of the data points to form a potential "inlier" set.
2.  Fit a model to the inlier set.
3.  Determine the remaining points that are consistent with the model, i.e., within a certain threshold of the model.
4.  Count the number of points that are consistent with the model.
5.  Repeat steps 1-4 for a fixed number of iterations.
6.  Select the model with the largest number of consistent points as the best fit for the data.
The RANSAC algorithm is widely used in computer vision and other fields for tasks such as object recognition, camera calibration, and feature matching, where it is important to robustly estimate parameters from a set of data points that may contain outliers.