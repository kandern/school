Pixels: great for spatial resolution, poor access  
to frequency  

Fourier transform: great for frequency, not for  
spatial info  

Pyramids/filter banks: balance between spatial  
and frequency information
### Gaussian Image Pyramids
Gaussian image pyramids are a type of multi-scale image representation used in computer vision and image processing. They are created by repeatedly applying a Gaussian smoothing filter and downsampling the image to produce a sequence of lower-resolution images, with each level of the pyramid representing the original image at a different scale.

The process of constructing a Gaussian image pyramid typically involves the following steps:

1.  Starting with the original image, apply a Gaussian smoothing filter to remove high-frequency content and produce a blurred version of the image.
    
2.  Downsample the blurred image by a factor of two in each dimension to produce a lower-resolution version of the image.
    
3.  Repeat steps 1 and 2 with the lower-resolution image to produce a sequence of images at different scales.
    
4.  Store the resulting images in a hierarchical structure known as the Gaussian pyramid.
    

The Gaussian image pyramid has several useful properties. Firstly, it provides a multi-scale representation of the image that allows for the detection of features at different scales. Secondly, the downsampling process helps to reduce the size of the image, making it easier and faster to process. Finally, the Gaussian smoothing filter helps to reduce noise and artifacts in the image, making it more suitable for further processing and analysis.

The Gaussian pyramid is often used in computer vision and image processing applications such as feature detection, object recognition, and image segmentation. For example, it can be used to detect corners or edges at multiple scales, or to segment an image into regions based on the scale of the features present.
### Laplacian Image Pyramids
Laplacian image pyramids are a type of multi-scale image representation that are closely related to Gaussian image pyramids. Like Gaussian pyramids, they are created by applying a sequence of filters and downscaling the image, resulting in a hierarchy of images at different scales. However, instead of representing the image at each scale directly, a Laplacian pyramid stores the difference between each level of the pyramid and an upsampled version of the next level.

The process of constructing a Laplacian image pyramid typically involves the following steps:

1.  Starting with the original image, apply a Gaussian smoothing filter to remove high-frequency content and produce a blurred version of the image.
    
2.  Downsample the blurred image by a factor of two in each dimension to produce a lower-resolution version of the image.
    
3.  Subtract an upsampled version of the lower-resolution image from the original blurred image to produce a high-pass filtered version of the image at the current scale.
    
4.  Repeat steps 1 to 3 with the lower-resolution image to produce a sequence of high-pass filtered images at different scales.
    
5.  Store the resulting images in a hierarchical structure known as the Laplacian pyramid.
    

The Laplacian pyramid has several useful properties. Firstly, it provides a multi-scale representation of the image that allows for the detection of features at different scales. Secondly, by storing the difference between each level of the pyramid and an upsampled version of the next level, it can be used to reconstruct the original image at any scale or resolution. Finally, the high-pass filtering process helps to emphasize the high-frequency content in the image, making it more suitable for edge detection, texture analysis, and other applications that require the detection of fine details.

The Laplacian pyramid is often used in computer vision and image processing applications such as image compression, texture synthesis, and image blending. For example, it can be used to compress an image by discarding the high-frequency details that are less important for visual perception, or to blend two images together by combining the high-frequency content of one image with the low-frequency content of another.

### Comparrison
Gaussian and Laplacian image pyramids are related and can be used together in various image processing applications. Here are some key differences between the two:

-   Gaussian pyramids are created by applying a series of Gaussian filters to an image and downsampling it, whereas Laplacian pyramids are created by computing the difference between a blurred image and an upsampled version of the next level in the pyramid.
-   Gaussian pyramids provide a multi-scale representation of the image, where each level represents a blurred version of the image at a different scale, while Laplacian pyramids provide a representation of the image that emphasizes the high-frequency content at each scale.
-   Gaussian pyramids can be used for image smoothing, downsampling, and feature extraction, while Laplacian pyramids are often used for image enhancement, edge detection, and texture analysis.
-   Gaussian pyramids are invertible, meaning that the original image can be reconstructed from the pyramid, while Laplacian pyramids are not strictly invertible, but can be used in conjunction with a Gaussian pyramid to reconstruct the original image.
-   In terms of computational complexity, the Laplacian pyramid is generally more expensive to compute than the Gaussian pyramid due to the need to compute the difference between levels.

Overall, Gaussian and Laplacian pyramids are complementary tools that can be used together to perform a variety of image processing tasks. While Gaussian pyramids are more commonly used and have a wider range of applications, Laplacian pyramids can provide more detailed information about the high-frequency content of an image, making them useful in certain contexts.