Camera calibration is the process of determining the intrinsic and extrinsic parameters of a camera. The intrinsic parameters are specific to the camera and describe the internal properties of the camera such as focal length, image sensor size, and principal point. The extrinsic parameters describe the position and orientation of the camera with respect to the 3D world coordinates.

The calibration process involves capturing several images of a calibration target with known geometry and using these images to estimate the intrinsic and extrinsic parameters of the camera. The calibration target can be a planar checkerboard, a 3D calibration object with known geometry, or a spherical object.

The calibration process involves the following steps:

1.  Image capture: Capture multiple images of the calibration target with the camera to be calibrated.
    
2.  Feature detection: Detect features (such as corners or dots) in the images that can be used to estimate the parameters.
    
3.  Parameter initialization: Initialize the intrinsic and extrinsic parameters of the camera to initial guesses based on the known camera specifications.
    
4.  Optimization: Use a nonlinear optimization algorithm to refine the parameter estimates based on the detected features and the known geometry of the calibration target.
    
5.  Error analysis: Analyze the error between the observed and predicted image points to evaluate the quality of the calibration and to estimate the uncertainty in the parameters.
    

Once the intrinsic and extrinsic parameters are estimated, they can be used to correct for lens distortion, compute the 3D position of objects in the scene, and perform other tasks related to camera imaging.

### When you can't callibrate
Calibration can become impossible if the camera system does not satisfy the assumptions made during the calibration process. For example, if the camera has a significant amount of lens distortion or if the camera's internal parameters (such as the focal length or principal point) vary over time, it may not be possible to accurately calibrate the camera. Additionally, if the calibration pattern is not visible or if there is significant noise in the images used for calibration, it may be difficult to accurately estimate the camera parameters. Finally, if the calibration pattern is not well-posed, meaning that it does not contain enough information to uniquely determine the camera parameters, calibration may also be impossible.


### Linear vs Non-Linear calibration
In camera calibration, linear calibration methods assume that the relationship between the pixel coordinates and the 3D world coordinates is linear. This means that the intrinsic and extrinsic camera parameters can be estimated using linear equations. Linear calibration methods are fast and simple but can be inaccurate for complex camera models.

Nonlinear calibration methods, on the other hand, can account for complex distortions and can estimate the intrinsic and extrinsic camera parameters with higher accuracy. Nonlinear methods involve minimizing a nonlinear error function that captures the difference between the observed image points and the projected 3D world points. This optimization problem can be solved using numerical optimization techniques such as Levenberg-Marquardt or Gauss-Newton.

Overall, nonlinear calibration methods are more accurate but also more computationally expensive compared to linear methods. The choice between linear and nonlinear methods depends on the specific application and the required level of accuracy.

### NonLinear
Nonlinear camera calibration involves solving for the intrinsic and extrinsic parameters of a camera using nonlinear optimization techniques. The process involves minimizing the error between the observed image points and the projected 3D points in the camera's image plane, using a non-linear cost function.

The steps involved in the nonlinear approach to camera calibration are as follows:

1.  Initial Parameter Estimate: The intrinsic and extrinsic parameters are initially estimated using a linear calibration technique or some other rough estimate.
    
2.  Define Nonlinear Cost Function: A nonlinear cost function is defined that captures the error between the observed image points and the projected 3D points. The cost function typically consists of a sum of squares of the difference between the observed and projected points.
    
3.  Optimization: The parameters of the camera model are adjusted to minimize the cost function. This can be done using an iterative optimization algorithm such as Levenberg-Marquardt or Gauss-Newton.
    
4.  Refinement: The optimized parameters are refined further by repeating steps 2 and 3 until the cost function is minimized to a satisfactory level.
    
5.  Validation: Finally, the accuracy of the calibrated camera can be validated by testing it on a set of independent test images.
    

Nonlinear camera calibration is a more accurate and flexible approach compared to linear calibration. It can handle more complex camera models and can capture lens distortions that cannot be accounted for using linear calibration. However, it is also more computationally intensive and requires more careful tuning of the optimization parameters.

The math for nonlinear camera calibration involves optimizing a nonlinear cost function using an iterative optimization algorithm such as Levenberg-Marquardt. The cost function typically involves the sum of squared reprojection errors, which measures the distance between the observed image points and the projected points on the image plane.

The nonlinear camera calibration model can be expressed as:

x = f(X, P) + e

where x is the observed image point, X is the corresponding 3D point in world coordinates, P is the camera intrinsic and extrinsic parameters, f() is the perspective projection function that maps 3D world points to 2D image points, and e is the reprojection error.

The goal of nonlinear camera calibration is to estimate the camera parameters P that minimize the sum of squared reprojection errors:

minimize ∑ ||x - f(X, P)||^2

This is a nonlinear optimization problem, which can be solved iteratively using the Levenberg-Marquardt algorithm. The algorithm starts with an initial estimate of the camera parameters and iteratively updates the parameters until the cost function is minimized. The updates are based on the Jacobian matrix of the cost function, which provides information about how the cost function changes with respect to changes in the camera parameters. The Jacobian is used to compute a search direction for the optimization algorithm at each iteration, and the step size is determined by a damping factor that balances between the gradient descent and Gauss-Newton methods. The optimization process continues until a convergence criterion is met, such as a minimum change in the cost function or a maximum number of iterations.


## Linear
1.  Choose a set of 3D points with known coordinates in a reference frame, and capture their 2D projections in the camera image.
    
2.  Formulate the camera projection model, which maps 3D points in the reference frame to their 2D projections in the camera image. This model can be represented by a matrix multiplication:
    
    s * \[u, v, 1\]ᵀ = K \[R | t\] \[X, Y, Z, 1\]ᵀ
    
    where [u, v, 1] is the homogeneous 2D projection of a 3D point [X, Y, Z, 1] in the camera image, K is the camera intrinsic matrix (containing the focal length, principal point, and skew), \R | t] is the camera extrinsic matrix (containing the rotation and translation), and s is a scaling factor that accounts for the depth of the 3D point.
    
3.  Rearrange the camera projection model to obtain a linear system of equations in the form A * p = 0, where p is a vector containing the unknown parameters (the elements of K, R, and t), and A is a matrix that depends on the known 3D-2D correspondences.
    
4.  Solve the linear system of equations using techniques such as singular value decomposition (SVD) to obtain an estimate of the unknown parameters p.
    
5.  Refine the estimate of the camera parameters using nonlinear optimization techniques to minimize the reprojection error, which measures the distance between the observed 2D projections and the projections predicted by the camera projection model using the estimated parameters.