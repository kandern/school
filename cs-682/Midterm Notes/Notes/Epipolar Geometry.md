1.  Acquire two images: To perform epipolar geometry, you need two images of the same scene taken from different viewpoints. These images should be rectified, meaning that the epipolar lines are aligned and parallel.
    
2.  Compute feature points: The next step is to detect feature points in each image that can be matched between the two images. Common feature point detectors include SIFT, SURF, and ORB.
    
3.  Match feature points: Once the feature points are detected, the next step is to find the corresponding points between the two images. This can be done using a matching algorithm such as brute force matching or using feature descriptors such as SIFT.
    
4.  Compute the fundamental matrix: The fundamental matrix describes the relationship between the two images and can be computed using the matched feature points. There are different algorithms to compute the fundamental matrix, including the 8-point algorithm and RANSAC.

5. Compute the epipolar lines: Once the fundamental matrix is computed, the epipolar lines can be computed for each point in one image. The epipolar lines are the lines on which the corresponding point in the other image must lie. To compute the epipolar line for a point in one image, you can multiply the coordinates of the point by the transpose of the fundamental matrix.

7. Triangulation: Finally, the 3D position of the corresponding points can be estimated using triangulation. This involves finding the intersection point of the two lines of sight from the two cameras to the corresponding points in the two images. Triangulation can be done using linear or nonlinear methods. In the linear method, the 3D point is computed by solving a system of linear equations, while in the nonlinear method, an iterative optimization is used to refine the 3D position. Once the 3D points are estimated, they can be used to reconstruct the 3D structure of the scene.