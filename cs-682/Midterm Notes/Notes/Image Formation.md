#### from point P to point X on image
![[Pasted image 20230408131818.png]]
The process of going from a point P in the world to a point X on an image using a pinhole camera involves several mathematical steps. Here are the main ones:

1.  Define the camera's intrinsic parameters: These include the focal length (f), the coordinates of the camera's principal point (c_x, c_y), and the image sensor size.  
2.  Define the camera's extrinsic parameters: These include the position and orientation of the camera in 3D space.
3.  Calculate the camera matrix: This is a 3x4 matrix that combines the intrinsic and extrinsic parameters of the camera.
4.  Convert the world point P to homogeneous coordinates: This involves appending a 1 to the end of the 3D coordinate vector.
5.  Multiply the camera matrix by the world point in homogeneous coordinates: This produces a 3D vector in camera coordinates.
6.  Divide the resulting vector by its third component: This normalizes the vector and gives the 2D point X in image coordinates.
The formula for this process is:

$$X=\frac{(f \times (R \times P + T))}{(R * P + T)_z} + (c_x, c_y)$$where:

-   X is the 2D point in image coordinates
-   f is the focal length
-   R is the rotation matrix that describes the camera's orientation in 3D space
-   P is the 3D point in world coordinates $\begin{bmatrix}X\\ Y\end{bmatrix}$
-   T is the translation vector that describes the camera's position in 3D space
-   (c_x, c_y) are the coordinates of the principal point of the camera
-   (R * P + T)_z is the third component of the resulting 3D vector.

Note that this formula assumes that the image sensor has the same aspect ratio as the camera's principal point. If the aspect ratios are different, the formula needs to be modified accordingly.
#### Homogeneous coordinates
The reason for using homogeneous coordinates in the pinhole camera model is that it allows us to perform perspective projection using matrix multiplication. Specifically, we can represent the projection of a 3D world point $P = (X, Y, Z)$ onto a 2D image point $x = (u, v)$ in homogeneous coordinates as:
$$s * x = K * [R | t] * P$$
Where:
- $s$ is a scaling factor
- $K$ is the camera's intrinsic matrix
- $[R | t]$ is the camera's extrinsic matrix
- $*$ denotes matrix multiplication.

In this equation, the right-hand side represents the transformation of the 3D point `P` from world coordinates to camera coordinates, followed by projection onto the image plane. The left-hand side represents the 2D point `x` in homogeneous coordinates, scaled by the scaling factor `s`.

To convert the homogeneous coordinates of the projected point `x` back to regular 2D coordinates, we divide the first two coordinates of `x` by the third coordinate:
$$u = x[0] / x[2] v = x[1] / x[2]$$
So, the use of homogeneous coordinates in the pinhole camera model enables us to represent perspective projection as a matrix multiplication and to convert the result back to regular 2D coordinates.
##### Homogeneous Pinhole
Homogeneous coordinates are often used in computer vision and computer graphics to represent points and transformations in projective space, which allows for easy manipulation of geometric transformations such as rotation, translation, and scaling.

In the case of the pinhole camera model, homogeneous coordinates can be used to represent points in 3D space that are projected onto the 2D image plane of the camera. Homogeneous coordinates are represented as a four-dimensional vector [X,Y,Z,W], where (X,Y,Z) represent the 3D coordinates of the point and W is a scaling factor.

To project a point from 3D space onto the 2D image plane of a pinhole camera using homogeneous coordinates, we first need to define a projection matrix that maps points in 3D space to the 2D image plane. This projection matrix is typically a 3x4 matrix that depends on the intrinsic parameters of the camera (focal length, image sensor size) and the extrinsic parameters (position and orientation of the camera relative to the scene).

Once the projection matrix is defined, we can use it to transform the 4D homogeneous coordinates of a point in 3D space into a 3D point in the camera's coordinate system. We then divide the x and y coordinates of the resulting 3D point by its z-coordinate to obtain the normalized image coordinates, which represent the location of the point on the 2D image plane.

In summary, homogeneous coordinates allow us to represent 3D points and transformations in a compact and convenient way, and can be used in conjunction with the pinhole camera model to project points from 3D space onto the 2D image plane of a camera.
#### Perspective projection
![[Pasted image 20230408192518.png]]
##### Thin lens formula
![[Pasted image 20230409102441.png]]

