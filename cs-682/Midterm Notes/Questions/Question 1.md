An important parameter of the imaging system is the field of view (FOV).  
Field of view is twice the angle between the optical axis (z-axis) and the end of  
the retinal plane (CCD array). Imagine that you have a camera system with focal  
length 16mm, and retinal plane (CCD array) is (16mm × 12mm) and that imaging  
surface is sampled 640 × 480 pixels in each dimension.
1. Compute the FOV (horizontal and vertical)
	 - To compute the FOV in the horizontal and vertical directions, we need to use the pixel size and the number of pixels in each dimension.
	- The horizontal FOV is given by:
	   FOV_horizontal = 2 * arctan((w/2) / f)
	   where w is the width of the CCD array. In this case, w = 16mm.
	   FOV_horizontal = 2 * arctan((16/2) / 16)
							     = 2 * arctan(0.5)
								 = 63.43 degrees
	- Similarly, the vertical FOV is given by:
	   FOV_vertical = 2 * arctan((h/2) / f)
	   where h is the height of the CCD array. In this case, h = 12mm.
	   FOV_vertical = 2 * arctan((12/2) / 16)
							= 2 * arctan(0.375)
							= 46.35 degrees
	- Therefore, the FOV in the horizontal direction is 63.43 degrees, and the FOV in the vertical direction is 46.35 degrees.
2. Write down the relationship between the image coordinate and a point in 3D world expressed in the camera coordinate system.
	1. The equation that describes the relationship between an image coordinate and a 3D point in the camera coordinate system is the camera projection equation:$$pi * w = P * Pw * w$$where:
	- `pi` is the image coordinate (a 2D point)
	- `P` is the camera projection matrix (a 3x4 matrix)
	- `Pw` is the 3D point in the camera coordinate system (a 3D point expressed as [Xw, Yw, Zw])
	- `w` is a homogeneous coordinate that accounts for perspective distortion in the image.
	The camera projection matrix `P` can be decomposed into the intrinsic matrix `K` and the extrinsic matrix `[R|t]`, so the camera projection equation can also be written as:$$pi * w = K * [R|t] * Pw * w$$ where `K` is the intrinsic matrix and `[R|t]` is the extrinsic matrix.
	Conversely, to compute the 3D point `Pw` from an image coordinate `pi`, we can use the inverse of the camera projection matrix:$$Pw * w = P^{-1} * pi * w$$
	where $P^{-1}$ is the inverse of the camera projection matrix. The resulting 3D point in camera coordinates can then be transformed into the world coordinate system by applying the inverse of the extrinsic matrix.

1. Describe how is the size of FOV related to the focal length and how it affects the resolution in the image.
	- The size of the field of view (FOV) is inversely proportional to the focal length of the camera lens. This means that as the focal length increases, the FOV decreases and vice versa.
2. Given the horizontal FOV you computed, how many images do you need to create 360 degree panorama, assuming that you will need 50% overlap between neighboring views.
	1. get 50% of FOV, see how many degrees that is. Then see how many times that goes into 360. You then need a final image at the end, so add 1 and round up.