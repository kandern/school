Consider a set of corresponding points x1 and x2 in retinal  
coordinates in two views, which are related by pure translation T.

- If the corresponding points $x_1$ and $x_2$ in retinal coordinates in two views are related by pure translation T, then the relationship between the two points can be expressed mathematically as: $$x_2 = x_1 + T$$Where $x_1$ and $x_2$ are 2D points in the retinal coordinate system of the two views, and T is the 2D translation vector that describes the translation between the two views.
  This equation simply means that to get the position of point $x_2$ in the second view, we need to add the translation vector T to the position of point $x_1$ in the first view. This assumes that there is no rotation or scaling between the two views, and only pure translation exists.
  This equation is commonly used in computer vision and stereo vision applications, where it is necessary to relate points in different views of the same scene. It can be used to calculate the 3D position of a point in the scene by triangulation, given its corresponding 2D positions in two views and the known translation vector between the views.




- Write down a simplified version of the epipolar constraint in case the motion is pure translation.
	- $x_2 = x_1 + T$
	  Where $x_1$ and $x_2$ are corresponding points in the two views and T is the translation vector between the two cameras. This equation states that the position of $x_2$ is the position of $x_1$ in the first camera plus the translation vector T. This equation holds true for all corresponding points in the two views.
	- E = [0 -Tz Ty; Tz 0 -Tx; -Ty Tx 0]
	  where (Tx, Ty, Tz) is the translation vector.
	  Using this essential matrix, we can compute the fundamental matrix F as:$$F = inv(K') * E * inv(K)$$Where K is the camera calibration matrix.
	  Once we have the fundamental matrix F, we can use it to compute the epipolar lines in one view corresponding to the points in the other view. Given a point x1 in the first view, the corresponding epipolar line l2 in the second view can be computed as:$$l2 = F * x1$$Similarly, given a point x2 in the second view, the corresponding epipolar line l1 in the first view can be computed as:
	  $$l1 = F' * x2$$
	  Where F' is the transpose of F.
	  The epipolar lines give us a constraint on where the corresponding points can lie in the other view. Specifically, the point x2 in the second view must lie on the line l2, and the point x1 in the first view must lie on the line l1. By computing the intersection of these lines, we can obtain an estimate of the position of the camera relative to the two views.
- Describe a linear least squares algorithm for estimation of translation T . What is the minimal number of corresponding points needed to solve for T?
	- To estimate the translation vector T from a set of corresponding points x1 and x2 in two views, we can use a linear least squares algorithm. The algorithm seeks to minimize the sum of the squared errors between the observed corresponding points and their predicted positions based on the translation vector T. 
	- The steps of the linear least squares algorithm are as follows:
		1.  Construct the matrix A, where each row represents a pair of corresponding points (x1_i, x2_i) and its corresponding translation vector T. Since the translation vector T has two components, the matrix A is of size N x 2, where N is the number of corresponding points.
		2.  Construct the vector b, which is a column vector of size N x 1, containing the difference between the observed positions of the corresponding points in the two views.
		3.  Solve the linear system of equations Ax = b using the least squares method. This will result in an estimate of the translation vector T that minimizes the sum of the squared errors.
	- The minimal number of corresponding points needed to solve for T is two, since a translation vector can be uniquely defined by two points in the two views. However, having more than two points will generally lead to a more accurate estimate of the translation vector, as it reduces the effect of noise and outliers in the data.
- Suppose now that the camera is not calibrated and you can measure only pixel coordinates points $x^′_1$ and $x^′_2$ Can you still recover the translation between the two views?
	- No
- If we know all the intrinsic camera parameters except the focal length, we cannot directly recover the translation between the two views. The focal length is a crucial parameter that determines the scale of the 3D reconstruction, and hence, we need to estimate it in order to recover the translation.
