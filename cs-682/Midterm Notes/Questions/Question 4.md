To compute the relative orientation of the camera and the field, we can use the following steps:

1.  Calculate the coordinates of the four corners of the field in 3D space using their image coordinates and the known focal length of the camera. We can assume that the z-coordinate of the plane is zero since the field is perfectly planar.
    
2.  Estimate the homography matrix H that maps the points in 3D space to their image coordinates using a method like Direct Linear Transformation (DLT).
    
3.  Decompose the homography matrix H into its constituent parts, i.e., the rotation matrix R and the translation vector t, using methods like RQ decomposition or Singular Value Decomposition (SVD).
    
4.  Check the sign of the third element of the translation vector t. If it is negative, then the plane is in front of the camera. Otherwise, the plane is behind the camera.
    
5.  Compute the distance between the camera and the plane using the formula $d = |t_3|/||n||$, where $t_3$ is the third element of the translation vector t, and ||n|| is the norm of the normal vector n of the plane, which can be computed as the cross product of the vectors connecting the three points (in 3D space) that do not lie on a line.
    

In this particular case, the image coordinates of the four corners of the field are (-30,-50), (120,-30), (100,50), (-30,60), and the focal length of the camera is known. We can assume that the field lies on the x-y plane, with the z-coordinate being zero. Therefore, the coordinates of the four corners of the field in 3D space are (-50, -100, 0), (50, -120, 0), (50, 100, 0), and (-50, 120, 0). Using these coordinates and the image coordinates, we can estimate the homography matrix H. We can then decompose H into R and t, and compute the distance between the camera and the plane using the formula mentioned above.