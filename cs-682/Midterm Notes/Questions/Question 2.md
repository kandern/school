1. Results:$$
\begin{bmatrix}
0& 0& 0& 0& 0& 0& 0& 0& 0& 0&\\
0& 0& 0& 0& 0& 0& 0& 0& 0& 0\\
0& 0& 1& 1& 0& 0& -1& -1& 0& 0\\
0& 0& 1& 1& 0& 0& -1& -1& 0& 0\\
0& 0& 1& 1& 0& 0& -1& -1& 0& 0\\
0& 0& 1& 1& 0& 0& -1& -1& 0& 0\\
0& 0& 1& 1& 0& 0& -1& -1& 0& 0\\
0& 0& 0& 0& 0& 0& 0& 0& 0& 0\\
0& 0& 0& 0& 0& 0& 0& 0& 0& 0
\end{bmatrix}$$
2. Compute gradient magnitude at pixels (4,4), (6,4) and (6,7) (marked with ∗ in the image
	- Compute the horizontal value
	- Compute the vertical value 
	- The result is$magnitude = sqrt(horizontal\_gradient^2 + vertical\_gradient^2)$
	- (4,4)
		- 1
		- 1
		- $sqrt(1^2+1^2) = sqrt(2)$
	- (6,4)
		- 1
		- 0
		- $sqrt(1^2+0)=1$
	- 6,7
		- -1
		- 0
		- $sqrt(-1^2+0)=1$
3. Compute gradient direction at those same points.
	1. arcTan2(Gy,Gx)
	2. (4,4)
		1. arcTan2(1,1)
	- (6,4)
		1. arcTan2(0,1)
	- 6,7
		1. arcTan2(0,-1)
4. Describe in words what does the non-maximum suppression step on the gradi-  
ent magnitude in edge detection process accomplish ?
 - It reduces the noise on the output to get a more obvious where the edge is. this reduces the number of false edges.