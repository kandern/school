#! /usr/bin/python
import numpy as np
import matplotlib.pyplot as plt
import cv2
from scipy import ndimage
from scipy import spatial
#https://cs.gmu.edu/~kosecka/cs682/hw3_cs682_S23.html
def readImage(path):
    colorImg = cv2.imread(path)
    bwImg    = cv2.cvtColor(colorImg, cv2.COLOR_BGR2GRAY)
    return colorImg,bwImg

'''
Detect feature points in both images. Use the OpenCV library to
  extract keypoints through the function cv2.xfeatures2D.SIFT_create().detect and compute descripters through
  the function cv2.xfeatures2D.SIFT_create().compute.
  This tutorial provides details about using SIFT in OpenCV. https://docs.opencv.org/3.1.0/da/df5/tutorial_py_sift_intro.html
'''
def detectAndComputeFeatures(bwImage):
    sift = cv2.SIFT_create()
    # from tutorial
    kp, des = sift.detectAndCompute(bwImage, None)
    return kp, des

'''
Compute distances between every descriptor in one image and every descriptor in the other image.
  In Python, you can use scipy.spatial.distance.cdist(X,Y,'sqeuclidean') for fast computation of Euclidean distance.
  If you are not using SIFT descriptors, you should experiment with computing normalized correlation,
  or Euclidean distance after normalizing all descriptors to have zero mean and unit standard deviation.
'''
def getDistances(leftDes,rightDes):
    return spatial.distance.cdist(leftDes, rightDes, 'sqeuclidean')

'''
Select putative matches based on the matrix of pairwise descriptor distances obtained above.
  You can select all pairs whose descriptor distances are below a specified threshold,
  or select the top few hundred descriptor pairs with the smallest pairwise distances.
'''
def getPutativeMatches(distances, threshold):
    putativeMatches = []
    allDistances = []
    for Y in range(distances.shape[0]):
        for X in range(distances.shape[1]):
            allDistances.append(distances[Y,X])
            if distances[Y,X] < distanceThreshold:
                putativeMatches.append([Y,X])
    return putativeMatches, allDistances

'''
Implement RANSAC to estimate a homography mapping one image onto the other.
  Report the number of inliers and the average residual for the inliers
  (squared distance between the point coordinates in one image and the transformed
  coordinates of the matching point in the other image). Also, display the locations of inlier matches in both images.
'''
def RANSAC(putativeMatches, leftKp, rightKp, iterations,threshold):
    
    '''
    For RANSAC, a very simple implementation is sufficient. 
    Use four matches to initialize the homography in each iteration. 
    You should output a single transformation that gets the most inliers in the course of all the iterations. 
    For the various RANSAC parameters (number of iterations, inlier threshold), play around with a few "reasonable" 
    values and pick the ones that work best.
    Homography fitting calls for homogeneous least squares. 
    The solution to the homogeneous least squares system AX=0 is obtained from the 
    SVD of A by the singular vector corresponding to the smallest singular value.
    In Python, U, s, V = numpy.linalg.svd(A) performs the singular value decomposition
    and V[len(V)-1] gives the smallest singular value.
    '''
    bestInliers = []
    bestAvgResidual = float('inf')

    for i in range(iterations):
        # Randomly sample 4 putative matches to estimate the homography
        sampleIndices = np.random.choice(len(putativeMatches), size=4, replace=False)
        sampleMatches = [putativeMatches[idx] for idx in sampleIndices]
        
        # Estimate homography using the sampled matches
        leftPts = np.array([leftKp[m[0]].pt for m in sampleMatches], dtype=np.float32)
        rightPts = np.array([rightKp[m[1]].pt for m in sampleMatches], dtype=np.float32)
        H, _ = cv2.findHomography(leftPts, rightPts, method=cv2.RANSAC)

        # Calculate inliers using the estimated homography and the threshold
        inliers = []
        residual = 0.0
        for match in putativeMatches:
            leftPt = np.array([leftKp[match[0]].pt[0], leftKp[match[0]].pt[1], 1], dtype=np.float32)
            rightPt = np.array([rightKp[match[1]].pt[0], rightKp[match[1]].pt[1], 1], dtype=np.float32)
            transformedLeftPt = np.dot(H, leftPt)
            transformedLeftPt /= transformedLeftPt[2]
            dist = np.linalg.norm(transformedLeftPt - rightPt)
            if dist < threshold:
                inliers.append(match)
                residual += dist**2

        # Update the best inliers and average residual if the current iteration has more inliers
        if len(inliers) > len(bestInliers):
            bestInliers = inliers
            bestAvgResidual = residual / len(inliers)

    return bestInliers, bestAvgResidual

# Also, display the locations of inlier matches in both images.
# def drawInliers(leftImg, rightImg, inliers):
#     numColors = len(inliers)
#     jumps = (255*3)
#     for i in range(nu)
    
# Load both images, convert to double and to grayscale.
leftColor ,leftBW = readImage("left.jpg")
rightColor ,rightBW = readImage("right.jpg")
# Detect feature points in both images.
leftKp, leftDes = detectAndComputeFeatures(leftBW)
rightKp, rightDes = detectAndComputeFeatures(rightBW)

# Compute distances between every descriptor in one image and every descriptor in the other image.
distances = getDistances(leftDes, rightDes)

# Select putative matches based on the matrix of pairwise descriptor distances obtained above.
distanceThreshold = 10000.0
putativeMatches, allDistances = getPutativeMatches(distances, distanceThreshold)
allDistances.sort()
print(sum(allDistances[:100])/len(allDistances[:100]))
print(len(putativeMatches))

# Implement RANSAC to estimate a homography mapping one image onto the other.
iterations = 1000
threshold = 1
inliers, avgResidual = RANSAC(putativeMatches, leftKp, rightKp, iterations, threshold)

#Also, display the locations of inlier matches in both images.

newImage = leftColor
newImage = cv2.cvtColor(newImage, cv2.COLOR_BGR2RGB)
