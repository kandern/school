#! /usr/bin/python
import os
import numpy as np
import cv2

def main():
  # Define the path to the training and testing image directories
  train_dir = 'train/'
  test_dir = 'test/'

  # Process labeled training images
  trainImages, trainLabels = loadImageSet('train/','s{index}.1.tif')
  testImages, testLabels = loadImageSet('test/','s{index}.2.tif')

  # Find mean μ and covariance matrix Σ
  mean_face = np.mean(trainImages)
  deviations = trainImages - mean_face
  covarianceMatrix = np.cov(deviations.T)

  # Find k principal components (eigenvectors of Σ) u1 ,...uk
  # Determine a reasonably low-dimension to project onto by looking at the eigenvalues of the covariance matrix.
  #   principal components:
  #   (wi1 ,...,wik ) = (u1T (xi – μ), ... , ukT (xi – μ))
  k = 10
  principalComponents = []
  eigenvalues, eigenvectors = np.linalg.eig(covarianceMatrix)
  eigenZip = list(zip(eigenvalues,eigenvectors))
  eigenZip.sort(key = lambda x: x[0],reverse=True)
  for i in range(k):
      principalComponents.append(eigenZip[i][1])
  sorted_indices = np.argsort(eigenvalues)[::-1]
  eigenvalues = eigenvalues[sorted_indices]
  eigenvectors = eigenvectors[:, sorted_indices]
  top_eigenvectors = eigenvectors[:, :k]

  # Project the training images onto the eigenfaces
  train_feature_vectors = np.dot(deviations, top_eigenvectors)

  # Project the testing images onto the eigenfaces
  test_deviations = testImages - mean_face
  test_feature_vectors = np.dot(test_deviations, top_eigenvectors)

  # Recognize new images
  correct = 0
  for i, test_feature_vector in enumerate(test_feature_vectors):
    # Calculate Euclidean distances between test feature vector and all training feature vectors
    distances = np.sqrt(np.sum((train_feature_vectors - test_feature_vector)**2, axis=1))
    
    # Find the nearest neighbor and assign the test image to its class
    nearest_index = np.argmin(distances)
    nearest_label = trainLabels[nearest_index]
    
    # Check if the prediction is correct and update the accuracy counter
    if nearest_label == testLabels[i]:
      correct += 1

  # Print the accuracy
  accuracy = correct / len(testImages)
  print(f"Accuracy: {accuracy*100:.2f}%")

# make arrays out of the greyscale images
def loadImageSet(imageDir,nameFormat):
  images = []
  labels = []
  for image in os.listdir(imageDir):
    images.append(cv2.imread(os.path.join(imageDir, image), cv2.IMREAD_GRAYSCALE).flatten())
    labels.append(len(labels))
  images = np.array(images)
  labels = np.array(labels)
  return images, labels

main()