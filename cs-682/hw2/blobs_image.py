#! /bin/python

# downsamples the image to find blobs

#https://cs.gmu.edu/~kosecka/cs682/hw2_cs682_S23.html
#https://cs.gmu.edu/~kosecka/cs682/cs682_hw2_S23.pdf
import cv2
import numpy as np
import sys
import getopt
import operator
import math
import matplotlib.pyplot as plt
import matplotlib.image as mpim
from scipy import ndimage
from skimage.transform import resize
import time
class blob:
    def __init__(self, pos=(0,0), radius=1, color=(0,0,255)):
        self.pos = pos
        self.radius = radius
        self.color = color

    def __str__(self):
        s = "blob" +\
        '\n  pos:' + str(self.pos) +\
        '\n  radius:' + str(self.radius) +\
        '\n  color:' + str(self.color)
        return s

'''
Generate a Laplacian of Gaussian filter.
   For creating the Laplacian filter, use the ndimage.gaussian_laplace function.
   Pay careful attention to setting the right filter mask size. Hint: Should the filter width be odd or even?
'''
def getFilter(img, size, sigma):
    # keep filter size odd
    newSize = round(size * sigma)
    if round(size * sigma) %2 == 0:
        newSize += 1
    #Filter image with scale-normalized Laplacian at current scale.
    #why is filter a reserved word in python...
    #My god this took forever to figure out, I thought it made a filter we convolve with,
    #  not that it did the convolution
    f = ndimage.gaussian_laplace(img, sigma)
    return f

'''
Build a Laplacian scale space, starting with some initial scale and going for n iterations:
     Filter image with scale-normalized Laplacian at current scale.
     Save square of Laplacian response for current level of scale space.
     Increase scale by a factor k.

     scale_space = numpy.empty((h,w,n)) # [h,w] - dimensions of image, n - number of levels in scale space
     Then scale_space[:,:,i] would give you the i-th level of the scale space.
'''
def buildScaleSpace(image, initSigma, level, scale, size=15):
    h, w = image.shape[:2]
    scaleSpace = np.empty(level, dtype=object)
    scaledImage = image
    for k in range(level):
        #print('  ' + str(k) + '/' + str(level))
        # apply filter
        filteredImage = getFilter(scaledImage, size, sigma=initSigma)
        # Save square of Laplacian response for current level of scale space.
        response = filteredImage ** 2
        scaleSpace[k] = response
        # downsample the image by a factor of 2
        downsampledSize = (scaledImage.shape[0]//scale, scaledImage.shape[1]//scale)
        scaledImage = resize(scaledImage, downsampledSize)
    return scaleSpace

'''
Perform nonmaximum suppression in scale space.
    To perform nonmaximum suppression in scale space, you should first do nonmaximum suppression in each 2D slice separately.
    For this, you may find functions scipy.ndimage.filters.rank_filter or scipy.ndimage.filters.generic_filter useful.
    Play around with these functions, and try to find the one that works the fastest. 
    To extract the final nonzero values (corresponding to detected regions), you may want to use the numpy.clip function.
'''
def nonMaximumSuppression1(scaleSpace,footprint):
    # footprint is required despite the docs saying it isn't
    for level in range(scaleSpace.shape[0]):
        max_filtered = ndimage.rank_filter(scaleSpace[level], rank=-1, footprint=footprint)

        # Suppress non-maximum values
        for i in range(scaleSpace[level].shape[0]):
            for j in range(scaleSpace[level].shape[1]):
                if scaleSpace[level][i, j] < max_filtered[i, j]:
                    scaleSpace[level][i, j] = 0
        # Clip values to extract final nonzero values
        scaleSpace[level] = np.clip(scaleSpace[level], 0, np.inf)
    return scaleSpace

def nonMaximumSuppression2(scaleSpace,footprint):
    # footprint is required despite the docs saying it isn't
    for level in range(scaleSpace.shape[2]):
        max_filtered = ndimage.rank_filter(scaleSpace[:, :, level], rank=-1, footprint=footprint)
        # Suppress non-maximum values
        for i in range(scaleSpace.shape[0]):
            for j in range(scaleSpace.shape[1]):
                if scaleSpace[i, j, level] < max_filtered[i, j]:
                    scaleSpace[i, j, level] = 0
    scaleSpace = np.clip(scaleSpace, 0, np.inf)
    return scaleSpace

# ?upsample? the scale-space so that we can perform non-maximum supression like in the filter implementation
def upsampleScaleSpace(oldScaleSpace,scale):
    h, w = oldScaleSpace[0].shape
    scaleSpace = np.zeros((h, w, len(oldScaleSpace)))
    for level in range(len(oldScaleSpace)):
        responses = oldScaleSpace[level]
        downsampledSize = responses.shape
        for y in range(oldScaleSpace[level].shape[0]):
            for x in range(oldScaleSpace[level].shape[1]):
                downsampledPos = (y, x)
                upsampleRatio = (h/downsampledSize[0],w/downsampledSize[1])
                scaledPos = (round(downsampledPos[0]*upsampleRatio[0]),round(downsampledPos[1]*upsampleRatio[1]))
                originalPos = (min(max(0,scaledPos[0]),h),min(max(0,scaledPos[1]),w))
                scaleSpace[originalPos[0],originalPos[1],level] = responses[y,x]*(scale*(len(oldScaleSpace)-level))
    return scaleSpace


#scale-space blob detection
def findBlobs(colorImage):
    img = cv2.cvtColor(colorImage, cv2.COLOR_RGB2GRAY) / 255.0
    maskSize = 15
    initSigma = 5
    scale = 2
    levels = 10
    threshold = 0.0001
    #since we are down scaling the image, we have to make sure we don't divide by 0  by doing too many levels
    h, w = img.shape[:2]
    minLevels = 0
    footprint = np.array([[1, 1, 1, 1, 1], [1, 1, 1, 1, 1], [1, 1, 1, 1, 1]])

    while minLevels >= 0:
        minLevels += 1
        h = h//scale
        w = w//scale
        if h == 0 or w == 0:
            minLevels -= 1
            break
    levels = min(levels,minLevels)
    
    #Build a Laplacian scale space, starting with some initial scale and going for n iterations:
    print("buildingScaleSpace")
    sp = buildScaleSpace(img, initSigma, levels, scale, maskSize)
    #Perform nonmaximum suppression in scale space.
    print("performing non-maximum suppression")
    sp = nonMaximumSuppression1(sp,footprint)
    sp = upsampleScaleSpace(sp,scale)
    sp = nonMaximumSuppression2(sp,footprint)
    print("getting blobs who exceed our threshold")
    # Calculate response for each blob
    responses = []
    res = []
    for i in range(sp.shape[0]):
        for j in range(sp.shape[1]):
            for k in range(sp.shape[2]):
                res.append(sp[i, j, k])
                if sp[i, j, k] > threshold:
                    responses.append((i, j, k, sp[i, j, k]))
    print(max(res))
    print(sum(res)/len(res))

    # Create list of blobs
    print("creating list of blobs")
    blobs = []
    for i in range(len(responses)):
        r = responses[i]
        pos = (r[1], r[0])
        radius = int(initSigma * scale ** r[2])
        blobs.append(blob(pos, radius))

    return blobs

def drawCircle(image, blob):
    cv2.circle(image, blob.pos, blob.radius, blob.color, thickness=1, lineType=8, shift=0)
    return image

def main():
    img_name = sys.argv[1]
    saveName=img_name
    img_name = "images/"+img_name+".jpg"
    img = cv2.imread(img_name)
    start = time.time()
    blobList = findBlobs(img)
    end = time.time()
    print(len(blobList))
    print("efficient:")
    print("  " + str(end - start))
    for blob in blobList:
        img = drawCircle(img, blob)
    if img is not None:
        cv2.imwrite(saveName + ".png", img)


if __name__ == "__main__":
    main()