#! /bin/python
#https://cs.gmu.edu/~kosecka/cs682/hw2_cs682_S23.html
#https://cs.gmu.edu/~kosecka/cs682/cs682_hw2_S23.pdf
"""

Usage example:
python harris_corner.py --window_size 5 --alpha 0.04 --corner_threshold 10000 hw3_images/butterfly.jpg

"""

import cv2
import numpy as np
import sys
import getopt
import operator
import math
import matplotlib.pyplot as plt
import matplotlib.image as mpim
from scipy import ndimage

def findCorners(img, window_size, k, thresh):
    """
    Finds and returns list of corners and new image with corners drawn
    :param img: The original image
    :param window_size: The size (side length) of the sliding window
    :param k: Harris corner constant. Usually 0.04 - 0.06
    :param thresh: The threshold above which a corner is counted
    :return:
    """
    height = img.shape[0]
    width = img.shape[1]

    #Find x and y derivatives
    dy, dx = np.gradient(img)
    Ixx = dx**2
    Ixy = dy*dx
    Iyy = dy**2

    cornerList = []
    newImg = img.copy()
    color_img = cv2.cvtColor(newImg, cv2.COLOR_GRAY2RGB)
    #x-offset -> x+offset = 
    offset = math.floor(window_size/2)
    print(len(Ixx[0]))
    if Ixx is not None:
        cv2.imwrite("Ixx.png", dx)
    if Iyy is not None:
        cv2.imwrite("Iyy.png", dy)
    if Iyy is not None and Iyy is not None:
        cv2.imwrite("gradientMagnitude.png", np.sqrt(Ixx+Iyy))
    if Iyy is not None and Iyy is not None:
        radToPi = 180 / np.pi
        cv2.imwrite("gradientOrientation.png",np.arctan2(dy, dx) * radToPi)

    # Loop through image and find our corners
    # and do non-maximum supression
    # this can be also implemented without loop
    print("Finding Corners...")
    #list of all r values, we will use it for non maximum suppression
    rs = []
    tots = []
    for y in range(offset, height-offset):
        rs.append([])
        for x in range(offset, width-offset):
             
            #your code goes here
            #Hint: given second moment matrix instead of computing eigenvalues 
            #and eigenvectors you can compute the corner response funtion 
            Mxx = np.sum(Ixx[y-offset:y+offset+1, x-offset:x+offset+1])
            Mxy = np.sum(Ixy[y-offset:y+offset+1, x-offset:x+offset+1])
            Myy = np.sum(Iyy[y-offset:y+offset+1, x-offset:x+offset+1])

            #using trace and det
            #Find determinant and trace, use to get corner response
            det = (Mxx * Myy) - (Mxy**2)
            trace = Mxx + Myy
            r = det - k*(trace**2)
            tots.append(r)
            if r > thresh:
                cornerList.append([y,x,r])
                rs[y-offset].append([y,x,r])
            else:
                rs[y-offset].append([y,x,0])
    #non-maximum supression
    #I eventually figured out numpy rank filter, but this works, so oh well
    print("Finding local Maximums...")
    maximumCorners = []
    for i in range(len(cornerList)):
        corner = cornerList[i]
        isMax = True
        for y in range(max(0,y-offset), min(len(rs)-1,corner[0]+offset)):
            for x in range(max(0,y-offset), min(corner[1]+offset,len(rs[0])-1)):
                print(str(corner[0]+offset) + ',' + str(corner[1]+offset))
                print(str(len(rs)) + ',' + str(len(rs[0])))
                if rs[y][x][2] > corner[2]:
                    isMax = False
                    break
            if not isMax:
                break
        if isMax:
            maximumCorners.append(corner)

    print("aplying color...")
    for corner in maximumCorners:
        #remember in y,x,[r,g,b] format
        color_img.itemset((int(corner[0]), int(corner[1]), 0), 0)
        color_img.itemset((int(corner[0]), int(corner[1]), 1), 0)
        color_img.itemset((int(corner[0]), int(corner[1]), 2), 255)

    return color_img

def main():
    """
    Main parses argument list and runs findCorners() on the image
    :return: None
    """
    args, img_name = getopt.getopt(sys.argv[1:], '', ['window_size=', 'alpha=', 'corner_threshold='])
    args = dict(args)
    print(args)
    window_size = args.get('--window_size')
    k = args.get('--alpha')
    thresh = args.get('--corner_threshold')

    print("Image Name: " + str(img_name[0]))
    print("Window Size: " + str(window_size))
    print("K alpha: " + str(k))
    print("Corner Response Threshold:" + thresh)
    img = cv2.imread(img_name[0])
    grayImage = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
    img = findCorners(grayImage, int(window_size), float(k), int(thresh))

    if img is not None:
        cv2.imwrite("out.png", img)



if __name__ == "__main__":
    main()