#! /bin/python

# increases the filter to find blobs

#https://cs.gmu.edu/~kosecka/cs682/hw2_cs682_S23.html
#https://cs.gmu.edu/~kosecka/cs682/cs682_hw2_S23.pdf

import cv2
import numpy as np
import sys
import getopt
import operator
import math
import time
import matplotlib.pyplot as plt
import matplotlib.image as mpim
from scipy import ndimage
class blob:
    def __init__(self, pos=(0,0), radius=1, color=(0,0,255)):
        self.pos = pos
        self.radius = radius
        self.color = color
'''
Generate a Laplacian of Gaussian filter.
   For creating the Laplacian filter, use the ndimage.gaussian_laplace function.
   Pay careful attention to setting the right filter mask size. Hint: Should the filter width be odd or even?
'''
def getFilter(img, size, sigma, level, scale):
    # keep filter size odd
    newSize = round(size * sigma)
    if round(size * sigma) %2 == 0:
        newSize += 1
    #Filter image with scale-normalized Laplacian at current scale.
    #why is filter a reserved word in python...
    #My god this took forever to figure out, I thought it made a filter we convolve with,
    #  not that it did the convolution
    f = ndimage.gaussian_laplace(img, sigma)
    return f

'''
Build a Laplacian scale space, starting with some initial scale and going for n iterations:
     Filter image with scale-normalized Laplacian at current scale.
     Save square of Laplacian response for current level of scale space.
     Increase scale by a factor k.

     scale_space = numpy.empty((h,w,n)) # [h,w] - dimensions of image, n - number of levels in scale space
     Then scale_space[:,:,i] would give you the i-th level of the scale space.
'''
def buildScaleSpace(image, initSigma, level, scale, size=15):
    h, w = image.shape[:2]
    scaleSpace = np.empty((h, w, level))
    scaledSigma = initSigma
    for k in range(level):
        #print('  ' + str(k) + '/' + str(level))
        # apply filter
        filteredImage = getFilter(image, size, scaledSigma, k, scale)
        # Save square of Laplacian response for current level of scale space.
        scaleSpace[:, :, k] = filteredImage ** 2 / scaledSigma ** 2
        # sigma(k+1) = 1.25*sigma 
        scaledSigma *= scale
    return scaleSpace

'''
Perform nonmaximum suppression in scale space.
    To perform nonmaximum suppression in scale space, you should first do nonmaximum suppression in each 2D slice separately.
    For this, you may find functions scipy.ndimage.filters.rank_filter or scipy.ndimage.filters.generic_filter useful.
    Play around with these functions, and try to find the one that works the fastest. 
    To extract the final nonzero values (corresponding to detected regions), you may want to use the numpy.clip function.
'''
def nonMaximumSuppression(scaleSpace,footprint):
    # footprint is required despite the docs saying it isn't
    for level in range(scaleSpace.shape[2]):
        max_filtered = ndimage.rank_filter(scaleSpace[:, :, level], rank=-1, footprint=footprint)
        # Suppress non-maximum values
        for i in range(scaleSpace.shape[0]):
            for j in range(scaleSpace.shape[1]):
                if scaleSpace[i, j, level] < max_filtered[i, j]:
                    scaleSpace[i, j, level] = 0
    scaleSpace = np.clip(scaleSpace, 0, np.inf)
    return scaleSpace

#scale-space blob detection
def findBlobs(colorImage):
    img = cv2.cvtColor(colorImage, cv2.COLOR_RGB2GRAY) / 255.0
    maskSize = 2
    initSigma = 10
    sigmaScale = 2
    levels = 10
    threshold = 0.000000000001
    footprint = np.array([[1, 1, 1, 1, 1], [1, 1, 1, 1, 1], [1, 1, 1, 1, 1]])
    
    print("buildingScaleSpace")
    #Build a Laplacian scale space, starting with some initial scale and going for n iterations:
    sp = buildScaleSpace(img, initSigma, levels, sigmaScale, maskSize)
    #Perform nonmaximum suppression in scale space.
    print("performing non-maximum suppression")
    sp = nonMaximumSuppression(sp,footprint)
    print("getting blobs who exceed our threshold")
    #filteredScaleSpace = np.where(sp >= 0)
    print("creating list of blobs")
    # Calculate response for each blob
    responses = []
    res = []
    for i in range(sp.shape[0]):
        for j in range(sp.shape[1]):
            for k in range(sp.shape[2]):
                res.append(sp[i, j, k])
                if sp[i, j, k] > threshold:
                    responses.append((i, j, k, sp[i, j, k]))
    print(max(res))
    print(sum(res)/len(res))
    # Create list of blobs
    blobs = []
    for i in range(len(responses)):
        r = responses[i]
        pos = (r[1], r[0])
        radius = int(initSigma * sigmaScale ** r[2])
        blobs.append(blob(pos, radius))

    return blobs

def drawCircle(image, blob):
    cv2.circle(image, blob.pos, blob.radius, blob.color, thickness=1, lineType=8, shift=0)
    return image

def main():
    img_name = sys.argv[1]
    img = cv2.imread(img_name)
    start = time.time()
    blobList = findBlobs(img)
    end = time.time()
    print(len(blobList))
    print("inefficient:")
    print("  " + str(end - start))
    for blob in blobList:
        img = drawCircle(img, blob)
    
    if img is not None:
        cv2.imwrite("inefficient.png", img)


if __name__ == "__main__":
    main()