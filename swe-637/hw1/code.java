// Online Java Compiler
// Use this editor to write, compile and run your Java code online

class HelloWorld {
  public static void main(String[] args) {
      int[] x = new int[]{0,1,0};
      System.out.println(lastZero(x));
  }
  
  public static int oddOrPos(int[] x) {
    int count = 0;
    for (int i = 0; i < x.length; i++) {
      if (x[i]%2 == 1 || x[i] > 0) {
        count ++;
      }
    }
    return count;
  }

 public static int findLast(int[] x, int y) { 
    for (int i=x.length-1; i >= 0; i--) {
       if (x[i] == y) {
          return i;
       }
    }
    return -1;
 }
 
 public static int lastZero(int[] x) {
    for (int i = 0; i < x.length; i++) {
       if (x[i] == 0) {
          return i;
       }
    }
    return -1;
 }
 
}