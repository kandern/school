import static org.junit.Assert.*;

import org.junit.Test;


public class TestMeTest {
    @Test
    public void testGetFive(){
        assertNotEquals("that's not a five.", 4, TestMe.getFive());
        assertEquals("didn't get Five", 5, TestMe.getFive());
    }

    @Test
    public void testGetSix(){
        assertNotEquals("didn't get Five", 5, TestMe.getSix());
        assertEquals("got not 6", 6, TestMe.getSix());
    }

    @Test
    public void testTimes(){
        assertEquals("No multiply", 31, TestMe.getFiveTimesSix(), 1);
    }
}
