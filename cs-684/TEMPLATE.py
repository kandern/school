def recalculateXXX(rank1,rank2,winner):
    scores = [0,0]
    scores[winner] = 1
    e1,e2 = calculateExpectationsXXX(rank1,rank2)
    trank1 = rank1 + 10 * (scores[0] - e1)
    trank2 = rank2 + 10 * (scores[1] - e2)
    return (trank1,trank2)

def predictXXX(rank1,rank2):
    if rank1>rank2:
        return 0
    else:
        return 1

def readRecordsXXX():
    recordFile =  open("records.csv")
    combatents = {}
    rows = recordFile.readlines()
    for row in rows:
        features = row.split(',')
        if features[0] not in combatents:
            combatents[features[0]] = {"rank":1000,
                                        "RD":50}
        if features[1] not in combatents:
            combatents[features[1]] = {"rank":1000,
                                        "RD":50}
    for row in rows:
        features = row.split(',')
        rank1 = combatents[features[0]]["rank"]
        rank2 = combatents[features[1]]["rank"]
        rank1,rank2 = recalculateXXX(rank1,rank2,int(features[2]))
        combatents[features[0]]["rank"] = rank1
        combatents[features[1]]["rank"] = rank2
    recordFile.close()
    return combatents
