from trueskill import Rating, quality_1vs1, rate_1vs1

def predictTRU(rank1,rank2):
    if rank1.mu > rank2.mu:
        return 0
    else:
        return 1

def readRecordsTRU():
    recordFile =  open("records.csv")
    combatents = {}
    rows = recordFile.readlines()
    for row in rows:
        features = row.split(',')
        if features[0] not in combatents:
            combatents[features[0]] = {"rank":Rating(25)}
        if features[1] not in combatents:
            combatents[features[1]] = {"rank":Rating(25)}
    for row in rows:
        features = row.split(',')
        rank1 = combatents[features[0]]["rank"]
        rank2 = combatents[features[1]]["rank"]
        if int(features[2]) == 0:
            rank1,rank2 = rate_1vs1(rank1,rank2)
        else:
            rank2,rank1 = rate_1vs1(rank2,rank1)
        combatents[features[0]]["rank"] = rank1
        combatents[features[1]]["rank"] = rank2
    recordFile.close()
    return combatents
