def calculateExpectationsELO(elo1,elo2):
    e1 = 1/(1+pow(10,(elo2-elo1)/400))
    e2 = 1/(1+pow(10,(elo1-elo2)/400))
    return (e1,e2)

def recalculateELO(elo1,elo2,winner):
    scores = [0,0]
    scores[winner] = 1
    e1,e2 = calculateExpectationsELO(elo1,elo2)
    telo1 = elo1 + 10 * (scores[0] - e1)
    telo2 = elo2 + 10 * (scores[1] - e2)
    return (telo1,telo2)

def predictELO(elo1,elo2):
    result1,result2 = calculateExpectationsELO(elo1,elo2)
    if result1 > result2:
        return 0
    else:
        return 1

def readRecordsELO():
    recordFile =  open("records.csv")
    combatents = {}
    rows = recordFile.readlines()
    for row in rows:
        features = row.split(',')
        if features[0] not in combatents:
            combatents[features[0]] = {"rank":1000,
                                        "wins":0,
                                        "losses":0}
        if features[1] not in combatents:
            combatents[features[1]] = {"rank":1000,
                                        "wins":0,
                                        "losses":0}
        combatents[features[int(features[2])]]["wins"] = combatents[features[int(features[2])]]["wins"]+1
    for row in rows:
        features = row.split(',')
        elo1 = combatents[features[0]]["rank"]
        elo2 = combatents[features[1]]["rank"]
        elo1,elo2 = recalculateELO(elo1,elo2,int(features[2]))
        combatents[features[int(features[2])]]["wins"] = combatents[features[int(features[2])]]["wins"]+1
        combatents[features[0]]["rank"] = elo1
        combatents[features[1]]["rank"] = elo2
    recordFile.close()
    return combatents
