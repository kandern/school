def recalculateWIN(rank1,rank2,winner):
    trank1 = rank1
    trank2 = rank2
    if winner == 0:
        trank1 += 1
    else:
        trank2 += 1
    return (trank1,trank2)

def predictWIN(rank1,rank2):
    if rank1>rank2:
        return 0
    else:
        return 1

def readRecordsWIN():
    recordFile =  open("records.csv")
    combatents = {}
    rows = recordFile.readlines()
    for row in rows:
        features = row.split(',')
        if features[0] not in combatents:
            combatents[features[0]] = {"rank":0}
        if features[1] not in combatents:
            combatents[features[1]] = {"rank":0}
        if features[2] == 0:
            combatents[features[0]]["rank"] += 1
        else:
            combatents[features[1]]["rank"] += 1
    recordFile.close()
    return combatents
