import sys
import re
sys.path.append(".")
from twitchio.ext import commands
from ranker import Ranker
from utils import readRecordsNAMES

combatents = readRecordsNAMES()
rankers = [ Ranker("elo"),
            Ranker("tru"),
            Ranker("win"),
            Ranker("los"),
            Ranker("ran")
          ]
RED = ""
BLUE = ""
popular = 0
numCorrect = 0
count = 0
record = False
text = []
redText = []
blueText = []
tmi_token = ""
clientID = ""
name = ""
channel = ""
#Setup Listeners
bot = commands.Bot(
    # set up the bot
    irc_token=tmi_token,
    client_id=clientID,
    nick=name,
    prefix="`",
    initial_channels=[channel]
)
@bot.event
async def event_ready():
    print("Connected to Saltybet")
@bot.event
async def event_message(ctx):
    global RED
    global BLUE
    global popular
    global count
    global numCorrect
    global record
    txt = ctx.content
    if record:
        text.append(txt)
    # make sure the bot ignores itself and the streamer
    if ctx.author.name.lower() != "WAIFU4u".lower():
        return
    print(txt)
    #stating matchup
    if txt.find("Bets are OPEN for ") != -1:
        record = True
        txt = txt.replace("Bets are OPEN for ",'')
        txt = txt.replace(" vs ","UNIQUESTRINGFORSPLITTING")
        txt = txt[0:txt.find("! (")]
        names = txt.split('UNIQUESTRINGFORSPLITTING')
        matchupSTR = ""
        if names[0] in combatents and names[1] in combatents:
            RED = names[0]
            BLUE = names[1]
            matchupSTR = names[0] + " vs " + names[1]
            for ranker in rankers:
                ranker.predict(names[0],names[1])
        print(matchupSTR)
    if txt.find("Payouts to") != -1:
        #Blue won
        winner = 0
        if txt.find("Payouts to Team Blue") != -1:
            for line in text:
                blueText.append(line)
            winner = 1
        else:
            for line in text:
                redText.append(line)
        count +=1
        if winner == popular:
            numCorrect += 1
        f = open("out.csv", "w")
        f.write("system,accuracy\n")
        for ranker in rankers:
            ranker.verify(winner)
            print(ranker.toCSV())
            f.write(ranker.toCSV() + "\n")
        print("pop," + str(float(numCorrect/count)))
        f.write("pop," + str(float(numCorrect/count))+ "\n")
        f.close()
    if txt.find("Bets are locked") != -1:
        amounts = re.findall("\$[\d,]*",txt)
        redBet = int(amounts[0].replace("$","").replace(",",""))
        blueBet = int(amounts[1].replace("$","").replace(",",""))
        if redBet > blueBet:
            popular = 0
        else:
            popular = 1
        f = open("red.csv","w")
        for line in redText:
            f.write(line +"\n")
        f.close()
        f = open("blue.csv","w")
        for line in blueText:
            f.write(line +"\n")
        f.close()
        record = False


bot.run()
