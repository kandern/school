Nicholas Anderson [g-number: G00895684]
Project 1
Requirements Implemented:
- The Plexiglass pyramid
- The unity project for displaying the hologram

Requirements Not Implemented:
- none

Extra Credits:
- none

Other Notes:
- Sorry the video isn't the best, My phone is too small for the size of the pyramid, so I used a laptop, and it was hard to get all 4 sides because of the keyboard being in the way.
