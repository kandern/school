David Glass [g-number: 01234567]
Project 9

Requirements Implemented:
- the function "intersect" under the Sphere struct to determine the intersection between a ray and the sphere
- the Phong's reflection model 
- recursive ray-tracing 

Requirements Not Implemented:
- specular reflection
- total internal reflection

Extra Credits:
- added GUI to control transparency of material (to test, click "transparency" button under the menu bar to test)
- added shadow effect (to test, press "s" to trigger the effect)

Other Notes:
- none
