Nicholas Anderson [g-number: 0859684]
Project 2

Requirements Implemented:
- Created Cardboard Headset
- Created project as described so that I have two cameras offset

Requirements Not Implemented:
- NA

Extra Credits:
- I used an animated Model, Sorry if it isn't too exagerated. It just has an idle animation
- Head Tracking using the gyroscope

Other Notes:
- The files are as follows: 
  - RenderedImage.png: Contains a pair of rendered images of the project
  - PhoneWithAnimation: Contains a video of me doing a closeup of the phone showing the gyroscopic effect, as well as the idle animation of the walnut character
  - HeadsetWithPhone: Contains a video of the phone while in the headset