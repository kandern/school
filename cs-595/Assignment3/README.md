Nicholas Anderson [g-number: 00895684]
Project 3

Requirements Implemented:
Part 2 image
Part 2 video
Part 3 video
Part 4 video
Part 5 video

Requirements Not Implemented:
- none

Extra Credits:
- none

Other Notes:
- part 5 is currently commented out, line 114-117
- the videos and images for the evidence are in the output folder. If you run the program, it will now attempt to save the output in the  same path in the output folder.
i.e python augmented_reality_with_aruco.py --video=/path/to/video.mp4 will attempt to save in ./output/path/to/video.mp4