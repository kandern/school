#version 330

uniform mat4 uProjectionMatrix;
uniform mat4 uViewMatrix;
uniform mat3 uNormalMatrix;
uniform float uTime;

in vec3 vPos;
in vec3 vNormal;
// in vec2 vTexCoord;

out vec3 fPos;
out vec3 fNormal;
// out vec2 fTexCoord;

void main()
{
    fPos = vPos;
    gl_Position = uProjectionMatrix * ( uViewMatrix * vec4(vPos, 1.0) );
}
