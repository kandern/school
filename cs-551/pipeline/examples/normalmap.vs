#version 330

uniform mat4 uProjectionMatrix;
uniform mat4 uViewMatrix;
uniform mat3 uNormalMatrix;

in vec3 vPos;
in vec3 vNormal;
in vec2 vTexCoord;
in vec3 vTangent;
in vec3 vBitangent;

out vec3 fPos;
out vec3 fNormal;
out vec2 fTexCoord;
out vec3 fTangent;
out vec3 fBitangent;


//n the vertex shader, you will convert the tangent and bitangent vectors from 
//world-space to eye-space (using uViewMatrix) and pass them to the fragment shader.
void main()
{
    // Your code goes here.
    fPos = vPos;
    fNormal = vNormal;
    fTexCoord = vTexCoord;
    vec4 fTangent4 = uViewMatrix*vec4(vTangent,0);
    fTangent = vec3(fTangent4.r,fTangent4.g,fTangent4.b);
    vec4 fBitangent4 = uViewMatrix*vec4(vTangent,0);
    fBitangent = vec3(fBitangent.r,fBitangent4.g,fBitangent4.b);
    // OpenGL requires that we put something for gl_Position.
    // This is wrong but will at least compile.
    gl_Position = uProjectionMatrix * ( uViewMatrix * vec4(vPos, 1.0) );
}
