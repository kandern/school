#version 330
uniform mat4 uViewMatrix;

// Eye-space.
in vec3 fPos;
in vec3 fNormal;
in vec2 fTexCoord;

uniform mat3 uNormalMatrix;

uniform samplerCube uEnvironmentTex;

uniform struct Material {
    vec3 color_ambient;
    vec3 color_diffuse;
    vec3 color_specular;
    float shininess;
    
    bool reflective;
    vec3 color_reflect;
    
    bool refractive;
    vec3 color_refract;
    float index_of_refraction;
    
    bool use_diffuse_texture;
    sampler2D diffuse_texture;
} material;

struct Light {
    // Eye-space.
    vec3 position;
    vec3 color;
    vec3 color_ambient;
};
const int MAX_LIGHTS = 5;
uniform Light lights[ MAX_LIGHTS ];

uniform int num_lights;

// gl_FragColor is old-fashioned, but it's what WebGL 1 uses.
// From: https://stackoverflow.com/questions/9222217/how-does-the-fragment-shader-know-what-variable-to-use-for-the-color-of-a-pixel
layout(location = 0) out vec4 FragColor;

void main() {
    vec3 c = vec3(0.0,0.0,0.0);
    vec3 origin = vec3(0,0,0);
    vec4 eye = inverse(uViewMatrix)*vec4(origin,1);
    vec3 eye3 = vec3(eye.x,eye.y,eye.z);
    vec3 N  = normalize(fNormal);
    //Reflections
    vec3 Kr = material.color_reflect;
    vec3 Ir = vec3(0.0,0.0,0.0);
    if(material.reflective){
        vec3 direction = reflect(fPos-eye3,N);
        vec4 Ir4 = texture(uEnvironmentTex,direction);
        Ir = vec3(Ir4.x,Ir4.y,Ir4.z);
    }
    c = Kr*Ir;

    for (int i = 0; i<MAX_LIGHTS;i++) {
        vec4 lPos4 = inverse(uViewMatrix)*vec4(lights[i].position,1);
        vec3 lPos = vec3(lPos4.x,lPos4.y,lPos4.z);
        //Ambient Light
        vec3 Ka = material.color_ambient;
        vec3 Ial = lights[i].color_ambient;
        if(i>num_lights) {
            break;
        }
        c+= (Ka*Ial);

        //Diffuse lighting
        vec4 K;
        if(material.use_diffuse_texture){
            K = vec4(material.color_diffuse,1) * texture(material.diffuse_texture,fTexCoord).rgba;
        }else{
            K = vec4(material.color_diffuse,1);
        }
        vec3 Kd = vec3(K.x,K.y,K.z);
        vec3 Il = lights[i].color;
        vec3 L  = normalize(lPos - fPos);
        c += (Kd * Il) * max(0.0,dot(N,L));

        vec3 Ks = material.color_specular;
        //Specular lighting
        vec3 V = normalize(eye3-fPos);
        vec3 R = reflect(normalize(fPos-lPos),N);
        float n = material.shininess;
        if(dot(N,L)>0.0){
            c+= Ks * Il * pow(max(0.0,dot(V,R)),n);
        }
    }
    // Color is the normal.
    FragColor = vec4(c, 1 );

}
