#version 330

in vec3 fNormal;
in vec3 fPos;
uniform float uTime;
// in vec2 fTexCoord;

uniform vec3 uColor;
// uniform sampler2D uTex;
// uniform samplerCube uTexCube;

// gl_FragColor is old-fashioned, but it's what WebGL 1 uses.
// From: https://stackoverflow.com/questions/9222217/how-does-the-fragment-shader-know-what-variable-to-use-for-the-color-of-a-pixel
layout(location = 0) out vec4 FragColor;

void main()
{

    if(fPos.y>(cos(10*(fPos.x+uTime/1.7)))/(20*(cos(uTime)+2)) + cos(uTime)/9-.05){
        if(fPos.y>(cos(10*(fPos.x+uTime/1.7)))/(10*(cos(uTime)+5)) + cos(uTime)/9){
            FragColor = vec4(0.0,1,1.0,1.0);
        }else{
            FragColor = vec4( 1.0, 1.0, 1.0, 1.0 );
        }
    }else if(fPos.y>(cos(uTime)+1)/2){
        FragColor = vec4( 0.0, max(.7+fPos.y,.2), 1.0, 1.0 );
    }
}
