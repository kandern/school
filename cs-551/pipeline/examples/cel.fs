#version 330
uniform mat4 uViewMatrix;

// Eye-space.
in vec3 fPos;
in vec3 fNormal;
in vec2 fTexCoord;

uniform mat3 uNormalMatrix;

uniform struct Material {
    vec3 color;
    float shininess;
    int bands;
    
    bool use_diffuse_texture;
    sampler2D diffuse_texture;
} material;

struct Light {
    // Eye-space.
    vec3 position;
    float color;
    float color_ambient;
};
const int MAX_LIGHTS = 5;
uniform Light lights[ MAX_LIGHTS ];

uniform int num_lights;

// gl_FragColor is old-fashioned, but it's what WebGL 1 uses.
// From: https://stackoverflow.com/questions/9222217/how-does-the-fragment-shader-know-what-variable-to-use-for-the-color-of-a-pixel
layout(location = 0) out vec4 FragColor;

//F = ∑L (IAL + [IL ( N · L ) + IL ( V · R )n])
void main()
{
    vec3 f = vec3(0.0,0.0,0.0);
    vec3 origin = vec3(0,0,0);
    vec4 eye = inverse(uViewMatrix)*vec4(origin,1);
    vec3 eye3 = vec3(eye.x,eye.y,eye.z);
    vec3 N  = normalize(fNormal);

    for (int i = 0; i<MAX_LIGHTS;i++) {
        vec4 lPos4 = inverse(uViewMatrix)*vec4(lights[i].position,1);
        vec3 lPos = vec3(lPos4.x,lPos4.y,lPos4.z);
        if(i>num_lights) {
            break;
        }
        //Ambient Light
        float Ial = lights[i].color_ambient;
        f+= Ial;

        //Diffuse lighting
        float Il = lights[i].color;
        vec3 L  = normalize(lPos - fPos);
        f+= (Il) * max(0.0,dot(N,L));

        //Specular lighting
        vec3 V = normalize(eye3-fPos);
        vec3 R = reflect(normalize(fPos-lPos),N);
        float n = material.shininess;
        if(dot(N,L)>=0.0){
            f+= Il * pow(max(0.0,dot(V,R)),n);
        }
    }
    vec3 Fd = min(floor(f*f*material.bands),material.bands-1)/(material.bands-1);
    vec4 K;
    if(material.use_diffuse_texture){
        K = vec4(material.color,1) * texture(material.diffuse_texture,fTexCoord).rgba;
    }else{
        K = vec4(material.color,1);
    }
    // Color is the normal.
    FragColor = vec4(Fd,1)*K;
}
