#include "shape.h"

#include "debugging.h"

#include <cmath> // std::sin(), std::cos()

using namespace glm;

namespace graphics101 {

void Cylinder::tessellate( Mesh& mesh ) {
    mesh.clear();
    // Let's iterate over x and y from the bottom-left (0,0) to the top-right (1,1)
    // and create positions:
    for( int yi = 0; yi < m_stacks; ++yi ){
      for( int xi = 0; xi < m_slices; ++xi ){
          // The xi-th coordinate will be x.
          float i = float(xi);
          float u = float(i)/m_slices;
          float phi = 2* pi * u;
          float x = cos(phi);
          float y = sin(phi);
            // The yi-th coordinate will be y.
            float j = yi;
            float v = j/(m_stacks-1);
            float z = v;
            // Create the position.
            mesh.positions.push_back( vec3( x, y, z ) );

            // If we aren't the bottom-most row of vertices or left-most column
            if(yi==0){
              mesh.normals.push_back(vec3( x, y, 0 ) );
              mesh.texcoords.push_back(vec2(phi/(2*pi),z));
            }
        }
    }
    for( int yi = 0; yi < m_stacks*m_slices-m_slices; yi+=m_slices){
      for( int xi = 1; xi < m_slices; xi++){
        int index2 = xi + yi;
        Triangle t =  Triangle(
              // upper-right
              index2,
              // upper-left
              index2+m_slices,
              // lower-left
              index2+m_slices-1
              );
        mesh.face_positions.push_back( t );
        mesh.face_normals.push_back(t%m_slices );
        mesh.face_texcoords.push_back(t%m_slices );
        //matching triangle
        t = Triangle(
              // upper-right
              index2,
              // upper-left
              index2+m_slices-1,
              // lower-left
              index2-1
              );
        mesh.face_positions.push_back( t );
        mesh.face_normals.push_back( t%m_slices );
        mesh.face_texcoords.push_back(t%m_slices );
      }
      //stitch the cylinder
      int index = yi;
      Triangle t = Triangle(
            // upper-right
            yi,
            // upper-left
            index+m_slices,
            // lower-left
            index+m_slices+m_slices-1
            );
      mesh.face_positions.push_back( t );
      mesh.face_normals.push_back( t%m_slices );
      mesh.face_texcoords.push_back(t%m_slices );
      //matching triangle
      t = Triangle(
            // upper-right
            yi,
            // upper-left
            index+m_slices+m_slices-1,
            // lower-left
            index+m_slices-1
            );
      mesh.face_positions.push_back( t );
      mesh.face_normals.push_back( t%m_slices );
      mesh.face_texcoords.push_back(t%m_slices );
    }
#ifdef CAPS
    mesh.positions.push_back( vec3( 0, 0, 1 ));
    int topPosition = mesh.positions.size()-1;
    mesh.normals.push_back(vec3( 0, 0, 1 ));
    Triangle topNormal = Triangle(mesh.normals.size()-1, mesh.normals.size()-1,mesh.normals.size()-1);

    mesh.positions.push_back( vec3( 0, 0, 0 ));
    int bottomPosition = mesh.positions.size()-1;
    mesh.normals.push_back(vec3( 0, 0, -1 ));
    Triangle bottomNormal = Triangle(mesh.normals.size()-1, mesh.normals.size()-1,mesh.normals.size()-1);

    int topOffset = mesh.positions.size()-2-m_slices;
    for( int i = 0; i < m_slices; i++){
      Triangle t =  Triangle(
            // upper-right
            (i+1)%m_slices,
            // upper-left
            i,
            // lower-left
            bottomPosition
            );
      mesh.face_positions.push_back( t );
      mesh.face_normals.push_back( bottomNormal );
      mesh.face_texcoords.push_back( vec3(0,0,0) );

      //matching triangle
      t =  Triangle(
            // upper-right
            topPosition,
            // upper-left
            topOffset+i,
            // lower-left
            topOffset+(i+1)%m_slices
            );
      mesh.face_positions.push_back( t );
      mesh.face_normals.push_back( topNormal );
      mesh.face_texcoords.push_back( vec3(mesh.texcoords.size()-1,mesh.texcoords.size()-1,mesh.texcoords.size()-1) );
    }
#endif
}

void Sphere::tessellate( Mesh& mesh )
{
  mesh.clear();
  // Let's iterate over x and y from the bottom-left (0,0) to the top-right (1,1)
  // and create positions:
  int count = 0;
  for( int yi = 1; yi < m_stacks+1; ++yi ){
      for( int xi = 0; xi < m_slices; ++xi ){
        // The xi-th coordinate will be x.
        float u     = float(xi)/m_slices;
        float phi   = 2*pi*u;
        float v     = float(yi)/(m_stacks+1);
        float theta = pi*v;
        float x     = sin(theta)*cos(phi);
        float y     = sin(theta)*sin(phi);
        float z     = cos(theta);
        count++;
        mesh.positions.push_back( vec3( x, y, z ) );
        mesh.normals.push_back(vec3( x, y, z ) );
      }
  }
  mesh.positions.push_back( vec3( 0, 0, 1 ) );
  mesh.normals.push_back(vec3( 0, 0, 1 ) );
  mesh.positions.push_back( vec3( 0, 0, -1 ) );
  mesh.normals.push_back(vec3( 0, 0, -1 ) );

  for( int yi = 0; yi < m_stacks*m_slices-m_slices; yi+=m_slices){
    for( int xi = 0; xi < m_slices-1; xi++){
      int index2 = xi + yi;
      Triangle t =  Triangle(
            // upper-right
            index2,
            // upper-left
            index2+m_slices,
            // lower-left
            index2+m_slices+1
            );
      mesh.face_positions.push_back(t);
      mesh.face_normals.push_back(t);
      //matching triangle
      t = Triangle(
            // upper-right
            index2,
            // upper-left
            index2+m_slices+1,
            // lower-left
            index2+1
            );
      mesh.face_positions.push_back( t );
      mesh.face_normals.push_back( t );
    }
    //stitch together
    int index = (yi/m_slices+1)*m_slices-1;
    Triangle t = Triangle(
          // upper-right
          index,
          // upper-left
          index+m_slices,
          // lower-left
          index+1
          );
    mesh.face_positions.push_back( t );
    mesh.face_normals.push_back( t );
    //matching triangle
    t = Triangle(
          // upper-right
          index,
          // upper-left
          index+1,
          // lower-left
          yi
          );
    mesh.face_positions.push_back( t );
    mesh.face_normals.push_back( t );
  }

  //Make the caps
  int top = m_stacks*m_slices;
  for( int i = 0; i < m_slices; ++i ){
    Triangle t = Triangle(
          // upper-right
          i,
          // upper-left
          (i+1)%m_slices,
          // lower-left
          top
          );
    mesh.face_positions.push_back( t );

    // The corresponding normal indices are simple, since all faces have
    // the same normal.
    mesh.face_normals.push_back( t );
  }
  int bottom = m_stacks*m_slices+1;
  for( int i = 0; i < m_slices; ++i ){
    //bottom cap
    int index = m_stacks*m_slices-m_slices;
    Triangle t = Triangle(
          // upper-right
          index+(i+1)%(m_slices),
          // upper-left
          index+i,
          // lower-left
          bottom
          );
    mesh.face_positions.push_back( t );

    // The corresponding normal indices are simple, since all faces have
    // the same normal.
    mesh.face_normals.push_back( t );
  }
}

void Cone::tessellate( Mesh& mesh )
{
    mesh.clear();
    // Let's iterate over x and y from the bottom-left (0,0) to the top-right (1,1)
    // and create positions:
    for( int yi = 0; yi < m_stacks; ++yi ){
      for( int xi = 0; xi < m_slices; ++xi ){
          // The xi-th coordinate will be x.
          float i = float(xi);
          float u = float(i)/m_slices;
          float phi = 2* pi * u;
          // The yi-th coordinate will be y.
          float j = yi;
          float v = j/(m_stacks);
          float z = v;
          float x = cos(phi)*(1-z);
          float y = sin(phi)*(1-z);

            // Create the position.
            mesh.positions.push_back( vec3( x, y, z ) );
        }
    }
    for( int yi = 0; yi < m_stacks*m_slices-m_slices; yi+=m_slices){
      for( int xi = 1; xi < m_slices; xi++){
        int index2 = xi + yi;
        Triangle t =  Triangle(
              // upper-right
              index2,
              // upper-left
              index2+m_slices,
              // lower-left
              index2+m_slices-1
              );
        mesh.face_positions.push_back( t );
        mesh.face_normals.push_back(vec3(xi,xi,xi));
        if(mesh.normals.size()<m_slices){
          mesh.normals.push_back(normalize(cross(mesh.positions[t[0]]-mesh.positions[t[1]],
                                     mesh.positions[t[0]]-mesh.positions[t[2]])));
        }
        //matching triangle
        t = Triangle(
              // upper-right
              index2,
              // upper-left
              index2+m_slices-1,
              // lower-left
              index2-1
              );
        mesh.face_positions.push_back( t );
        mesh.face_normals.push_back(vec3(xi,xi,xi));
        if(mesh.normals.size()<m_slices){
          mesh.normals.push_back(cross(mesh.positions[t[0]]-mesh.positions[t[1]],
                                     mesh.positions[t[0]]-mesh.positions[t[2]]));
        }
      }
      //stitch the cone
      int index = yi;
      Triangle t = Triangle(
            // upper-right
            yi,
            // upper-left
            index+m_slices,
            // lower-left
            index+m_slices+m_slices-1
            );
      mesh.face_positions.push_back( t );
      mesh.face_normals.push_back(vec3(mesh.normals.size()-1,mesh.normals.size()-1,mesh.normals.size()-1));
      //matching triangle
      t = Triangle(
            // upper-right
            yi,
            // upper-left
            index+m_slices+m_slices-1,
            // lower-left
            index+m_slices-1
            );
      mesh.face_positions.push_back( t );
      mesh.face_normals.push_back(vec3(mesh.normals.size()-1,mesh.normals.size()-1,mesh.normals.size()-1));
      if(mesh.normals.size()<m_slices){
        mesh.normals.push_back(cross(mesh.positions[t[0]]-mesh.positions[t[1]],
                                   mesh.positions[t[0]]-mesh.positions[t[2]]));
      }
    }
    //top cap
    int topPosition = mesh.positions.size();
    mesh.positions.push_back( vec3( 0, 0, 1 ));
    int topOffset = mesh.positions.size()-1-m_slices;
    for( int i = 0; i < m_slices; i++){
      Triangle t =  Triangle(
            // upper-right
            topOffset+i,
            // upper-left
            topOffset+(i+1)%m_slices,
            // lower-left
            topPosition
            );
      mesh.face_positions.push_back( t );
      mesh.face_normals.push_back(vec3(i,i,i));
      if(mesh.normals.size()<m_slices){
        mesh.normals.push_back(cross(mesh.positions[t[0]]-mesh.positions[t[1]],
                                   mesh.positions[t[0]]-mesh.positions[t[2]]));
      }
    }

#ifdef CAPS
    int bottomPosition = mesh.positions.size();
    mesh.positions.push_back( vec3( 0, 0, 0 ));
    mesh.normals.push_back(vec3( 0, 0, -1 ));
    for( int i = 0; i < m_slices; i++){
      Triangle t =  Triangle(
            // upper-right
            (i+1)%m_slices,
            // upper-left
            i,
            // lower-left
            bottomPosition
            );
      mesh.face_positions.push_back( t );
      mesh.face_normals.push_back(vec3(mesh.normals.size()-2,mesh.normals.size()-2,mesh.normals.size()-2));
    }
#endif

}

void Torus::tessellate( Mesh& mesh )
{
    mesh.clear();
    
    // Your code goes here.
}

void Cube::tessellate( Mesh& mesh )
{
    mesh.clear();
    
    // Your code goes here.
}

}
