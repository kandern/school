#include "scene.h"

// Include all of glm here.
#include "glm/glm.hpp"

#include "debugging.h"

namespace graphics101 {

void Scene::render( Image& into_image ) {
    // Iterate over the pixels of the image. For each pixel:
    vec2 uv;
    vec3 color;
    for(int x=0;x<into_image.width();x++){
        for(int y=0;y<into_image.height();y++){
            // 1. Use camera->getPixelUV() and camera->getRay() to create a ray3.
            uv = camera->getPixelUV(vec2(x,y),into_image.width(),into_image.height());
            ray3 ray = camera->getRay(uv);
            // 2. Call rayColor() to get a vec3 color for that ray.
            color = rayColor(ray,1);
            // 3. Use into_image.pixel() to set the pixel color.
            into_image.pixel(x,y) = ColorRGBA8(color.x*255,color.y*255,color.z*255);
        }
    }
}

bool Scene::closestIntersection( const ray3& ray, Intersection& hit_out ) const {
    Intersection tClosestIntersection;
    tClosestIntersection.t = -1;
    // Iterate over all the shapes, calling rayIntersect() on each.
    for( const ShapePtr shape: shapes ) {
        Intersection tIntersection;
        if(shape->rayIntersect(ray, tIntersection)){
            if(tClosestIntersection.t<0||tIntersection.t<tClosestIntersection.t){
                tClosestIntersection = tIntersection;
            }
        }
    }
    //if we found a hit, return it
    if(tClosestIntersection.t>=0){
        hit_out = tClosestIntersection;
        return true;
    }
    return false;
}
vec3 Scene::rayColor( const ray3& ray, int max_recursion ) const {
    // In this ray casting assignment, you will simply call closestIntersection(),
    // and, if there is one, return the .material.color_diffuse;
    // Otherwise, return black (0,0,0).
    assert( max_recursion >= 0 );
    //const real eps = 1e-7;
    Intersection tObject;
    vec3 c( 0,0,0 );
    if(closestIntersection(ray,tObject)){
        c = tObject.material.color_diffuse;
    }

    return c;
}

}
