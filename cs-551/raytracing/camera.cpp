#include "camera.h"

#include "debugging.h"

#include <cmath> // fabs()
#include <algorithm> // std::max()

namespace graphics101 {

ray3 CameraPerspective::getRay( const vec2& uv ) const {
    // Create a ray3( ray origin point, ray direction ).
    // Since you will be testing for intersections along the ray,
    // you may want to start the ray on the film plane so that objects
    // between the eye() and the film plane aren't drawn.
    vec3 tLocation(e);
//    tLocation.x -=uv[0];
//    tLocation.y -=uv[1];
//    tLocation.z -=focal_length;
    vec3 tDirection(u*uv[0]+v*uv[1]-focal_length*w);
    return ray3( tLocation, tDirection );
}

ray3 CameraOrthographic::getRay( const vec2& uv ) const {
    // Create a ray3( ray origin point, ray direction ).
    vec3 tLocation(e+u*uv[0]+v*uv[1]);
    return ray3( tLocation, -w);
}

void Camera::imageDimensionsWithLongEdge( int long_edge_pixels, int& width_out, int& height_out ) const {
    assert( long_edge_pixels > 0 );
    
    const real w = fabs( film_right - film_left );
    const real h = fabs( film_bottom - film_top );
    
    if( w > h ) {
        width_out = long_edge_pixels;
        height_out = std::max( 1L, lround( long_edge_pixels * h / w ) );
    } else {
        height_out = long_edge_pixels;
        width_out = std::max( 1L, lround( long_edge_pixels * w / h ) );
    }
}

}
