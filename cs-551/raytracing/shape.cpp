#include "shape.h"

#include "debugging.h"

#include <vector>
#include <cmath> // std::sqrt()

namespace graphics101 {


vec4 Shape::toObjectSpace(vec3 aVector,float aHomogeneous) const{
    return toObjectSpace(vec4(aVector.x,aVector.y,aVector.z,aHomogeneous));
}

vec4 Shape::toObjectSpace(vec4 aVector) const{
    return transformInverse() * aVector;
}

vec4 Shape::toWorldSpace(vec3 aVector,float aHomogeneous) const{
    return toWorldSpace(vec4(aVector.x,aVector.y,aVector.z,aHomogeneous));
}

vec4 Shape::toWorldSpace(vec4 aVector) const{
    return transpose(transformInverse())*aVector;
}

bool Sphere::rayIntersect( const ray3& ray, Intersection& hit_out ) const {

    // Remember that this intersection occurs in object-space.
    // You convert the world-space ray to object-space by multipling transformInverse() * ray.p and ray.d.
    // When filling out `hit_out`, don't forget to return the resulting position and normal in world-space.
    // For the normal, that means left-multiplying the object-space normal by transpose( transformInverse() ).
    vec4 tP = toObjectSpace(ray.p,1);
    vec4 tD = toObjectSpace(ray.d,0);
    float a = pow(tD.x,2) + pow(tD.y,2) + pow(tD.z,2);//dot(tD,tD);
    float b = 2*(tP.x*tD.x + tP.y*tD.y + tP.z*tD.z);//2*dot(tP,tD);
    float c = pow(tP.x,2) + pow(tP.y,2) + pow(tP.z,2) - 1.0;//dot(tP,tP)-1;
    //quadratic formula to determine:
    if(pow(b,2)-(4*a*c)<0){
        return false;
    }
    double t  = (-b + sqrt(pow(b,2)-4*a*c))/(2*a);
    float t2 = (-b - sqrt(pow(b,2)-4*a*c))/(2*a);
    if(t<0&&t2<0){
        return false;
    }else if(t2<0){
        hit_out.t = t;
    }else{
        hit_out.t = t2;
    }
    vec3  q  = tP+(hit_out.t*tD);
    hit_out.position = ray.p +t*ray.d;
    hit_out.normal = toWorldSpace(vec4(2*q.x,2*q.y,2*q.z,0));
    hit_out.material = material();
    return true;
}

bool Plane::rayIntersect( const ray3& ray, Intersection& hit_out ) const {
    // Remember that this intersection occurs in object-space.
    // You convert the world-space ray to object-space by multipling transformInverse() * ray.p and ray.d.
    // When filling out `hit_out`, don't forget to return the resulting position and normal in world-space.
    // For the normal, that means left-multiplying the object-space normal by transpose( transformInverse() ).
    vec4 tP = toObjectSpace(ray.p,1);
    vec4 tD = toObjectSpace(ray.d,0);
    if(tD.z == 0){
        return false;
    }
    double t = -tP.z/tD.z;
    if(t>=0){
        hit_out.t = t;
        hit_out.normal = toWorldSpace(vec4(0,0,1,0));
        hit_out.position = ray.p +t*ray.d;
        hit_out.material = material();
        return true;
    }
    return false;
}

bool Cylinder::rayIntersect( const ray3& ray, Intersection& hit_out ) const {
    // Remember that this intersection occurs in object-space.
    // You convert the world-space ray to object-space by multipling transformInverse() * ray.p and ray.d.
    // When filling out `hit_out`, don't forget to return the resulting position and normal in world-space.
    // For the normal, that means left-multiplying the object-space normal by transpose( transformInverse() ).

    // The list of all intersections.

    std::vector<Intersection> ts(0);

    vec4 tP = toObjectSpace(ray.p,1);
    vec4 tD = toObjectSpace(ray.d,0);
    if(tD.z!=0){
        // z = 0 plane: F( x,y,z ) = -z
        //Bottom Cap
        double t = -tP.z / tD.z;
        if(t >= 0){
            vec4 q = tP + (t*tD);
            if((pow(q.x,2) + pow(q.y,2)) < 1){
                vec4 tNormal(0,0,-1,0);
                Intersection tIntersection;
                tIntersection.t = t;
                tIntersection.normal = toWorldSpace(tNormal);
                tIntersection.position=ray.p +t*ray.d;
                tIntersection.material = material();
                ts.push_back(tIntersection);
            }
        }

        // z = 1 plane: F( x,y,z ) = z - 1
        //Top Cap
        t = ( 1 - tP.z ) / tD.z;
        if(t >= 0){
            vec4 q = tP + (t*tD);
            if((pow(q.x,2) + pow(q.y,2)) < 1){
                vec4 tNormal(0,0,1,0);
                Intersection tIntersection;
                tIntersection.t = t;
                tIntersection.normal = toWorldSpace(tNormal);
                tIntersection.position=ray.p +t*ray.d;
                tIntersection.material = material();
                ts.push_back(tIntersection);
            }
        }
    }

    // sides of the cylinder: F( x,y,z ) = x^2 + y^2 - 1
    // F( p + t*d ) = ( p.x + t*d.x )^2 + ( p.y + t*d.y )^2 - 1
    // <=> t^2 * a + t * b + c
    float a = pow(tD.x,2) + pow(tD.y,2);
    float b = 2*(tP.x*tD.x + tP.y*tD.y);
    float c = pow(tP.x,2) + pow(tP.y,2) - 1.0;

    // quadratic formula: t = ( -b +- sqrt( b^2 - 4ac ) )/( 2*a )
    float radical = pow(b,2) - 4*a*c;
    if(radical >= 0){
        double t = (-b + sqrt(radical)) / (2*a);
        if(t >= 0){
            vec4 q = tP + (t * tD);
            vec4 tNormal( 2*q.x, 2*q.y, 0, 0 );
            if(0 < q.z && q.z < 1){
                Intersection tIntersection;
                tIntersection.t = t;
                tIntersection.normal = toWorldSpace(-tNormal);
                tIntersection.position=ray.p +t*ray.d;
                tIntersection.material = material();
                ts.push_back(tIntersection);
            }
        }
        double t2 = (-b - sqrt(radical)) / (2*a);
        if(t2 >= 0){
            vec4 q = tP + (t2 * tD);
            vec4 tNormal( 2*q.x, 2*q.y,0,0 );
            if(0 < q.z && q.z < 1){
                Intersection tIntersection;
                tIntersection.t = t2;
                tIntersection.normal = toWorldSpace(-tNormal);
                tIntersection.position=ray.p +t*ray.d;
                tIntersection.material = material();
                ts.push_back(tIntersection);
            }
        }
    }
    if (ts.empty()){
        return false;
    }
    Intersection tIntersection;
    tIntersection.t = -1;
    //get minimum t value
    for(Intersection tIntersect:ts){
        if(tIntersect.t>=0){
            if(tIntersection.t<0||tIntersect.t<tIntersection.t){
                tIntersection.t = tIntersect.t;
                tIntersection.normal = tIntersect.normal;
                tIntersection.material = material();
                tIntersection.position = tIntersection.position;
            }
        }
    }
    hit_out = tIntersection;
    return true;
}

bool Cone::rayIntersect( const ray3& ray, Intersection& hit_out ) const {

//    // Remember that this intersection occurs in object-space.
//    // You convert the world-space ray to object-space by multipling transformInverse() * ray.p and ray.d.
//    // When filling out `hit_out`, don't forget to return the resulting position and normal in world-space.
//    // For the normal, that means left-multiplying the object-space normal by transpose( transformInverse() ).
    std::vector<Intersection> ts(0);

    vec4 tP = toObjectSpace(ray.p,1);
    vec4 tD = toObjectSpace(ray.d,0);
    if(tD.z!=0){
        // z = 0 plane: F( x,y,z ) = -z
        //Bottom
        double t = -tP.z / tD.z;
        if(t >= 0){
            vec4 q = tP + (t*tD);
            if((pow(q.x,2) + pow(q.y,2)) < 1){
                vec4 tNormal(0,0,-1,0);
                Intersection tIntersection;
                tIntersection.t = t;
                tIntersection.normal = toWorldSpace(tNormal);
                tIntersection.position=ray.p +t*ray.d;
                ts.push_back(tIntersection);
            }
        }
    }

    // sides of the cylinder: F( x,y,z ) = x^2 + y^2 - 1
    // F( p + t*d ) = ( p.x + t*d.x )^2 + ( p.y + t*d.y )^2 - 1
    // <=> t^2 * a + t * b + c
    float a = pow(tD.x,2) + pow(tD.y,2) - pow(tD.z,2);
    float b = 2*( tP.x*tD.x + tP.y*tD.y + (1-tP.z)*tD.z );
    float c = pow(tP.x,2) + pow(tP.y,2) - pow(1-tP.z,2);

    // quadratic formula: t = ( -b +- sqrt( b^2 - 4ac ) )/( 2*a )
    float radical = pow(b,2) - 4*a*c;
    if(radical >= 0){
        double t = (-b + sqrt(radical)) / (2*a);
        if(t >= 0){
            vec4 q = tP + (t * tD);
            vec4 tNormal(2*q.x, 2*q.y, 2*(1-q.z),0);
            if(0 <= q.z && q.z <= 1){
                Intersection tIntersection;
                tIntersection.t = t;
                tIntersection.normal = toWorldSpace(tNormal);
                tIntersection.position=ray.p +t*ray.d;
                ts.push_back(tIntersection);
            }
        }
        double t2 = (-b - sqrt(radical)) / (2*a);
        if(t2 >= 0){
            vec4 q = tP + (t2 * tD);
            vec4 tNormal( 2*q.x, 2*q.y, 2*(1-q.z),0 );
            if(0 <= q.z && q.z <= 1){
                Intersection tIntersection;
                tIntersection.t = t2;
                tIntersection.normal = toWorldSpace(tNormal);
                tIntersection.position=ray.p +t2*ray.d;
                ts.push_back(tIntersection);
            }
        }
    }

    if (ts.empty()){
        return false;
    }
    Intersection tIntersection;
    tIntersection.t = -1;
    //get minimum t value
    for(Intersection tIntersect:ts){
        if(tIntersect.t>=0){
            if(tIntersection.t<0||tIntersect.t<tIntersection.t){
                tIntersection = tIntersect;
            }
        }
    }
    hit_out = tIntersection;
    hit_out.material = material();
    return true;
}

bool Cube::rayIntersect( const ray3& ray, Intersection& hit_out ) const {
    std::vector<Intersection> ts(0);
    vec4 tP = toObjectSpace(ray.p,1);
    vec4 tD = toObjectSpace(ray.d,0);
    //for x y and z faces
    for(int tPlane=0;tPlane<3;tPlane++){
        int tSign = -1;
        float b = tD[tPlane];
        float c = tP[tPlane] - tSign;
        if(b != 0){
            double t = -c/b;
            vec4 q = tP+(t*tD);
            bool tValid = t>=0;
            tValid &= q[(tPlane+1)%3]>-1&&q[(tPlane+1)%3]<1;
            tValid &= q[(tPlane+2)%3]>-1&&q[(tPlane+2)%3]<1;
            if(tValid){
                Intersection tIntersection;
                tIntersection.t = t;
                tIntersection.position =ray.p +t*ray.d;
                tIntersection.normal[tPlane] = tSign;
                tIntersection.normal[(tPlane+1)%3] = 0;
                tIntersection.normal[(tPlane+2)%3] = 0;
                tIntersection.normal = toWorldSpace(tIntersection.normal,0);
                ts.push_back(tIntersection);
            }
            tSign *=-1;
            c = tP[tPlane] - tSign;
            t = -c/b;
            q = tP+(t*tD);
            tValid = t>=0;
            tValid &= q[(tPlane+1)%3]>-1&&q[(tPlane+1)%3]<1;
            tValid &= q[(tPlane+2)%3]>-1&&q[(tPlane+2)%3]<1;
            if(tValid){
                Intersection tIntersection;
                tIntersection.t = t;
                tIntersection.position =ray.p +t*ray.d;
                tIntersection.normal[tPlane] = tSign;
                tIntersection.normal[(tPlane+1)%3] = 0;
                tIntersection.normal[(tPlane+2)%3] = 0;
                tIntersection.normal = toWorldSpace(tIntersection.normal,0);
                ts.push_back(tIntersection);
            }
        }
    }
    Intersection tIntersection;
    tIntersection.t = -1;
    //get minimum t value
    for(Intersection tIntersect:ts){
        if(tIntersect.t>=0){
            if(tIntersection.t<0||tIntersect.t<tIntersection.t){
                tIntersection.t = tIntersect.t;
                tIntersection.normal = tIntersect.normal;
                tIntersection.material = material();
                tIntersection.position = tIntersect.position;
            }
        }
    }
    if(tIntersection.t <0){
        return false;
    }
    hit_out = tIntersection;
    return true;
}

bool Mesh::rayIntersect( const ray3& ray, Intersection& hit_out ) const {
    (void)ray;
    (void) hit_out;
    // Remember that this intersection occurs in object-space.
    // You convert the world-space ray to object-space by multipling transformInverse() * ray.p and ray.d.
    // When filling out `hit_out`, don't forget to return the resulting position and normal in world-space.
    // For the normal, that means left-multiplying the object-space normal by transpose( transformInverse() ).

    return false;
}

}
