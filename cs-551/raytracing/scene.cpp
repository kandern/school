#include "scene.h"
#include <vector>
#include <cmath> // std::sqrt()
// Include all of glm here.
#include "glm/glm.hpp"

#include "debugging.h"

namespace graphics101 {

void Scene::render( Image& into_image ) {
    // Iterate over the pixels of the image. For each pixel:
    vec2 uv;
    vec3 color;
    for(int x=0;x<into_image.width();x++){
        for(int y=0;y<into_image.height();y++){
            // 1. Use camera->getPixelUV() and camera->getRay() to create a ray3.
            uv = camera->getPixelUV(vec2(x,y),into_image.width(),into_image.height());
            ray3 ray = camera->getRay(uv);
            // 2. Call rayColor() to get a vec3 color for that ray.
            color = rayColor(ray,3);
            color = clamp( color, 0.0, 1.0 );
            // 3. Use into_image.pixel() to set the pixel color.
            into_image.pixel(x,y) = ColorRGBA8(color.x*255,color.y*255,color.z*255);
        }
    }
}

bool Scene::closestIntersection( const ray3& ray, Intersection& hit_out ) const {
    Intersection tClosestIntersection;
    tClosestIntersection.t = -1;
    // Iterate over all the shapes, calling rayIntersect() on each.
    for( const ShapePtr shape: shapes ) {
        Intersection tIntersection;
        if(shape->rayIntersect(ray, tIntersection)){
            if(tClosestIntersection.t<0||tIntersection.t<tClosestIntersection.t){
                tClosestIntersection = tIntersection;
            }
        }
    }
    //if we found a hit, return it
    if(tClosestIntersection.t>=0){
        hit_out = tClosestIntersection;
        return true;
    }
    return false;
}

// (Kr * Ir) + (Kt  * It) + SUM( (Ka * Ial) + [(Kd * Il)*(N * L) + (Ks * Il * (V * R))]*Sl)
vec3 Scene::rayColor( const ray3& ray, int max_recursion ) const {
    assert( max_recursion >= 0 );
    static const real eps = 1e-7;
    Intersection tIntersection;
    Material     tMaterial;
    vec3 c( 0,0,0 );
    if(!closestIntersection(ray,tIntersection)){
       return c;
    }
    tMaterial = tIntersection.material;
    vec3 Kr = tMaterial.color_reflect;
    vec3 Ir = vec3(0,0,0);
    //    vec3 Kt = tMaterial.color_refract;
    //    vec3 It = vec3(0,0,0);


    vec3 N = normalize(tIntersection.normal);
    //recursilvely get the color of light by reflecting max_recursion times
    if(tMaterial.reflective&&max_recursion>0){
        ray3 tRay(tIntersection.position+reflect(ray.d,N)*eps,reflect(ray.d,N));
        Ir = clamp(rayColor(tRay,max_recursion-1),0.0,1.0);
    }
    //c = (Kr * Ir);// Translucient: + (Kt  * It);
    //for every light in the scene add up
    for( const Light& l : lights ) {
            vec3 Ka = tMaterial.color_ambient;
            vec3 Ial = l.color_ambient;

            vec3 Kd = tMaterial.color_diffuse;
            vec3 Il  = l.color;
                 N = normalize(tIntersection.normal);
            vec3 L = normalize(l.position - tIntersection.position);

            vec3 Ks = tMaterial.color_specular;
                 Il  = l.color;
            vec3 V = normalize(ray.p-tIntersection.position);
            vec3 R = reflect(normalize(tIntersection.position-l.position),N);
            double n = tMaterial.shininess;

            c += (Ka * Ial);
            Intersection tInBetween;
            ray3 tRay(tIntersection.position + (L)*eps,L);
            if(closestIntersection(tRay,tInBetween)){
                if(!(distance(tRay.p,tInBetween.position)<distance(tRay.p,l.position))){
                    if(dot(N,L)>=0.0){
                        c += ((Kd * Il) * std::max(0.0,dot(N,L)) + Ks * Il * pow(std::max(0.0,dot(V,R)),n));
                    }
                }
            }else{
                if(dot(N,L)>=0.0){
                    c += ((Kd * Il) * std::max(0.0,dot(N,L)) + Ks * Il * pow(std::max(0.0,dot(V,R)),n));
                }
            }
    }
    return c;

}
}
