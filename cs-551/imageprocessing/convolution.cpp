#include "convolution.h"

#include <algorithm>
#include <cassert>
#include <cstdlib>
#include <cmath>
#include <functional> // std::function
#include <iostream> // std::cout and std::cerr
#include <math.h>
#include <vector>

using namespace graphics101;

namespace {

typedef struct filter{
  float radius;
  std::function<real(real)> sample;
} Filter;

inline real triangle( real radius, real x ) {
    return std::max(0.0f,1-std::abs(x/radius));
}

// Returns `value` clamped to lie in the range [min,max].
template< typename T >
inline T clamp( const T& value, const T& min, const T& max ) {
    return std::min( max, std::max( min, value ) );
}

// Your helper functions go here.
//takes an array for an input and an array for the output, for each pixel in input apply the filter
void convolve1D( int               inputlength,
                 const ColorRGBA8* input,
                 ColorRGBA8*       output,
                 std::vector<real>  filter,
                 bool              normalize = true ) {
  int tReference = filter.size()/2;
  //if the filter has an even length, add 1, I don't think it really matters, but just I prefer it this way
  tReference+= filter.size()%2 ? 0 : 1;
  //for each pixel in the index apply the filter to the surrounding pixels and average your result
  for(int tIndex = 0; tIndex<inputlength;tIndex+= 1){
    real tR        = 0;
    real tG        = 0;
    real tB        = 0;
    real denominator = 0;
    // For each position in the filter calculate the filtered input pixel.
    for(unsigned int tFilterIndex = 0;tFilterIndex<filter.size();tFilterIndex += 1){
      // Start at the left most position in the filter.
      int tRelativeIndex = tFilterIndex - tReference;
      // if the begining of the filter is outside of the input array skip this part
     if (tIndex+tRelativeIndex<0||tIndex+tRelativeIndex>=inputlength) {
       continue;
     }
     real tScale = filter[tFilterIndex];
     const ColorRGBA8 tPixel = input[tIndex+tRelativeIndex];
     //I know it is greyscale, but this should allow it to not be
     denominator += tScale;

     tR += tPixel.r * tScale;
     tG += tPixel.g * tScale;
     tB += tPixel.b * tScale;
   }
   //normalize by dividing by the denominator
   if(normalize){
       tR /= denominator;
       tG /= denominator;
       tB /= denominator;
   }
   output[tIndex].r = clamp(lround(tR),0l,255l);
   output[tIndex].g = clamp(lround(tG),0l,255l);
   output[tIndex].b = clamp(lround(tB),0l,255l);
 }
}

void resample1D( int               inputlength,
                 const ColorRGBA8* a,
                 ColorRGBA8*       output,
                 int               outputlength,
                 Filter            filter,
                 bool              normalize = true ) {
  float r  = filter.radius;
  float xl = -0.5;
  float xh = inputlength - 0.5;
  int   n  = outputlength;
  float dx = (xh-xl)/n;
  float x0 = xl + dx/2;
  //for each pixel in the index apply the filter to the surrounding pixels and average your result
  for(int i = 0; i<n;i+= 1){
    real sR = 0;
    real sG = 0;
    real sB = 0;
    real denominator = 0;
    real x = x0 + i*dx;
    for(int j = std::ceil(x-r); j<std::floor(x+r);j+= 1){
      denominator += filter.sample(x-j);
      sR += a[j].r*filter.sample(x-j);
      sG += a[j].g*filter.sample(x-j);
      sB += a[j].b*filter.sample(x-j);
    }
    //normalize by dividing by the denominator
    if(normalize){
       sR /= denominator;
       sG /= denominator;
       sB /= denominator;
    }
    output[i].r = clamp(lround(sR),0l,255l);
    output[i].g = clamp(lround(sG),0l,255l);
    output[i].b = clamp(lround(sB),0l,255l);
  }
}
}
namespace graphics101 {
typedef struct Reference{
    int x;//row
    int y;//column
}Reference;

// Convolves the `input` image with `filter`, saving the result into `output`.
void convolve( const Image& input, const Image& filter, Image& output ) {
  // Debug with the identity filter
  // Image filter = Image( 1,1 );
  // filter.fill(ColorRGBA8(255,255,255));
  // Allocate an output image the same size as the input.
  output = Image( input.width(), input.height() );

  Image tFilter(filter);
  tFilter.flip().mirror();
  int tFilterWidth = tFilter.width();
  int tFilterHeight = tFilter.height();
  Reference tReference = {tFilterHeight/2,tFilterWidth/2};
  // iterate over each column.
  for( int x = 0; x < input.height(); ++x ) {
    //for each pixel in the column
    for(int y = 0; y < input.width(); ++y){
      float denominator = 0;
      int tR = 0;
      int tG = 0;
      int tB = 0;
      Reference tIndex;
      ColorRGBA8 tScale(0,0,0);
      //for each valid index in the filter, apply it to the input
      for(int tFilterX = 0; tFilterX<tFilterHeight; tFilterX+= 1){
        tIndex.x = tFilterX-tReference.x;
        //this row is off the image, skip it
        if(x+tIndex.x<0||x+tIndex.x>=input.height()){
          continue;
        }
        // For each position in the filter calculate the filtered input pixel.
        for(int tFilterY = 0;tFilterY<tFilterWidth;tFilterY += 1){
          // Start at the left most position in the filter.
          tIndex.y = tFilterY - tReference.y;
          // if the begining of the filter is outside of the input array skip this part
          if(y+tIndex.y<0||y+tIndex.y>=input.width()){
            continue;
          }
          tScale = filter.pixel(tFilterY,tFilterX);
          ColorRGBA8 tPixel = input.pixel(y+tIndex.y,x+tIndex.x);
          //I know it is greyscale, but this should allow it to not be
          denominator += ( tScale.r
                          +tScale.g
                          +tScale.b)/3.0;

          tR += ((int)tPixel.r) * tScale.r;
          tG += ((int)tPixel.g) * tScale.g;
          tB += ((int)tPixel.b) * tScale.b;
        }
      }
      //normalize by dividing by the denominator
      if(denominator){
        tR /= denominator;
        tG /= denominator;
        tB /= denominator;
      }else{
        tR = 1;
        tG = 1;
        tB = 1;
      }
      output.pixel(y,x) = ColorRGBA8(clamp(tR,0,255),clamp(tG,0,255),clamp(tB,0,255));
    }
  }
}

// Applies a box blur with `radius` to `input`, saving the result
// into `output`.
void blur_box( const Image& input, int r, Image& output ) {
  assert( r >= 0 );

  int width = input.width();
  int height  = input.height();
  Image tDx = Image(width, height);
  tDx.fill(ColorRGBA8(255,255,0,255));
  Image tDy = Image(width, height);
  tDy.fill(ColorRGBA8(255,255,0,255));

  real filterValue = 1.0/(2.0*r+1);
  std::vector<real> tFilter(2*r+1,filterValue);
  // Fill out Dx
  for( int j = 0; j < height; ++j ) {
      // Get a pointer to the pixels in each row.
      const ColorRGBA8* row_in = input.scanline(j);
      ColorRGBA8* row_out = tDx.scanline(j);

      convolve1D(width,row_in,row_out,tFilter);
  }

  // Fill out Dy
  for( int i = 0; i < width; ++i ) {
    // Get a pointer to the pixels in each column.
    // Pixels in a row are adjacent.
    // Pixels in a column are separated by a stride of input.width().
    const ColorRGBA8* col_in = tDx.scanline(0) + i;
    const int stride = width;
    ColorRGBA8* col_out = tDy.scanline(0) + i;
    std::vector<ColorRGBA8> inputRow;
    std::vector<ColorRGBA8> outputRow(height,ColorRGBA8(1,1,1));
    for( int j = 0; j < input.height(); ++j ) {
        // To access the j-th pixel, multiply j by the stride.
        inputRow.push_back(col_in[j*stride]);
    }
    convolve1D(height,&inputRow[0],&outputRow[0],tFilter,true);
    for(unsigned int i=0 ; i < outputRow.size();i++){
      col_out[i*stride] = outputRow[i];
    }
  }
  output = tDy;
}

// Scales the `input` image to the new dimensions, saving the result
// into `output`.
void scale( const Image& input, int new_width, int new_height, Image& output ) {
  assert( input.width() > 0 );
  assert( input.height() > 0 );
  assert( new_width > 0 );
  assert( new_height > 0 );
  real radiusX;
  if (new_width > input.width()){
    radiusX = 1;
  }else{
    radiusX = input.width()/new_width;
  }
  auto filterX = [radiusX]( real x ) { return triangle( radiusX, x ); };

  real radiusY;
  if (new_height > input.height()){
    radiusY = 1;
  }else{
    radiusY = input.height()/new_height;
  }
  auto filterY = [radiusY]( real x ) { return triangle( radiusY, x ); };
  Filter tFilterX{radiusX,filterX};
  Filter tFilterY{radiusY,filterY};
  Image tDx = Image( new_width, input.height() );
  Image tDy = Image( new_width, new_height );
  tDx.fill(ColorRGBA8(255,255,0,255));
  tDy.fill(ColorRGBA8(255,255,0,255));
  // Fill out Dx
  for( int j = 0; j < input.height(); ++j ) {
      // Get a pointer to the pixels in each row.
      const ColorRGBA8* row_in = input.scanline(j);
      ColorRGBA8* row_out = tDx.scanline(j);

      resample1D(input.width(),row_in,row_out,new_width,tFilterX,true);
  }

  // Fill out Dy
  for( int i = 0; i < new_width; ++i ) {
    // Get a pointer to the pixels in each column.
    // Pixels in a row are adjacent.
    // Pixels in a column are separated by a stride of input.width().
    const ColorRGBA8* col_in = tDx.scanline(0) + i;
    const int stride_in = tDx.width();
    ColorRGBA8* col_out = tDy.scanline(0) + i;
    const int stride_out = tDy.width();
    std::vector<ColorRGBA8> inputRow;
    std::vector<ColorRGBA8> outputRow(new_height,ColorRGBA8(1,1,1));
    for( int j = 0; j < input.height(); ++j ) {
        // To access the j-th pixel, multiply j by the stride.
        inputRow.push_back(col_in[j*stride_in]);
    }
    resample1D(tDx.height(),&inputRow[0],&outputRow[0],new_height,tFilterY,true);
    for(unsigned int i=0 ; i < outputRow.size();i++){
      col_out[i*stride_out] = outputRow[i];
    }
  }
  output = tDy;
}

// Sharpens the `input` image by moving `amount` away from a blur with `radius`.
// Saves the result into `output`.
void sharpen( const Image& input, real amount, int radius, Image& output ) {
    assert( radius >= 0 );
    blur_box(input,radius,output);
    for( int j = 0; j < input.height(); ++j ) {
      for( int i = 0; i < input.width(); ++i ) {
        // To access the i-th pixel, multiply i by the stride.
        const ColorRGBA8 inPix = input.pixel( i,j );
        const ColorRGBA8 outPix = output.pixel( i,j );
        real inR = inPix.r;
        inR = (1.0+amount)*inR - (amount*outPix.r);
        real inG = inPix.g;
        inG = (1.0+amount)*inG - (amount*outPix.g);
        real inB = inPix.b;
        inB = (1.0+amount)*inB - (amount*outPix.b);
        output.pixel( i,j ) = ColorRGBA8(
              clamp(lround(inR),0l,255l),
              clamp(lround(inG),0l,255l),
              clamp(lround(inB),0l,255l));
      }
    }
}

// Performs edge detection on the `input` image. Stores the result into `output`.
void edge_detect( const Image& input, Image& output ) {
  int width = input.width();
  int height  = input.height();
  output = Image( width, height );
  Image tDx = Image( width, height );
  Image tDy = Image( width, height );
  tDx.fill(ColorRGBA8(255,255,0,255));
  tDy.fill(ColorRGBA8(255,255,0,255));
  std::vector<real> filter = {-1,0,1};

  // Fill out Dx
  for( int j = 0; j < height; ++j ) {
      // Get a pointer to the pixels in each row.
      const ColorRGBA8* row_in = input.scanline(j);
      ColorRGBA8* row_out = tDx.scanline(j);

      convolve1D(width,row_in,row_out,filter,false);
  }

  // Fill out Dy
  for( int i = 0; i < width; ++i ) {
    // Get a pointer to the pixels in each column.
    // Pixels in a row are adjacent.
    // Pixels in a column are separated by a stride of width.
    const ColorRGBA8* col_in = tDx.scanline(0) + i;
    const int stride = width;
    ColorRGBA8* col_out = tDy.scanline(0) + i;
    std::vector<ColorRGBA8> inputRow;
    std::vector<ColorRGBA8> outputRow(height,ColorRGBA8(1,1,1));
    for( int j = 0; j < height; ++j ) {
        // To access the j-th pixel, multiply j by the stride.
        inputRow.push_back(col_in[j*stride]);
    }
    convolve1D(height,&inputRow[0],&outputRow[0],filter,false);
    for(unsigned int i=0 ; i < outputRow.size();i++){
      col_out[i*stride] = outputRow[i];
    }
  }

  //assign the output values
  for( int j = 0; j < height; ++j ) {
      for( int i = 0; i < width; ++i ) {
        ColorRGBA8 tDxPixel = tDx.pixel(i,j);
        ColorRGBA8 tDyPixel = tDy.pixel(i,j);
        ColorRGBA8 outputPixel(std::sqrt(std::pow(tDxPixel.r,2) + std::pow(tDyPixel.r,2)),
                               std::sqrt(std::pow(tDxPixel.g,2) + std::pow(tDyPixel.g,2)),
                               std::sqrt(std::pow(tDxPixel.b,2) + std::pow(tDyPixel.b,2)));
        output.pixel(i,j) = outputPixel;
      }
  }
}

// Converts each pixel to greyscale, saving the result into `output`.
void greyscale( const Image& input, Image& output ) {

    // Your code goes here.

    // Allocate an output image the same size as the input.
    output = Image( input.width(), input.height() );

    // Three ways to write it:
    // 1 Using input.pixel() and output.pixel()
    // 2 Using scanline() to iterate over rows.
    // 3 Using scanline() to iterate over columns.
    // I have written all three below.


    // 1 Using input.pixel()
    for( int j = 0; j < input.height(); ++j ) {
        for( int i = 0; i < input.width(); ++i ) {
            // To access the i-th pixel, multiply i by the stride.
            const ColorRGBA8 pix = input.pixel( i,j );

            // The grey value is just the average of red, green, and blue.
            // Perceptually, green is more important, but this is a fine approximation.
            const int grey = ( pix.r + pix.g + pix.b )/3;

            output.pixel( i,j ) = ColorRGBA8( grey, grey, grey );
        }
    }


    // 2 Using scanline() to iterate over each row.
    for( int j = 0; j < input.height(); ++j ) {

        // Get a pointer to the pixels in each row.
        // Pixels in a row are adjacent.
        // Pixels in a column are separated by a stride of input.width().
        const ColorRGBA8* row_in = input.scanline( j );
        const int stride_in = 1;

        ColorRGBA8* row_out = output.scanline( j );
        const int stride_out = 1;

        for( int i = 0; i < input.width(); ++i ) {
            // To access the i-th pixel, multiply i by the stride.
            const ColorRGBA8 pix = row_in[ i*stride_in ];

            // The grey value is just the average of red, green, and blue.
            // Perceptually, green is more important, but this is a fine approximation.
            const int grey = ( pix.r + pix.g + pix.b )/3;

            row_out[ i*stride_out ] = ColorRGBA8( grey, grey, grey );
        }
    }


    // 3 Using scanline() to iterate over each column.
    for( int i = 0; i < input.width(); ++i ) {

        // Get a pointer to the pixels in each column.
        // Pixels in a row are adjacent.
        // Pixels in a column are separated by a stride of input.width().
        const ColorRGBA8* col_in = input.scanline(0) + i;
        const int stride_in = input.width();

        ColorRGBA8* col_out = output.scanline(0) + i;
        const int stride_out = output.width();

        for( int j = 0; j < input.height(); ++j ) {
            // To access the j-th pixel, multiply j by the stride.
            const ColorRGBA8 pix = col_in[ j*stride_in ];

            // The grey value is just the average of red, green, and blue.
            // Perceptually, green is more important, but this is a fine approximation.
            const int grey = ( pix.r + pix.g + pix.b )/3;

            col_out[ j*stride_out ] = ColorRGBA8( grey, grey, grey );
        }
    }
}

// Converts each Applies a simple rgb filter to an image
void rgbFilter( const Image& input, ColorRGBA8 filter, Image& output ) {
  output = Image( input.width(), input.height() );

  for( int j = 0; j < input.height(); ++j ) {
    for( int i = 0; i < input.width(); ++i ) {
      // To access the i-th pixel, multiply i by the stride.
      const ColorRGBA8 pix = input.pixel( i,j );
      output.pixel( i,j ) = ColorRGBA8(
          clamp(pix.r * filter.r,0,255),
          clamp(pix.r * filter.g,0,255),
          clamp(pix.r * filter.b,0,255));
    }
  }

}

// Converts each Applies a simple rgb filter to an image
void invert( const Image& input, Image& output ) {
  output = Image( input.width(), input.height() );

  for( int j = 0; j < input.height(); ++j ) {
    for( int i = 0; i < input.width(); ++i ) {
      // To access the i-th pixel, multiply i by the stride.
      const ColorRGBA8 pix = input.pixel( i,j );
      output.pixel( i,j ) = ColorRGBA8(255 - pix.r, 255 - pix.g, 255 - pix.b);
    }
  }

}

// Subtracts `input1` from `input2`, saving the absolute value of the result into `output`.
// This function assumes that the dimensions of input1 and input2 match.
void difference( const Image& input1, const Image& input2, Image& output ) {
    assert( input1.width() == input2.width() );
    assert( input1.height() == input2.height() );

    // Your code goes here.

    // Allocate an output image the same size as the input.
    output = Image( input1.width(), input1.height() );

    // 1 Using input.pixel()
    for( int j = 0; j < input1.height(); ++j ) {
        for( int i = 0; i < input1.width(); ++i ) {
            const ColorRGBA8 pix1 = input1.pixel( i,j );
            const ColorRGBA8 pix2 = input2.pixel( i,j );

            const int rdiff = abs( int( pix1.r ) - int( pix2.r ) );
            const int gdiff = abs( int( pix1.g ) - int( pix2.g ) );
            const int bdiff = abs( int( pix1.b ) - int( pix2.b ) );

            output.pixel( i,j ) = ColorRGBA8( rdiff, gdiff, bdiff );
        }
    }
}

}
